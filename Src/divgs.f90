!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
!
! ... file divgs.f
!
!     this file includes documentation and code for
!     subroutine divgs          i
!
! ... files which must be loaded with divgs.f
!
!     sphcom.f, hrfft.f, vhags.f, shsgs.f, gaqd.f
!
!
!     subroutine divgs(nlat,nlon,isym,nt,divg,idiv,jdiv,br,bi,mdb,ndb,
!    +                 wshsgs,lshsgs,work,lwork,ierror)
!
!     given the vector spherical harmonic coefficients br and bi, precomputed
!     by subroutine vhags for a vector field (v,w), subroutine divgs
!     computes the divergence of the vector field in the scalar array divg.
!     divg(i,j) is the divergence at the gaussian colatitude point theta(i)
!     (see nlat as input parameter) and east longitude
!
!            lambda(j) = (j-1)*2*pi/nlon
!
!     on the sphere.  i.e.
!
!            dv(i,j) = 1/sint*[ d(sint*v(i,j))/dtheta + d(w(i,j))/dlambda ]
!
!     where sint = sin(theta(i)).  w is the east longitudinal and v
!     is the colatitudinal component of the vector field from which
!     br,bi were precomputed
!
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are computed
!            in radians in theta(1) <...< theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid point
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!
!     isym   a parameter which determines whether the divergence is
!            computed on the full or half sphere as follows:
!
!      = 0
!
!            the symmetries/antsymmetries described in isym=1,2 below
!            do not exist in (v,w) about the equator.  in this case the
!            divergence is neither symmetric nor antisymmetric about
!            the equator.  the divergence is computed on the entire
!            sphere.  i.e., in the array divg(i,j) for i=1,...,nlat and
!            j=1,...,nlon.
!
!      = 1
!
!            w is antisymmetric and v is symmetric about the equator.
!            in this case the divergence is antisymmetyric about
!            the equator and is computed for the northern hemisphere
!            only.  i.e., if nlat is odd the divergence is computed
!            in the array divg(i,j) for i=1,...,(nlat+1)/2 and for
!            j=1,...,nlon.  if nlat is even the divergence is computed
!            in the array divg(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!      = 2
!            w is symmetric and v is antisymmetric about the equator
!            in this case the divergence is symmetyric about the
!            equator and is computed for the northern hemisphere
!            only.  i.e., if nlat is odd the divergence is computed
!            in the array divg(i,j) for i=1,...,(nlat+1)/2 and for
!            j=1,...,nlon.  if nlat is even the divergence is computed
!            in the array divg(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!
!     nt     nt is the number of scalar and vector fields.  some
!            computational efficiency is obtained for multiple fields.
!            in the program that calls divgs, the arrays br,bi, and divg
!            can be three dimensional corresponding to an indexed multiple
!            vector field.  in this case multiple scalar synthesis will
!            be performed to compute the divergence for each field.  the
!            third index is the synthesis index which assumes the values
!            k=1,...,nt.  for a single synthesis set nt = 1.  the
!            description of the remaining parameters is simplified by
!            assuming that nt=1 or that all the arrays are two dimensional.
!
!     idiv   the first dimension of the array divg as it appears in
!            the program that calls divgs. if isym = 0 then idiv
!            must be at least nlat.  if isym = 1 or 2 and nlat is
!            even then idiv must be at least nlat/2. if isym = 1 or 2
!            and nlat is odd then idiv must be at least (nlat+1)/2.
!
!     jdiv   the second dimension of the array divg as it appears in
!            the program that calls divgs. jdiv must be at least nlon.
!
!     br,bi  two or three dimensional arrays (see input parameter nt)
!            that contain vector spherical harmonic coefficients
!            of the vector field (v,w) as computed by subroutine vhags.
!     ***    br and bi must be computed by vhags prior to calling
!            divgs.
!
!     mdb    the first dimension of the arrays br and bi as it
!            appears in the program that calls divgs. mdb must be at
!            least min0(nlat,nlon/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!     ndb    the second dimension of the arrays br and bi as it
!            appears in the program that calls divgs. ndb must be at
!            least nlat.
!
!
!     wshsgs an array which must be intialized by subroutine shsgsi.
!            once initialized,
!            wshsgs can be used repeatedly by divgs as long as nlon
!            and nlat remain unchanged.  wshsgs must not be altered
!            between calls of divgs.
!
!
!     lshsgs the dimension of the array wshsgs as it appears in the
!            program that calls divgs. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshsgs must be at least
!
!               nlat*(3*(l1+l2)-2)+(l1-1)*(l2*(2*nlat-l1)-3*l1)/2+nlon+15
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls divgs. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2                    if nlat is even or
!               l2 = (nlat+1)/2                if nlat is odd
!
!            if isym = 0 then lwork must be at least
!
!               nlat*((nt+1)*nlon+2*nt*l1+1)
!
!            if isym > 0 then lwork must be at least
!
!               (nt+1)*l2*nlon+nlat*(2*nt*l1+1)
!
!     **************************************************************
!
!     output parameters
!
!
!    divg   a two or three dimensional array (see input parameter nt)
!           that contains the divergence of the vector field (v,w)
!           whose coefficients br,bi where computed by subroutine
!           vhags.  divg(i,j) is the divergence at the gaussian colatitude
!           point theta(i) and longitude point lambda(j) = (j-1)*2*pi/nlon.
!           the index ranges are defined above at the input parameter
!           isym.
!
!
!    ierror = 0  no errors
!           = 1  error in the specification of nlat
!           = 2  error in the specification of nlon
!           = 3  error in the specification of isym
!           = 4  error in the specification of nt
!           = 5  error in the specification of idiv
!           = 6  error in the specification of jdiv
!           = 7  error in the specification of mdb
!           = 8  error in the specification of ndb
!           = 9  error in the specification of lshsgs
!           = 10 error in the specification of lwork
! **********************************************************************
!
!
      SUBROUTINE DIVGS(Nlat,Nlon,Isym,Nt,Divg,Idiv,Jdiv,Br,Bi,Mdb,Ndb,  &
                     & Wshsgs,Lshsgs,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL Bi , Br , Divg , Work , Wshsgs
      INTEGER ia , ib , Idiv , Ierror , imid , is , Isym , iwk , Jdiv , &
            & l1 , l2 , lp , lpimn , ls , Lshsgs , lwk , Lwork , mab ,  &
            & Mdb , mmax
      INTEGER mn , Ndb , Nlat , nln , Nlon , Nt
 
      DIMENSION Divg(Idiv,Jdiv,Nt) , Br(Mdb,Ndb,Nt) , Bi(Mdb,Ndb,Nt)
      DIMENSION Wshsgs(Lshsgs) , Work(Lwork)
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Isym==0 .AND. Idiv<Nlat) .OR. (Isym>0 .AND. Idiv<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdiv<Nlon ) RETURN
      Ierror = 7
      IF ( Mdb<MIN0(Nlat,(Nlon+1)/2) ) RETURN
      mmax = MIN0(Nlat,(Nlon+2)/2)
      Ierror = 8
      IF ( Ndb<Nlat ) RETURN
      Ierror = 9
      imid = (Nlat+1)/2
      lpimn = (imid*mmax*(Nlat+Nlat-mmax+1))/2
!     check permanent work space length
      l2 = (Nlat+1)/2
      l1 = MIN0((Nlon+2)/2,Nlat)
      lp = Nlat*(3*(l1+l2)-2) + (l1-1)*(l2*(2*Nlat-l1)-3*l1)/2 + Nlon + &
         & 15
      IF ( Lshsgs<lp ) RETURN
      Ierror = 10
!
!     verify unsaved work space (add to what shses requires, file f3)
!
      ls = Nlat
      IF ( Isym>0 ) ls = imid
      nln = Nt*ls*Nlon
!
!     set first dimension for a,b (as requried by shses)
!
      mab = MIN0(Nlat,Nlon/2+1)
      mn = mab*Nlat*Nt
      IF ( Lwork<nln+ls*Nlon+2*mn+Nlat ) RETURN
      Ierror = 0
!
!     set work space pointers
!
      ia = 1
      ib = ia + mn
      is = ib + mn
      iwk = is + Nlat
      lwk = Lwork - 2*mn - Nlat
      CALL DIVGS1(Nlat,Nlon,Isym,Nt,Divg,Idiv,Jdiv,Br,Bi,Mdb,Ndb,       &
                & Work(ia),Work(ib),mab,Work(is),Wshsgs,Lshsgs,Work(iwk)&
                & ,lwk,Ierror)
    END SUBROUTINE DIVGS
 
    SUBROUTINE DIVGS1(Nlat,Nlon,Isym,Nt,Divg,Idiv,Jdiv,Br,Bi,Mdb,Ndb, &
                      & A,B,Mab,Sqnn,Wshsgs,Lshsgs,Wk,Lwk,Ierror)
      IMPLICIT NONE
!*--DIVGS1286
      REAL A , B , Bi , Br , Divg , fn , Sqnn , Wk , Wshsgs
      INTEGER Idiv , Ierror , Isym , Jdiv , k , Lshsgs , Lwk , m , Mab ,&
            & Mdb , mmax , n , Ndb , Nlat , Nlon , Nt
      DIMENSION Divg(Idiv,Jdiv,Nt) , Br(Mdb,Ndb,Nt) , Bi(Mdb,Ndb,Nt)
      DIMENSION A(Mab,Nlat,Nt) , B(Mab,Nlat,Nt) , Sqnn(Nlat)
      DIMENSION Wshsgs(Lshsgs) , Wk(Lwk)
!
!     set coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Sqnn(n) = SQRT(fn*(fn+1.))
      ENDDO
!
!     compute divergence scalar coefficients for each vector field
!
      DO k = 1 , Nt
         DO n = 1 , Nlat
            DO m = 1 , Mab
               A(m,n,k) = 0.0
               B(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            A(1,n,k) = -Sqnn(n)*Br(1,n,k)
            B(1,n,k) = -Sqnn(n)*Bi(1,n,k)
         ENDDO
!
!     compute m>0 coefficients using vector spherepack value for mmax
!
         mmax = MIN0(Nlat,(Nlon+1)/2)
         DO m = 2 , mmax
            DO n = m , Nlat
               A(m,n,k) = -Sqnn(n)*Br(m,n,k)
               B(m,n,k) = -Sqnn(n)*Bi(m,n,k)
            ENDDO
         ENDDO
      ENDDO
!
!     synthesize a,b into divg
!
      CALL SHSGS(Nlat,Nlon,Isym,Nt,Divg,Idiv,Jdiv,A,B,Mab,Nlat,Wshsgs,  &
               & Lshsgs,Wk,Lwk,Ierror)
    END SUBROUTINE DIVGS1


    
    SUBROUTINE DDIVGS(Nlat,Nlon,Isym,Nt,Divg,Idiv,Jdiv,Br,Bi,Mdb,Ndb,  &
                     & Wshsgs,Lshsgs,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Br , Divg , Work , Wshsgs
      INTEGER ia , ib , Idiv , Ierror , imid , is , Isym , iwk , Jdiv , &
            & l1 , l2 , lp , lpimn , ls , Lshsgs , lwk , Lwork , mab ,  &
            & Mdb , mmax
      INTEGER mn , Ndb , Nlat , nln , Nlon , Nt
 
      DIMENSION Divg(Idiv,Jdiv,Nt) , Br(Mdb,Ndb,Nt) , Bi(Mdb,Ndb,Nt)
      DIMENSION Wshsgs(Lshsgs) , Work(Lwork)
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Isym==0 .AND. Idiv<Nlat) .OR. (Isym>0 .AND. Idiv<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdiv<Nlon ) RETURN
      Ierror = 7
      IF ( Mdb<MIN0(Nlat,(Nlon+1)/2) ) RETURN
      mmax = MIN0(Nlat,(Nlon+2)/2)
      Ierror = 8
      IF ( Ndb<Nlat ) RETURN
      Ierror = 9
      imid = (Nlat+1)/2
      lpimn = (imid*mmax*(Nlat+Nlat-mmax+1))/2
!     check permanent work space length
      l2 = (Nlat+1)/2
      l1 = MIN0((Nlon+2)/2,Nlat)
      lp = Nlat*(3*(l1+l2)-2) + (l1-1)*(l2*(2*Nlat-l1)-3*l1)/2 + Nlon + &
         & 15
      IF ( Lshsgs<lp ) RETURN
      Ierror = 10
!
!     verify unsaved work space (add to what shses requires, file f3)
!
      ls = Nlat
      IF ( Isym>0 ) ls = imid
      nln = Nt*ls*Nlon
!
!     set first dimension for a,b (as requried by shses)
!
      mab = MIN0(Nlat,Nlon/2+1)
      mn = mab*Nlat*Nt
      IF ( Lwork<nln+ls*Nlon+2*mn+Nlat ) RETURN
      Ierror = 0
!
!     set work space pointers
!
      ia = 1
      ib = ia + mn
      is = ib + mn
      iwk = is + Nlat
      lwk = Lwork - 2*mn - Nlat
      CALL DDIVGS1(Nlat,Nlon,Isym,Nt,Divg,Idiv,Jdiv,Br,Bi,Mdb,Ndb,       &
                & Work(ia),Work(ib),mab,Work(is),Wshsgs,Lshsgs,Work(iwk)&
                & ,lwk,Ierror)
    END SUBROUTINE DDIVGS

    
    SUBROUTINE DDIVGS1(Nlat,Nlon,Isym,Nt,Divg,Idiv,Jdiv,Br,Bi,Mdb,Ndb, &
                      & A,B,Mab,Sqnn,Wshsgs,Lshsgs,Wk,Lwk,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION A , B , Bi , Br , Divg , fn , Sqnn , Wk , Wshsgs
      INTEGER Idiv , Ierror , Isym , Jdiv , k , Lshsgs , Lwk , m , Mab ,&
            & Mdb , mmax , n , Ndb , Nlat , Nlon , Nt
      DIMENSION Divg(Idiv,Jdiv,Nt) , Br(Mdb,Ndb,Nt) , Bi(Mdb,Ndb,Nt)
      DIMENSION A(Mab,Nlat,Nt) , B(Mab,Nlat,Nt) , Sqnn(Nlat)
      DIMENSION Wshsgs(Lshsgs) , Wk(Lwk)
!
!     set coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Sqnn(n) = SQRT(fn*(fn+1.))
      ENDDO
!
!     compute divergence scalar coefficients for each vector field
!
      DO k = 1 , Nt
         DO n = 1 , Nlat
            DO m = 1 , Mab
               A(m,n,k) = 0.0
               B(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            A(1,n,k) = -Sqnn(n)*Br(1,n,k)
            B(1,n,k) = -Sqnn(n)*Bi(1,n,k)
         ENDDO
!
!     compute m>0 coefficients using vector spherepack value for mmax
!
         mmax = MIN0(Nlat,(Nlon+1)/2)
         DO m = 2 , mmax
            DO n = m , Nlat
               A(m,n,k) = -Sqnn(n)*Br(m,n,k)
               B(m,n,k) = -Sqnn(n)*Bi(m,n,k)
            ENDDO
         ENDDO
      ENDDO
!
!     synthesize a,b into divg
!
      CALL DSHSGS(Nlat,Nlon,Isym,Nt,Divg,Idiv,Jdiv,A,B,Mab,Nlat,Wshsgs,  &
               & Lshsgs,Wk,Lwk,Ierror)
    END SUBROUTINE DDIVGS1
