!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
! ... file idivec.f
!
!     this file includes documentation and code for
!     subroutine idivec          i
!
! ... files which must be loaded with idivec.f
!
!     sphcom.f, hrfft.f, vhsec.f,shaec.f
!
!
!
!     subroutine idivec(nlat,nlon,isym,nt,v,w,idvw,jdvw,a,b,mdab,ndab,
!    +                  wvhsec,lvhsec,work,lwork,pertrb,ierror)
!
!     given the scalar spherical harmonic coefficients a and b, precomputed
!     by subroutine shaec for a scalar array dv, subroutine idivec computes
!     an irrotational vector field (v,w) whose divergence is dv - pertrb.
!     w is the east longitude component and v is the colatitudinal component.
!     pertrb is a constant which must be subtracted from dv for (v,w) to
!     exist (see the description of pertrb below).  usually pertrb is zero
!     or small relative to dv.  the vorticity of (v,w), as computed by
!     vortec, is the zero scalar field.  v(i,j) and w(i,j) are the
!     velocity components at colatitude
!
!            theta(i) = (i-1)*pi/(nlat-1)
!
!     and longitude
!
!            lambda(j) = (j-1)*2*pi/nlon.
!
!     the
!
!            divergence[v(i,j),w(i,j)]
!
!          = [d(w(i,j)/dlambda + d(sint*v(i,j))/dtheta]/sint
!
!          = dv(i,j) - pertrb
!
!     and
!
!            vorticity(v(i,j),w(i,j))
!
!         =  [dv/dlambda - d(sint*w)/dtheta]/sint
!
!         =  0.0
!
!     where sint = sin(theta(i)).  required associated legendre polynomials
!     are recomputed rather than stored as they are in subroutine idives.
!
!     input parameters
!
!
!     nlat   the number of colatitudes on the full sphere including the
!            poles. for example, nlat = 37 for a five degree grid.
!            nlat determines the grid increment in colatitude as
!            pi/(nlat-1).  if nlat is odd the equator is located at
!            grid point i=(nlat+1)/2. if nlat is even the equator is
!            located half way between points i=nlat/2 and i=nlat/2+1.
!            nlat must be at least 3. note: on the half sphere, the
!            number of grid points in the colatitudinal direction is
!            nlat/2 if nlat is even or (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater than
!            3.  the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!
!     isym   this has the same value as the isym that was input to
!            subroutine shaec to compute the arrays a and b from the
!            scalar field dv.  isym determines whether (v,w) are
!            computed on the full or half sphere as follows:
!
!      = 0
!
!           dv is not symmetric about the equator. in this case
!           the vector field (v,w) is computed on the entire sphere.
!           i.e., in the arrays  v(i,j),w(i,j) for i=1,...,nlat and
!           j=1,...,nlon.
!
!      = 1
!
!           dv is antisymmetric about the equator. in this case w is
!           antisymmetric and v is symmetric about the equator. w
!           and v are computed on the northern hemisphere only.  i.e.,
!           if nlat is odd they are computed for i=1,...,(nlat+1)/2
!           and j=1,...,nlon.  if nlat is even they are computed for
!           i=1,...,nlat/2 and j=1,...,nlon.
!
!      = 2
!
!           dv is symmetric about the equator. in this case w is
!           symmetric and v is antisymmetric about the equator. w
!           and v are computed on the northern hemisphere only.  i.e.,
!           if nlat is odd they are computed for i=1,...,(nlat+1)/2
!           and j=1,...,nlon.  if nlat is even they are computed for
!           i=1,...,nlat/2 and j=1,...,nlon.
!
!
!     nt     nt is the number of divergence and vector fields.  some
!            computational efficiency is obtained for multiple fields.
!            the arrays a,b,v, and w can be three dimensional and pertrb
!            can be one dimensional corresponding to an indexed multiple
!            array dv.  in this case, multiple vector synthesis will be
!            performed to compute each vector field.  the third index for
!            a,b,v,w and first for pertrb is the synthesis index which
!            assumes the values k = 1,...,nt.  for a single synthesis set
!            nt = 1.  the description of the remaining parameters is
!            simplified by assuming that nt=1 or that a,b,v,w are two
!            dimensional and pertrb is a constant.
!
!     idvw   the first dimension of the arrays v,w as it appears in
!            the program that calls idivec. if isym = 0 then idvw
!            must be at least nlat.  if isym = 1 or 2 and nlat is
!            even then idvw must be at least nlat/2. if isym = 1 or 2
!            and nlat is odd then idvw must be at least (nlat+1)/2.
!
!     jdvw   the second dimension of the arrays v,w as it appears in
!            the program that calls idivec. jdvw must be at least nlon.
!
!     a,b    two or three dimensional arrays (see input parameter nt)
!            that contain scalar spherical harmonic coefficients
!            of the divergence array dv as computed by subroutine shaec.
!     ***    a,b must be computed by shaec prior to calling idivec.
!
!     mdab   the first dimension of the arrays a and b as it appears in
!            the program that calls idivec (and shaec). mdab must be at
!            least min0(nlat,(nlon+2)/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!     ndab   the second dimension of the arrays a and b as it appears in
!            the program that calls idivec (and shaec). ndab must be at
!            least nlat.
!
!
!  wvhsec    an array which must be initialized by subroutine vhseci.
!            once initialized,
!            wvhsec can be used repeatedly by idivec as long as nlon
!            and nlat remain unchanged.  wvhsec must not be altered
!            between calls of idivec.
!
!
!  lvhsec    the dimension of the array wvhsec as it appears in the
!            program that calls idivec. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lvhsec must be at least
!
!            4*nlat*l2+3*max0(l1-2,0)*(nlat+nlat-l1-1)+nlon+15
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls idivec. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2                  if nlat is even or
!               l2 = (nlat+1)/2              if nlat is odd
!
!            if isym = 0 then lwork must be at least
!
!               nlat*(2*nt*nlon+max0(6*l2,nlon) + 2*nt*l1 + 1)
!
!            if isym = 1 or 2 then lwork must be at least
!
!               l2*(2*nt*nlon+max0(6*nlat,nlon)) + nlat*(2*l1*nt+1)
!
!     **************************************************************
!
!     output parameters
!
!
!     v,w   two or three dimensional arrays (see input parameter nt) that
!           contain an irrotational vector field whose divergence is
!           dv-pertrb at the colatitude point theta(i)=(i-1)*pi/(nlat-1)
!           and longitude point lambda(j)=(j-1)*2*pi/nlon.  w is the east
!           longitude component and v is the colatitudinal component.  the
!           indices for w and v are defined at the input parameter isym.
!           the curl or vorticity of (v,w) is the zero vector field.  note
!           that any nonzero vector field on the sphere will be multiple
!           valued at the poles [reference swarztrauber].
!
!   pertrb  a nt dimensional array (see input parameter nt and assume nt=1
!           for the description that follows).  dv - pertrb is a scalar
!           field which can be the divergence of a vector field (v,w).
!           pertrb is related to the scalar harmonic coefficients a,b
!           of dv (computed by shaec) by the formula
!
!                pertrb = a(1,1)/(2.*sqrt(2.))
!
!
!
!           the unperturbed scalar field dv can be the divergence of a
!           vector field only if a(1,1) is zero.  if a(1,1) is nonzero
!           (flagged by pertrb nonzero) then subtracting pertrb from
!           dv yields a scalar field for which a(1,1) is zero.
!
!    ierror = 0  no errors
!           = 1  error in the specification of nlat
!           = 2  error in the specification of nlon
!           = 3  error in the specification of isym
!           = 4  error in the specification of nt
!           = 5  error in the specification of idvw
!           = 6  error in the specification of jdvw
!           = 7  error in the specification of mdab
!           = 8  error in the specification of ndab
!           = 9  error in the specification of lvhsec
!           = 10 error in the specification of lwork
! **********************************************************************
!
!
      SUBROUTINE IDIVEC(Nlat,Nlon,Isym,Nt,V,W,Idvw,Jdvw,A,B,Mdab,Ndab,  &
                      & Wvhsec,Lvhsec,Work,Lwork,Pertrb,Ierror)
      IMPLICIT NONE

      REAL A , B , Pertrb , V , W , Work , Wvhsec
      INTEGER ibi , ibr , Idvw , idz , Ierror , imid , is , Isym , iwk ,&
            & Jdvw , l1 , l2 , liwk , Lvhsec , lwmin , Lwork , lzimn ,  &
            & Mdab , mmax , mn
      INTEGER Ndab , Nlat , Nlon , Nt

      DIMENSION V(Idvw,Jdvw,Nt) , W(Idvw,Jdvw,Nt) , Pertrb(Nt)
      DIMENSION A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt)
      DIMENSION Wvhsec(Lvhsec) , Work(Lwork)
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Isym==0 .AND. Idvw<Nlat) .OR. (Isym/=0 .AND. Idvw<imid) )   &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdab<MIN0(Nlat,(Nlon+2)/2) ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
      l1 = MIN0(Nlat,(Nlon+1)/2)
      l2 = (Nlat+1)/2
      lwmin = 4*Nlat*l2 + 3*MAX0(l1-2,0)*(Nlat+Nlat-l1-1) + Nlon + 15
      IF ( Lvhsec<lwmin ) RETURN
      Ierror = 10
!
!     verify unsaved work space length
!
      mn = mmax*Nlat*Nt
      IF ( Isym/=0 .AND. Lwork<Nlat*(2*Nt*Nlon+MAX0(6*imid,Nlon))       &
         & +2*mn+Nlat ) RETURN
      IF ( Isym==0 .AND. Lwork<imid*(2*Nt*Nlon+MAX0(6*Nlat,Nlon))       &
         & +2*mn+Nlat ) RETURN
      Ierror = 0
!
!     set work space pointers
!
      ibr = 1
      ibi = ibr + mn
      is = ibi + mn
      iwk = is + Nlat
      liwk = Lwork - 2*mn - Nlat
      CALL IDVEC1(Nlat,Nlon,Isym,Nt,V,W,Idvw,Jdvw,Work(ibr),Work(ibi),  &
                & mmax,Work(is),Mdab,Ndab,A,B,Wvhsec,Lvhsec,Work(iwk),  &
                & liwk,Pertrb,Ierror)
    END SUBROUTINE IDIVEC

    
    SUBROUTINE IDVEC1(Nlat,Nlon,Isym,Nt,V,W,Idvw,Jdvw,Br,Bi,Mmax,Sqnn,&
                      & Mdab,Ndab,A,B,Wvhsec,Lvhsec,Wk,Lwk,Pertrb,      &
                      & Ierror)
      IMPLICIT NONE

      REAL A , B , Bi , Br , ci , cr , fn , Pertrb , Sqnn , V , W , Wk ,&
         & Wvhsec
      INTEGER Idvw , Ierror , Isym , ityp , Jdvw , k , Lvhsec , Lwk ,   &
            & m , Mdab , Mmax , n , Ndab , Nlat , Nlon , Nt

      DIMENSION V(Idvw,Jdvw,Nt) , W(Idvw,Jdvw,Nt) , Pertrb(Nt)
      DIMENSION Br(Mmax,Nlat,Nt) , Bi(Mmax,Nlat,Nt) , Sqnn(Nlat)
      DIMENSION A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt)
      DIMENSION Wvhsec(Lvhsec) , Wk(Lwk)
!     preset coefficient multiplyers in vector
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Sqnn(n) = SQRT(fn*(fn+1.))
      ENDDO
!
!     compute multiple vector fields coefficients
!
      DO k = 1 , Nt
!
!     set divergence field perturbation adjustment
!
         Pertrb(k) = A(1,1,k)/(2.*SQRT(2.))
!
!     preset br,bi to 0.0
!
         DO n = 1 , Nlat
            DO m = 1 , Mmax
               Br(m,n,k) = 0.0
               Bi(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            Br(1,n,k) = -A(1,n,k)/Sqnn(n)
            Bi(1,n,k) = -B(1,n,k)/Sqnn(n)
         ENDDO
!
!     compute m>0 coefficients
!
         DO m = 2 , Mmax
            DO n = m , Nlat
               Br(m,n,k) = -A(m,n,k)/Sqnn(n)
               Bi(m,n,k) = -B(m,n,k)/Sqnn(n)
            ENDDO
         ENDDO
      ENDDO
!
!     set ityp for vector synthesis with curl=0
!
      IF ( Isym==0 ) THEN
         ityp = 1
      ELSEIF ( Isym==1 ) THEN
         ityp = 4
      ELSEIF ( Isym==2 ) THEN
         ityp = 7
      ENDIF
!
!     vector sythesize br,bi into irrotational (v,w)
!
      CALL VHSEC(Nlat,Nlon,ityp,Nt,V,W,Idvw,Jdvw,Br,Bi,cr,ci,Mmax,Nlat, &
               & Wvhsec,Lvhsec,Wk,Lwk,Ierror)
      END SUBROUTINE IDVEC1

      
      SUBROUTINE DIDIVEC(Nlat,Nlon,Isym,Nt,V,W,Idvw,Jdvw,A,B,Mdab,Ndab,  &
                      & Wvhsec,Lvhsec,Work,Lwork,Pertrb,Ierror)
      IMPLICIT NONE

      DOUBLE PRECISION A , B , Pertrb , V , W , Work , Wvhsec
      INTEGER ibi , ibr , Idvw , idz , Ierror , imid , is , Isym , iwk ,&
            & Jdvw , l1 , l2 , liwk , Lvhsec , lwmin , Lwork , lzimn ,  &
            & Mdab , mmax , mn
      INTEGER Ndab , Nlat , Nlon , Nt

      DIMENSION V(Idvw,Jdvw,Nt) , W(Idvw,Jdvw,Nt) , Pertrb(Nt)
      DIMENSION A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt)
      DIMENSION Wvhsec(Lvhsec) , Work(Lwork)
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Isym==0 .AND. Idvw<Nlat) .OR. (Isym/=0 .AND. Idvw<imid) )   &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdab<MIN0(Nlat,(Nlon+2)/2) ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
      l1 = MIN0(Nlat,(Nlon+1)/2)
      l2 = (Nlat+1)/2
      lwmin = 4*Nlat*l2 + 3*MAX0(l1-2,0)*(Nlat+Nlat-l1-1) + Nlon + 15
      IF ( Lvhsec<lwmin ) RETURN
      Ierror = 10
!
!     verify unsaved work space length
!
      mn = mmax*Nlat*Nt
      IF ( Isym/=0 .AND. Lwork<Nlat*(2*Nt*Nlon+MAX0(6*imid,Nlon))       &
         & +2*mn+Nlat ) RETURN
      IF ( Isym==0 .AND. Lwork<imid*(2*Nt*Nlon+MAX0(6*Nlat,Nlon))       &
         & +2*mn+Nlat ) RETURN
      Ierror = 0
!
!     set work space pointers
!
      ibr = 1
      ibi = ibr + mn
      is = ibi + mn
      iwk = is + Nlat
      liwk = Lwork - 2*mn - Nlat
      CALL DIDVEC1(Nlat,Nlon,Isym,Nt,V,W,Idvw,Jdvw,Work(ibr),Work(ibi),  &
                & mmax,Work(is),Mdab,Ndab,A,B,Wvhsec,Lvhsec,Work(iwk),  &
                & liwk,Pertrb,Ierror)
    END SUBROUTINE DIDIVEC

    
    SUBROUTINE DIDVEC1(Nlat,Nlon,Isym,Nt,V,W,Idvw,Jdvw,Br,Bi,Mmax,Sqnn,&
                      & Mdab,Ndab,A,B,Wvhsec,Lvhsec,Wk,Lwk,Pertrb,      &
                      & Ierror)
      IMPLICIT NONE

      DOUBLE PRECISION A , B , Bi , Br , ci , cr , fn , Pertrb , Sqnn , V , W , Wk ,&
         & Wvhsec
      INTEGER Idvw , Ierror , Isym , ityp , Jdvw , k , Lvhsec , Lwk ,   &
            & m , Mdab , Mmax , n , Ndab , Nlat , Nlon , Nt

      DIMENSION V(Idvw,Jdvw,Nt) , W(Idvw,Jdvw,Nt) , Pertrb(Nt)
      DIMENSION Br(Mmax,Nlat,Nt) , Bi(Mmax,Nlat,Nt) , Sqnn(Nlat)
      DIMENSION A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt)
      DIMENSION Wvhsec(Lvhsec) , Wk(Lwk)
!     preset coefficient multiplyers in vector
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Sqnn(n) = SQRT(fn*(fn+1.))
      ENDDO
!
!     compute multiple vector fields coefficients
!
      DO k = 1 , Nt
!
!     set divergence field perturbation adjustment
!
         Pertrb(k) = A(1,1,k)/(2.*SQRT(2.))
!
!     preset br,bi to 0.0
!
         DO n = 1 , Nlat
            DO m = 1 , Mmax
               Br(m,n,k) = 0.0
               Bi(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            Br(1,n,k) = -A(1,n,k)/Sqnn(n)
            Bi(1,n,k) = -B(1,n,k)/Sqnn(n)
         ENDDO
!
!     compute m>0 coefficients
!
         DO m = 2 , Mmax
            DO n = m , Nlat
               Br(m,n,k) = -A(m,n,k)/Sqnn(n)
               Bi(m,n,k) = -B(m,n,k)/Sqnn(n)
            ENDDO
         ENDDO
      ENDDO
!
!     set ityp for vector synthesis with curl=0
!
      IF ( Isym==0 ) THEN
         ityp = 1
      ELSEIF ( Isym==1 ) THEN
         ityp = 4
      ELSEIF ( Isym==2 ) THEN
         ityp = 7
      ENDIF
!
!     vector sythesize br,bi into irrotational (v,w)
!
      CALL DVHSEC(Nlat,Nlon,ityp,Nt,V,W,Idvw,Jdvw,Br,Bi,cr,ci,Mmax,Nlat, &
               & Wvhsec,Lvhsec,Wk,Lwk,Ierror)
      END SUBROUTINE DIDVEC1
