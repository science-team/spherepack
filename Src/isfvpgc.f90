!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
! ... file isfvpgc.f
!
!     this file includes documentation and code for
!     subroutine isfvpgc          i
!
! ... files which must be loaded with isfvpgc.f
!
!     sphcom.f, hrfft.f, vhsgc.f, shagc.f, gaqd.f
!
!
!     subroutine isfvpgc(nlat,nlon,isym,nt,sf,vp,idv,jdv,as,bs,av,bv,
!    +                   mdb,ndb,wvhsgc,lvhsgc,work,lwork,ierror)
!
!     given the scalar spherical harmonic coefficients as,bs precomputed
!     by shagc for the scalar stream function sf and av,bv precomputed by
!     shagc for the scalar velocity potenital vp, subroutine isfvpgc computes
!     the vector field (v,w) corresponding to sf and vp.  w is the east
!     longitudinal and v is the colatitudinal component of the vector field.
!     (v,w) is expressed in terms of sf,vp by the helmholtz relations (in
!     mathematical spherical coordinates):
!
!          v = -1/sin(theta)*d(vp)/dlambda + d(st)/dtheta
!
!          w =  1/sin(theta)*d(st)/dlambda + d(vp)/dtheta
!
!     required legendre functions are recomputed rather than stored as
!     they are in subroutine isfvpgs.  v(i,j) and w(i,j) are given at
!     the i(th) gaussian colatitude point (see gaqd) theta(i) and east
!     longitude lambda(j) = (j-1)*2.*pi/nlon on the sphere.
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are computed
!            in radians in theta(1) <...< theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid point
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater than
!            3.  the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!
!     isym   a parameter which determines whether the vector field is
!            computed on the full or half sphere as follows:
!
!      = 0
!
!            the symmetries/antsymmetries described in isym=1,2 below
!            do not exist in sf,vp about the equator.  in this case v
!            and w are not necessarily symmetric or antisymmetric about
!            equator.  v and w are computed on the entire sphere.
!            i.e., in arrays sf(i,j),vp(i,j) for i=1,...,nlat and
!            j=1,...,nlon.
!
!      = 1
!
!            vp is antisymmetric and sf is symmetric about the equator.
!            in this case v is symmetric and w antisymmetric about
!            the equator and are computed for the northern hemisphere
!            only.  i.e., if nlat is odd the v(i,j),w(i,j) are computed
!            for i=1,...,(nlat+1)/2 and for j=1,...,nlon.  if nlat is
!            even then v(i,j),w(i,j) are computed for i=1,...,nlat/2
!            and j=1,...,nlon.
!
!      = 2
!
!            vp is symmetric and sf is antisymmetric about the equator.
!            in this case v is antisymmetric and w symmetric about
!            the equator and are computed for the northern hemisphere
!            only.  i.e., if nlat is odd the v(i,j),w(i,j) are computed
!            for i=1,...,(nlat+1)/2 and for j=1,...,nlon.  if nlat is
!            even then v(i,j),w(i,j) are computed for i=1,...,nlat/2
!            and j=1,...,nlon.
!
!     nt     nt is the number of scalar and vector fields.  some
!            computational efficiency is obtained for multiple fields. arrays
!            can be three dimensional corresponding to an indexed multiple
!            vector field.  in this case multiple vector synthesis will
!            be performed to compute (v,w) for each field.  the
!            third index is the synthesis index which assumes the values
!            k=1,...,nt.  for a single synthesis set nt = 1.  the
!            description of the remaining parameters is simplified by
!            assuming that nt=1 or that all the arrays are two dimensional.
!
!     idv    the first dimension of the arrays v,w as it appears in
!            the program that calls isfvpgc. if isym = 0 then idv
!            must be at least nlat.  if isym = 1 or 2 and nlat is
!            even then idv must be at least nlat/2. if isym = 1 or 2
!            and nlat is odd then idv must be at least (nlat+1)/2.
!
!     jdv    the second dimension of the arrays v,w as it appears in
!            the program that calls isfvpgc. jdv must be at least nlon.
!
!     as,bs  two or three dimensional arrays (see input parameter nt)
!            that contain the spherical harmonic coefficients of
!            the scalar field sf as computed by subroutine shagc.
!
!     av,bv  two or three dimensional arrays (see input parameter nt)
!            that contain the spherical harmonic coefficients of
!            the scalar field vp as computed by subroutine shagc.
!
!     mdb    the first dimension of the arrays as,bs,av,bv as it
!            appears in the program that calls isfvpgc. mdb must be at
!            least min0(nlat,nlon/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!     ndb    the second dimension of the arrays as,bs,av,bv as it
!            appears in the program that calls isfvpgc. ndb must be at
!            least nlat.
!
!     wvhsgc an array which must be initialized by subroutine vhsgci.
!            once initialized, wvhsgc can be used repeatedly by isfvpgc
!            as long as nlon and nlat remain unchanged.  wvhsgc must
!            not bel altered between calls of isfvpgc.
!
!
!     lvhsgc the dimension of the array wvhsgc as it appears in the
!            program that calls isfvpgc. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lvhsgc must be at least
!
!
!               4*nlat*l2+3*max0(l1-2,0)*(nlat+nlat-l1-1)+nlon+15
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls isfvpgc. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2                    if nlat is even or
!               l2 = (nlat+1)/2                if nlat is odd
!
!            if isym = 0 then lwork must be at least
!
!               nlat*(2*nt*nlon+max0(6*l2,nlon)+4*l1*nt+1)
!
!            if isym = 1 or 2 then lwork must be at least
!
!               l2*(2*nt*nlon+max0(6*nlat,nlon))+nlat*(4*l1*nt+1)
!
!     **************************************************************
!
!     output parameters
!
!    v,w    two or three dimensional arrays (see input parameter nt)
!           that contains the vector field corresponding to the stream
!           function sf and velocity potential vp whose coefficients,
!           as,bs (for sf) and av,bv (for vp), were precomputed by
!           subroutine shagc.  v(i,j) and w(i,j) are given at the
!           i(th) gaussian colatitude point theta(i) and east longitude
!           point lambda(j) = (j-1)*2*pi/nlon.  the index ranges are
!           defined above at the input parameter isym.
!
!
!    ierror = 0  no errors
!           = 1  error in the specification of nlat
!           = 2  error in the specification of nlon
!           = 3  error in the specification of isym
!           = 4  error in the specification of nt
!           = 5  error in the specification of idv
!           = 6  error in the specification of jdv
!           = 7  error in the specification of mdb
!           = 8  error in the specification of ndb
!           = 9  error in the specification of lvhsgc
!           = 10 error in the specification of lwork
! **********************************************************************
!
      SUBROUTINE ISFVPGC(Nlat,Nlon,Isym,Nt,V,W,Idv,Jdv,As,Bs,Av,Bv,Mdb, &
                       & Ndb,Wvhsgc,Lvhsgc,Work,Lwork,Ierror)
      IMPLICIT NONE
      INTEGER Nlat , Nlon , Isym , Nt , Idv , Jdv , Mdb , Ndb , Lvhsgc ,&
            & Lwork , Ierror
      REAL V(Idv,Jdv,Nt) , W(Idv,Jdv,Nt)
      REAL As(Mdb,Ndb,Nt) , Bs(Mdb,Ndb,Nt)
      REAL Av(Mdb,Ndb,Nt) , Bv(Mdb,Ndb,Nt)
      REAL Wvhsgc(Lvhsgc) , Work(Lwork)
      INTEGER l1 , l2 , mn , is , lwk , iwk , lwmin
      INTEGER ibr , ibi , icr , ici
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      l2 = (Nlat+1)/2
      IF ( (Isym==0 .AND. Idv<Nlat) .OR. (Isym>0 .AND. Idv<l2) ) RETURN
      Ierror = 6
      IF ( Jdv<Nlon ) RETURN
      Ierror = 7
      l1 = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdb<MIN0(Nlat,(Nlon+2)/2) ) RETURN
      Ierror = 8
      IF ( Ndb<Nlat ) RETURN
      Ierror = 9
      lwmin = 4*Nlat*l2 + 3*MAX0(l1-2,0)*(Nlat+Nlat-l1-1) + Nlon + 15
      IF ( Lvhsgc<lwmin ) RETURN
      Ierror = 10
      IF ( Isym==0 ) THEN
         lwmin = Nlat*(2*Nt*Nlon+MAX0(6*l2,Nlon)+4*l1*Nt+1)
      ELSE
         lwmin = l2*(2*Nt*Nlon+MAX0(6*Nlat,Nlon)) + Nlat*(4*l1*Nt+1)
      ENDIF
      IF ( Lwork<lwmin ) RETURN
!
!     set first dimension for br,bi,cr,ci (as requried by vhsgc)
!
      mn = l1*Nlat*Nt
      Ierror = 0
!
!     set work space pointers
!
      ibr = 1
      ibi = ibr + mn
      icr = ibi + mn
      ici = icr + mn
      is = ici + mn
      iwk = is + Nlat
      lwk = Lwork - 4*mn - Nlat
      CALL ISFVPGC1(Nlat,Nlon,Isym,Nt,V,W,Idv,Jdv,As,Bs,Av,Bv,Mdb,Ndb,  &
                  & Work(ibr),Work(ibi),Work(icr),Work(ici),l1,Work(is),&
                  & Wvhsgc,Lvhsgc,Work(iwk),lwk,Ierror)
    END SUBROUTINE ISFVPGC
 
      SUBROUTINE ISFVPGC1(Nlat,Nlon,Isym,Nt,V,W,Idv,Jdv,As,Bs,Av,Bv,Mdb,&
                        & Ndb,Br,Bi,Cr,Ci,Mab,Fnn,Wvhsgc,Lvhsgc,Wk,Lwk, &
                        & Ierror)
      IMPLICIT NONE

      INTEGER Nlat , Nlon , Isym , Nt , Idv , Jdv , Mdb , Ndb , Mab ,   &
            & Lvhsgc , Lwk , Ierror
      REAL V(Idv,Jdv,Nt) , W(Idv,Jdv,Nt)
      REAL As(Mdb,Ndb,Nt) , Bs(Mdb,Ndb,Nt)
      REAL Av(Mdb,Ndb,Nt) , Bv(Mdb,Ndb,Nt)
      REAL Br(Mab,Nlat,Nt) , Bi(Mab,Nlat,Nt)
      REAL Cr(Mab,Nlat,Nt) , Ci(Mab,Nlat,Nt)
      REAL Wvhsgc(Lvhsgc) , Wk(Lwk) , Fnn(Nlat)
      INTEGER n , m , mmax , k , ityp
!
!     set coefficient multiplyers
!
      DO n = 2 , Nlat
         Fnn(n) = -SQRT(FLOAT(n*(n-1)))
      ENDDO
      mmax = MIN0(Nlat,(Nlon+1)/2)
!
!     compute (v,w) coefficients from as,bs,av,bv
!
      DO k = 1 , Nt
         DO n = 1 , Nlat
            DO m = 1 , Mab
               Br(m,n,k) = 0.0
               Bi(m,n,k) = 0.0
               Cr(m,n,k) = 0.0
               Ci(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            Br(1,n,k) = -Fnn(n)*Av(1,n,k)
            Bi(1,n,k) = -Fnn(n)*Bv(1,n,k)
            Cr(1,n,k) = Fnn(n)*As(1,n,k)
            Ci(1,n,k) = Fnn(n)*Bs(1,n,k)
         ENDDO
!
!     compute m>0 coefficients using vector spherepack value for mmax
!
         DO m = 2 , mmax
            DO n = m , Nlat
               Br(m,n,k) = -Fnn(n)*Av(m,n,k)
               Bi(m,n,k) = -Fnn(n)*Bv(m,n,k)
               Cr(m,n,k) = Fnn(n)*As(m,n,k)
               Ci(m,n,k) = Fnn(n)*Bs(m,n,k)
            ENDDO
         ENDDO
      ENDDO
!
!     synthesize br,bi,cr,ci into (v,w)
!
      IF ( Isym==0 ) THEN
         ityp = 0
      ELSEIF ( Isym==1 ) THEN
         ityp = 3
      ELSEIF ( Isym==2 ) THEN
         ityp = 6
      ENDIF
      CALL VHSGC(Nlat,Nlon,ityp,Nt,V,W,Idv,Jdv,Br,Bi,Cr,Ci,Mab,Nlat,    &
               & Wvhsgc,Lvhsgc,Wk,Lwk,Ierror)
      END SUBROUTINE ISFVPGC1 

      SUBROUTINE DISFVPGC(Nlat,Nlon,Isym,Nt,V,W,Idv,Jdv,As,Bs,Av,Bv,Mdb, &
                       & Ndb,Wvhsgc,Lvhsgc,Work,Lwork,Ierror)
      IMPLICIT NONE
      INTEGER Nlat , Nlon , Isym , Nt , Idv , Jdv , Mdb , Ndb , Lvhsgc ,&
            & Lwork , Ierror
      DOUBLE PRECISION V(Idv,Jdv,Nt) , W(Idv,Jdv,Nt)
      DOUBLE PRECISION As(Mdb,Ndb,Nt) , Bs(Mdb,Ndb,Nt)
      DOUBLE PRECISION Av(Mdb,Ndb,Nt) , Bv(Mdb,Ndb,Nt)
      DOUBLE PRECISION Wvhsgc(Lvhsgc) , Work(Lwork)
      INTEGER l1 , l2 , mn , is , lwk , iwk , lwmin
      INTEGER ibr , ibi , icr , ici
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      l2 = (Nlat+1)/2
      IF ( (Isym==0 .AND. Idv<Nlat) .OR. (Isym>0 .AND. Idv<l2) ) RETURN
      Ierror = 6
      IF ( Jdv<Nlon ) RETURN
      Ierror = 7
      l1 = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdb<MIN0(Nlat,(Nlon+2)/2) ) RETURN
      Ierror = 8
      IF ( Ndb<Nlat ) RETURN
      Ierror = 9
      lwmin = 4*Nlat*l2 + 3*MAX0(l1-2,0)*(Nlat+Nlat-l1-1) + Nlon + 15
      IF ( Lvhsgc<lwmin ) RETURN
      Ierror = 10
      IF ( Isym==0 ) THEN
         lwmin = Nlat*(2*Nt*Nlon+MAX0(6*l2,Nlon)+4*l1*Nt+1)
      ELSE
         lwmin = l2*(2*Nt*Nlon+MAX0(6*Nlat,Nlon)) + Nlat*(4*l1*Nt+1)
      ENDIF
      IF ( Lwork<lwmin ) RETURN
!
!     set first dimension for br,bi,cr,ci (as requried by vhsgc)
!
      mn = l1*Nlat*Nt
      Ierror = 0
!
!     set work space pointers
!
      ibr = 1
      ibi = ibr + mn
      icr = ibi + mn
      ici = icr + mn
      is = ici + mn
      iwk = is + Nlat
      lwk = Lwork - 4*mn - Nlat
      CALL DISFVPGC1(Nlat,Nlon,Isym,Nt,V,W,Idv,Jdv,As,Bs,Av,Bv,Mdb,Ndb,  &
                  & Work(ibr),Work(ibi),Work(icr),Work(ici),l1,Work(is),&
                  & Wvhsgc,Lvhsgc,Work(iwk),lwk,Ierror)
    END SUBROUTINE DISFVPGC
 
      SUBROUTINE DISFVPGC1(Nlat,Nlon,Isym,Nt,V,W,Idv,Jdv,As,Bs,Av,Bv,Mdb,&
                        & Ndb,Br,Bi,Cr,Ci,Mab,Fnn,Wvhsgc,Lvhsgc,Wk,Lwk, &
                        & Ierror)
      IMPLICIT NONE

      INTEGER Nlat , Nlon , Isym , Nt , Idv , Jdv , Mdb , Ndb , Mab ,   &
            & Lvhsgc , Lwk , Ierror
      DOUBLE PRECISION V(Idv,Jdv,Nt) , W(Idv,Jdv,Nt)
      DOUBLE PRECISION As(Mdb,Ndb,Nt) , Bs(Mdb,Ndb,Nt)
      DOUBLE PRECISION Av(Mdb,Ndb,Nt) , Bv(Mdb,Ndb,Nt)
      DOUBLE PRECISION Br(Mab,Nlat,Nt) , Bi(Mab,Nlat,Nt)
      DOUBLE PRECISION Cr(Mab,Nlat,Nt) , Ci(Mab,Nlat,Nt)
      DOUBLE PRECISION  Wvhsgc(Lvhsgc) , Wk(Lwk) , Fnn(Nlat)
      INTEGER n , m , mmax , k , ityp
!
!     set coefficient multiplyers
!
      DO n = 2 , Nlat
         Fnn(n) = -SQRT(FLOAT(n*(n-1)))
      ENDDO
      mmax = MIN0(Nlat,(Nlon+1)/2)
!
!     compute (v,w) coefficients from as,bs,av,bv
!
      DO k = 1 , Nt
         DO n = 1 , Nlat
            DO m = 1 , Mab
               Br(m,n,k) = 0.0
               Bi(m,n,k) = 0.0
               Cr(m,n,k) = 0.0
               Ci(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            Br(1,n,k) = -Fnn(n)*Av(1,n,k)
            Bi(1,n,k) = -Fnn(n)*Bv(1,n,k)
            Cr(1,n,k) = Fnn(n)*As(1,n,k)
            Ci(1,n,k) = Fnn(n)*Bs(1,n,k)
         ENDDO
!
!     compute m>0 coefficients using vector spherepack value for mmax
!
         DO m = 2 , mmax
            DO n = m , Nlat
               Br(m,n,k) = -Fnn(n)*Av(m,n,k)
               Bi(m,n,k) = -Fnn(n)*Bv(m,n,k)
               Cr(m,n,k) = Fnn(n)*As(m,n,k)
               Ci(m,n,k) = Fnn(n)*Bs(m,n,k)
            ENDDO
         ENDDO
      ENDDO
!
!     synthesize br,bi,cr,ci into (v,w)
!
      IF ( Isym==0 ) THEN
         ityp = 0
      ELSEIF ( Isym==1 ) THEN
         ityp = 3
      ELSEIF ( Isym==2 ) THEN
         ityp = 6
      ENDIF
      CALL DVHSGC(Nlat,Nlon,ityp,Nt,V,W,Idv,Jdv,Br,Bi,Cr,Ci,Mab,Nlat,    &
               & Wvhsgc,Lvhsgc,Wk,Lwk,Ierror)
      END SUBROUTINE DISFVPGC1 
