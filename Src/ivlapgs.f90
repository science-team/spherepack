!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK                          .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
! ... file ivlapgs.f
!
!     this file includes documentation and code for
!     subroutine ivlapgs
!
! ... files which must be loaded with ivlapgs.f
!
!     sphcom.f, hrfft.f, vhags.f, vhsgs.f, gaqd.f
!
!
!     subroutine ivlapgs(nlat,nlon,ityp,nt,v,w,idvw,jdvw,br,bi,cr,ci,
!    +mdbc,ndbc,wvhsgs,lvhsgs,work,lwork,ierror)
!
!     given the vector spherical harmonic coefficients (br,bi,cr,ci)
!     precomputed by subroutine vhags for a vector field (vlap,wlap),
!     subroutine ivlapgs computes a vector field (v,w) whose vector
!     laplacian is (vlap,wlap).  v,vlap are the colatitudinal
!     components and w,wlap are the east longitudinal components of
!     the vectors.  (v,w) have the same symmetry or lack of symmetry
!     about the equator as (vlap,wlap).  the input parameters ityp,
!     nt,mdbc,ndbc must have the same values used by vhags to compute
!     br,bi,cr,ci for (vlap,wlap).
!
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are computed
!            in radians in theta(1) <...< theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid point
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct longitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!*PL*ERROR* Comment line too long
!     ityp   this parameter should have the same value input to subroutine
!            vhags to compute the coefficients br,bi,cr, and ci for the
!            vector field (vlap,wlap).  ityp is set as follows:
!
!*PL*ERROR* Comment line too long
!            = 0  no symmetries exist in (vlap,wlap) about the equator. (v,w)
!*PL*ERROR* Comment line too long
!                 is computed and stored on the entire sphere in the arrays
!*PL*ERROR* Comment line too long
!                 arrays v(i,j) and w(i,j) for i=1,...,nlat and j=1,...,nlon.
!
!*PL*ERROR* Comment line too long
!            = 1  no symmetries exist in (vlap,wlap) about the equator. (v,w)
!*PL*ERROR* Comment line too long
!                 is computed and stored on the entire sphere in the arrays
!*PL*ERROR* Comment line too long
!                 v(i,j) and w(i,j) for i=1,...,nlat and j=1,...,nlon.  the
!*PL*ERROR* Comment line too long
!                 vorticity of (vlap,wlap) is zero so the coefficients cr and
!*PL*ERROR* Comment line too long
!                 ci are zero and are not used.  the vorticity of (v,w) is
!                 also zero.
!
!
!*PL*ERROR* Comment line too long
!            = 2  no symmetries exist in (vlap,wlap) about the equator. (v,w)
!*PL*ERROR* Comment line too long
!                 is computed and stored on the entire sphere in the arrays
!*PL*ERROR* Comment line too long
!                 w(i,j) and v(i,j) for i=1,...,nlat and j=1,...,nlon.  the
!*PL*ERROR* Comment line too long
!                 divergence of (vlap,wlap) is zero so the coefficients br and
!*PL*ERROR* Comment line too long
!                 bi are zero and are not used.  the divergence of (v,w) is
!                 also zero.
!
!            = 3  wlap is antisymmetric and vlap is symmetric about the
!*PL*ERROR* Comment line too long
!                 equator. consequently w is antisymmetric and v is symmetric.
!*PL*ERROR* Comment line too long
!                 (v,w) is computed and stored on the northern hemisphere
!*PL*ERROR* Comment line too long
!                 only.  if nlat is odd, storage is in the arrays v(i,j),
!*PL*ERROR* Comment line too long
!                 w(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.  if nlat
!                 is even, storage is in the arrays v(i,j),w(i,j) for
!                 i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 4  wlap is antisymmetric and vlap is symmetric about the
!*PL*ERROR* Comment line too long
!                 equator. consequently w is antisymmetric and v is symmetric.
!*PL*ERROR* Comment line too long
!                 (v,w) is computed and stored on the northern hemisphere
!*PL*ERROR* Comment line too long
!                 only.  if nlat is odd, storage is in the arrays v(i,j),
!*PL*ERROR* Comment line too long
!                 w(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.  if nlat
!                 is even, storage is in the arrays v(i,j),w(i,j) for
!*PL*ERROR* Comment line too long
!                 i=1,...,nlat/2 and j=1,...,nlon.  the vorticity of (vlap,
!                 wlap) is zero so the coefficients cr,ci are zero and
!                 are not used. the vorticity of (v,w) is also zero.
!
!            = 5  wlap is antisymmetric and vlap is symmetric about the
!*PL*ERROR* Comment line too long
!                 equator. consequently w is antisymmetric and v is symmetric.
!*PL*ERROR* Comment line too long
!                 (v,w) is computed and stored on the northern hemisphere
!*PL*ERROR* Comment line too long
!                 only.  if nlat is odd, storage is in the arrays w(i,j),
!*PL*ERROR* Comment line too long
!                 v(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.  if nlat
!                 is even, storage is in the arrays w(i,j),v(i,j) for
!*PL*ERROR* Comment line too long
!                 i=1,...,nlat/2 and j=1,...,nlon.  the divergence of (vlap,
!                 wlap) is zero so the coefficients br,bi are zero and
!                 are not used. the divergence of (v,w) is also zero.
!
!
!            = 6  wlap is symmetric and vlap is antisymmetric about the
!*PL*ERROR* Comment line too long
!                 equator. consequently w is symmetric and v is antisymmetric.
!*PL*ERROR* Comment line too long
!                 (v,w) is computed and stored on the northern hemisphere
!*PL*ERROR* Comment line too long
!                 only.  if nlat is odd, storage is in the arrays w(i,j),
!*PL*ERROR* Comment line too long
!                 v(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.  if nlat
!                 is even, storage is in the arrays w(i,j),v(i,j) for
!                 i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 7  wlap is symmetric and vlap is antisymmetric about the
!*PL*ERROR* Comment line too long
!                 equator. consequently w is symmetric and v is antisymmetric.
!*PL*ERROR* Comment line too long
!                 (v,w) is computed and stored on the northern hemisphere
!*PL*ERROR* Comment line too long
!                 only.  if nlat is odd, storage is in the arrays w(i,j),
!*PL*ERROR* Comment line too long
!                 v(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.  if nlat
!                 is even, storage is in the arrays w(i,j),v(i,j) for
!*PL*ERROR* Comment line too long
!                 i=1,...,nlat/2 and j=1,...,nlon.  the vorticity of (vlap,
!                 wlap) is zero so the coefficients cr,ci are zero and
!                 are not used. the vorticity of (v,w) is also zero.
!
!            = 8  wlap is symmetric and vlap is antisymmetric about the
!*PL*ERROR* Comment line too long
!                 equator. consequently w is symmetric and v is antisymmetric.
!*PL*ERROR* Comment line too long
!                 (v,w) is computed and stored on the northern hemisphere
!*PL*ERROR* Comment line too long
!                 only.  if nlat is odd, storage is in the arrays w(i,j),
!*PL*ERROR* Comment line too long
!                 v(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.  if nlat
!                 is even, storage is in the arrays w(i,j),v(i,j) for
!*PL*ERROR* Comment line too long
!                 i=1,...,nlat/2 and j=1,...,nlon.  the divergence of (vlap,
!                 wlap) is zero so the coefficients br,bi are zero and
!                 are not used. the divergence of (v,w) is also zero.
!
!
!*PL*ERROR* Comment line too long
!     nt     nt is the number of vector fields (vlap,wlap). some computational
!            efficiency is obtained for multiple fields.  in the program
!            that calls ivlapgs, the arrays v,w,br,bi,cr and ci can be
!*PL*ERROR* Comment line too long
!            three dimensional corresponding to an indexed multiple vector
!*PL*ERROR* Comment line too long
!            field.  in this case multiple vector synthesis will be performed
!            to compute the (v,w) for each field (vlap,wlap).  the third
!*PL*ERROR* Comment line too long
!            index is the synthesis index which assumes the values k=1,...,nt.
!            for a single synthesis set nt=1.  the description of the
!            remaining parameters is simplified by assuming that nt=1 or
!            that all arrays are two dimensional.
!
!   idvw     the first dimension of the arrays w and v as it appears in
!*PL*ERROR* Comment line too long
!            the program that calls ivlapgs.  if ityp=0,1, or 2  then idvw
!*PL*ERROR* Comment line too long
!            must be at least nlat.  if ityp > 2 and nlat is even then idvw
!*PL*ERROR* Comment line too long
!            must be at least nlat/2. if ityp > 2 and nlat is odd then idvw
!            must be at least (nlat+1)/2.
!
!   jdvw     the second dimension of the arrays w and v as it appears in
!            the program that calls ivlapgs. jdvw must be at least nlon.
!
!
!   br,bi    two or three dimensional arrays (see input parameter nt)
!   cr,ci    that contain vector spherical harmonic coefficients of the
!            vector field (vlap,wlap) as computed by subroutine vhags.
!            br,bi,cr and ci must be computed by vhags prior to calling
!            ivlapgs.  if ityp=1,4, or 7 then cr,ci are not used and can
!            be dummy arguments.  if ityp=2,5, or 8 then br,bi are not
!            used and can be dummy arguments.
!
!    mdbc    the first dimension of the arrays br,bi,cr and ci as it
!            appears in the program that calls ivlapgs.  mdbc must be
!            at least min0(nlat,nlon/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!    ndbc    the second dimension of the arrays br,bi,cr and ci as it
!            appears in the program that calls ivlapgs. ndbc must be at
!            least nlat.
!
!    wvhsgs  an array which must be initialized by subroutine vhsgsi.
!            once initialized, wvhsgsi
!            can be used repeatedly by ivlapgs as long as nlat and nlon
!            remain unchanged.  wvhsgs must not be altered between calls
!            of ivlapgs.
!
!    lvhsgs  the dimension of the array wvhsgs as it appears in the
!            program that calls ivlapgs.  let
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd.
!
!            let
!
!               lsavmin = (l1*l2*(nlat+nlat-l1+1))/2+nlon+15
!
!            then lvhsgs must be greater than or equal to lsavmin
!            (see ierror=9 below).
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls ivlapgs. define
!
!               l2 = nlat/2                    if nlat is even or
!               l2 = (nlat+1)/2                if nlat is odd
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            if ityp .le. 2 then
!
!               (2*nt+1)*nlat*nlon + nlat*(4*nt*l1+1)
!
!            or if ityp .gt. 2 then
!
!               (2*nt+1)*l2*nlon + nlat*(4*nt*l1+1)
!
!            will suffice as a length for lwork.
!
!     **************************************************************
!
!     output parameters
!
!
!*PL*ERROR* Comment line too long
!    v,w     two or three dimensional arrays (see input parameter nt) that
!*PL*ERROR* Comment line too long
!            contain a vector field whose vector laplacian is (vlap,wlap).
!*PL*ERROR* Comment line too long
!            w(i,j) is the east longitude and v(i,j) is the colatitudinal
!            component of the vector. v(i,j) and w(i,j) are given on the
!            sphere at the guassian colatitude theta(i) for i=1,...,nlat
!*PL*ERROR* Comment line too long
!            and east longitude lambda(j)=(j-1)*2*pi/nlon for j = 1,...,nlon.
!*PL*ERROR* Comment line too long
!            let cost and sint be the cosine and sine at colatitude theta.
!*PL*ERROR* Comment line too long
!            let d( )/dlambda  and d( )/dtheta be the first order partial
!*PL*ERROR* Comment line too long
!            derivatives in longitude and colatitude.  let sf be either v
!            or w.  define:
!
!                 del2s(sf) = [d(sint*d(sf)/dtheta)/dtheta +
!                               2            2
!                              d (sf)/dlambda /sint]/sint
!
!            then the vector laplacian of (v,w) in (vlap,wlap) satisfies
!
!                 vlap = del2s(v) + (2*cost*dw/dlambda - v)/sint**2
!
!            and
!
!                 wlap = del2s(w) - (2*cost*dv/dlambda + w)/sint**2
!
!
!*PL*ERROR* Comment line too long
!  ierror    a parameter which flags errors in input parameters as follows:
!
!            = 0  no errors detected
!
!            = 1  error in the specification of nlat
!
!            = 2  error in the specification of nlon
!
!            = 3  error in the specification of ityp
!
!            = 4  error in the specification of nt
!
!            = 5  error in the specification of idvw
!
!            = 6  error in the specification of jdvw
!
!            = 7  error in the specification of mdbc
!
!            = 8  error in the specification of ndbc
!
!            = 9  error in the specification of lvhsgs
!
!            = 10 error in the specification of lwork
!
!
! **********************************************************************
!
!     end of documentation for ivlapgs
!
! **********************************************************************
!
      SUBROUTINE DIVLAPGS(NLAT,NLON,ITYP,NT,V,W,IDVW,JDVW,BR,BI,CR,CI, &
           &                   MDBC,NDBC,WVHSGS,LVHSGS,WORK,LWORK,IERROR)
        IMPLICIT NONE
        INTEGER NLAT, NLON, ITYP, NT, IDVW, JDVW, MDBC, NDBC
        INTEGER LWORK, IERROR, LVHSGS
        INTEGER IBI, IBR, ICI, ICR, IDX, IFN, IMID, IWK, L1
        INTEGER IDZ, L2, LIWK, LSAVMIN, LWKMIN, LZIMN
        INTEGER MMAX, MN
      DOUBLE PRECISION V
      DOUBLE PRECISION W
      DOUBLE PRECISION BR
      DOUBLE PRECISION BI
      DOUBLE PRECISION CR
      DOUBLE PRECISION CI
      DOUBLE PRECISION WVHSGS
      DOUBLE PRECISION WORK
      DIMENSION V(IDVW,JDVW,NT),W(IDVW,JDVW,NT)
      DIMENSION BR(MDBC,NDBC,NT),BI(MDBC,NDBC,NT)
      DIMENSION CR(MDBC,NDBC,NT),CI(MDBC,NDBC,NT)
      DIMENSION WVHSGS(LVHSGS),WORK(LWORK)

      IERROR = 1
      IF (NLAT.LT.3) RETURN
      IERROR = 2
      IF (NLON.LT.1) RETURN
      IERROR = 3
      IF (ITYP.LT.0 .OR. ITYP.GT.8) RETURN
      IERROR = 4
      IF (NT.LT.0) RETURN
      IERROR = 5
      IMID = (NLAT+1)/2
      IF ((ITYP.LE.2.AND.IDVW.LT.NLAT) .OR. &
     &    (ITYP.GT.2.AND.IDVW.LT.IMID)) RETURN
      IERROR = 6
      IF (JDVW.LT.NLON) RETURN
      IERROR = 7
      MMAX = MIN0(NLAT, (NLON+1)/2)
      IF (MDBC.LT.MMAX) RETURN
      IERROR = 8
      IF (NDBC.LT.NLAT) RETURN
      IERROR = 9
!
!     set minimum and verify saved workspace length
!
      IDZ = (MMAX* (NLAT+NLAT-MMAX+1))/2
      LZIMN = IDZ*IMID
      LSAVMIN = LZIMN + LZIMN + NLON + 15
      IF (LVHSGS.LT.LSAVMIN) RETURN
!
!     set minimum and verify unsaved work space length
!
      MN = MMAX*NLAT*NT
      L2 = (NLAT+1)/2
      L1 = MIN0(NLAT, (NLON+1)/2)
      IF (ITYP.LE.2) THEN
          LWKMIN = (2*NT+1)*NLAT*NLON + NLAT* (4*NT*L1+1)
      ELSE
          LWKMIN = (2*NT+1)*L2*NLON + NLAT* (4*NT*L1+1)
      END IF
      IF (LWORK.LT.LWKMIN) RETURN
      IERROR = 0
!
!     set work space pointers for vector laplacian coefficients
!
      IF (ITYP.EQ.0 .OR. ITYP.EQ.3 .OR. ITYP.EQ.6) THEN
          IBR = 1
          IBI = IBR + MN
          ICR = IBI + MN
          ICI = ICR + MN
      ELSE IF (ITYP.EQ.1 .OR. ITYP.EQ.4 .OR. ITYP.EQ.7) THEN
          IBR = 1
          IBI = IBR + MN
          ICR = IBI + MN
          ICI = ICR
      ELSE
          IBR = 1
          IBI = 1
          ICR = IBI + MN
          ICI = ICR + MN
      END IF
      IFN = ICI + MN
      IWK = IFN + NLAT
      IF (ITYP.EQ.0 .OR. ITYP.EQ.3 .OR. ITYP.EQ.6) THEN
          LIWK = LWORK - 4*MN - NLAT
      ELSE
          LIWK = LWORK - 2*MN - NLAT
      END IF
      CALL DIVLAPGS1(NLAT,NLON,ITYP,NT,V,W,IDVW,JDVW,WORK(IBR),   &
     &               WORK(IBI),WORK(ICR),WORK(ICI),MMAX,WORK(IFN),MDBC,  &
     &               NDBC,BR,BI,CR,CI,WVHSGS,LVHSGS,WORK(IWK),LIWK, &
     &               IERROR)
      RETURN
      END SUBROUTINE DIVLAPGS

      
      SUBROUTINE DIVLAPGS1(NLAT,NLON,ITYP,NT,V,W,IDVW,JDVW,BRVW,BIVW, &
     &                    CRVW,CIVW,MMAX,FNN,MDBC,NDBC,BR,BI,CR,CI, &
     &                    WSAVE,LSAVE,WK,LWK,IERROR)
        IMPLICIT NONE
        INTEGER NLON, NLAT, ITYP, NT, IDVW, JDVW, MMAX
        INTEGER MDBC, NDBC, LSAVE, LWK, IERROR, K, M, N
      DOUBLE PRECISION V
      DOUBLE PRECISION W
      DOUBLE PRECISION BRVW
      DOUBLE PRECISION BIVW
      DOUBLE PRECISION CRVW
      DOUBLE PRECISION CIVW
      DOUBLE PRECISION FNN
      DOUBLE PRECISION BR
      DOUBLE PRECISION BI
      DOUBLE PRECISION CR
      DOUBLE PRECISION CI
      DOUBLE PRECISION WSAVE
      DOUBLE PRECISION WK
      DOUBLE PRECISION FN
      DIMENSION V(IDVW,JDVW,NT),W(IDVW,JDVW,NT)
      DIMENSION FNN(NLAT),BRVW(MMAX,NLAT,NT),BIVW(MMAX,NLAT,NT)
      DIMENSION CRVW(MMAX,NLAT,NT),CIVW(MMAX,NLAT,NT)
      DIMENSION BR(MDBC,NDBC,NT),BI(MDBC,NDBC,NT)
      DIMENSION CR(MDBC,NDBC,NT),CI(MDBC,NDBC,NT)
      DIMENSION WSAVE(LSAVE),WK(LWK)
!
!     preset coefficient multiplyers
!
      DO  N = 2,NLAT
          FN = DBLE(N-1)
          FNN(N) = -1.0D0/ (FN* (FN+1.D0))
       END DO
!
!     set (u,v) coefficients from br,bi,cr,ci
!
      IF (ITYP.EQ.0 .OR. ITYP.EQ.3 .OR. ITYP.EQ.6) THEN
!
!     all coefficients needed
!
          DO 2 K = 1,NT
              DO 3 N = 1,NLAT
                  DO 4 M = 1,MMAX
                      BRVW(M,N,K) = 0.0D0
                      BIVW(M,N,K) = 0.0D0
                      CRVW(M,N,K) = 0.0D0
                      CIVW(M,N,K) = 0.0D0
    4             CONTINUE
    3         CONTINUE
              DO 5 N = 2,NLAT
                  BRVW(1,N,K) = FNN(N)*BR(1,N,K)
                  BIVW(1,N,K) = FNN(N)*BI(1,N,K)
                  CRVW(1,N,K) = FNN(N)*CR(1,N,K)
                  CIVW(1,N,K) = FNN(N)*CI(1,N,K)
    5         CONTINUE
              DO 6 M = 2,MMAX
                  DO 7 N = M,NLAT
                      BRVW(M,N,K) = FNN(N)*BR(M,N,K)
                      BIVW(M,N,K) = FNN(N)*BI(M,N,K)
                      CRVW(M,N,K) = FNN(N)*CR(M,N,K)
                      CIVW(M,N,K) = FNN(N)*CI(M,N,K)
    7             CONTINUE
    6         CONTINUE
    2     CONTINUE
      ELSE IF (ITYP.EQ.1 .OR. ITYP.EQ.4 .OR. ITYP.EQ.7) THEN
!
!     vorticity is zero so cr,ci=0 not used
!
          DO 12 K = 1,NT
              DO 13 N = 1,NLAT
                  DO 14 M = 1,MMAX
                      BRVW(M,N,K) = 0.0D0
                      BIVW(M,N,K) = 0.0D0
   14             CONTINUE
   13         CONTINUE
              DO 15 N = 2,NLAT
                  BRVW(1,N,K) = FNN(N)*BR(1,N,K)
                  BIVW(1,N,K) = FNN(N)*BI(1,N,K)
   15         CONTINUE
              DO 16 M = 2,MMAX
                  DO 17 N = M,NLAT
                      BRVW(M,N,K) = FNN(N)*BR(M,N,K)
                      BIVW(M,N,K) = FNN(N)*BI(M,N,K)
   17             CONTINUE
   16         CONTINUE
   12     CONTINUE
      ELSE
!
!     divergence is zero so br,bi=0 not used
!
          DO K = 1,NT
              DO N = 1,NLAT
                  DO  M = 1,MMAX
                      CRVW(M,N,K) = 0.0D0
                      CIVW(M,N,K) = 0.0D0
                  END DO
              END DO
              DO N = 2,NLAT
                  CRVW(1,N,K) = FNN(N)*CR(1,N,K)
                  CIVW(1,N,K) = FNN(N)*CI(1,N,K)
              END DO
              DO M = 2,MMAX
                  DO  N = M,NLAT
                      CRVW(M,N,K) = FNN(N)*CR(M,N,K)
                      CIVW(M,N,K) = FNN(N)*CI(M,N,K)
                  END DO
              END DO
          END DO
      END IF
!
!     sythesize coefs into vector field (v,w)
!
      CALL DVHSGS(NLAT,NLON,ITYP,NT,V,W,IDVW,JDVW,BRVW,BIVW,CRVW,CIVW, &
     &           MMAX,NLAT,WSAVE,LSAVE,WK,LWK,IERROR)
      RETURN
      END SUBROUTINE DIVLAPGS1

      
      SUBROUTINE IVLAPGS(NLAT,NLON,ITYP,NT,V,W,IDVW,JDVW,BR,BI,CR,CI, &
           &                   MDBC,NDBC,WVHSGS,LVHSGS,WORK,LWORK,IERROR)
        IMPLICIT NONE
        INTEGER NLAT, NLON, ITYP, NT, IDVW, JDVW, MDBC, NDBC
        INTEGER LWORK, IERROR, LVHSGS
        INTEGER IBI, IBR, ICI, ICR, IDX, IFN, IMID, IWK, L1
        INTEGER IDZ, L2, LIWK, LSAVMIN, LWKMIN, LZIMN
        INTEGER MMAX, MN
      REAL V
      REAL W
      REAL BR
      REAL BI
      REAL CR
      REAL CI
      REAL WVHSGS
      REAL WORK
      DIMENSION V(IDVW,JDVW,NT),W(IDVW,JDVW,NT)
      DIMENSION BR(MDBC,NDBC,NT),BI(MDBC,NDBC,NT)
      DIMENSION CR(MDBC,NDBC,NT),CI(MDBC,NDBC,NT)
      DIMENSION WVHSGS(LVHSGS),WORK(LWORK)

      IERROR = 1
      IF (NLAT.LT.3) RETURN
      IERROR = 2
      IF (NLON.LT.1) RETURN
      IERROR = 3
      IF (ITYP.LT.0 .OR. ITYP.GT.8) RETURN
      IERROR = 4
      IF (NT.LT.0) RETURN
      IERROR = 5
      IMID = (NLAT+1)/2
      IF ((ITYP.LE.2.AND.IDVW.LT.NLAT) .OR. &
     &    (ITYP.GT.2.AND.IDVW.LT.IMID)) RETURN
      IERROR = 6
      IF (JDVW.LT.NLON) RETURN
      IERROR = 7
      MMAX = MIN0(NLAT, (NLON+1)/2)
      IF (MDBC.LT.MMAX) RETURN
      IERROR = 8
      IF (NDBC.LT.NLAT) RETURN
      IERROR = 9
!
!     set minimum and verify saved workspace length
!
      IDZ = (MMAX* (NLAT+NLAT-MMAX+1))/2
      LZIMN = IDZ*IMID
      LSAVMIN = LZIMN + LZIMN + NLON + 15
      IF (LVHSGS.LT.LSAVMIN) RETURN
!
!     set minimum and verify unsaved work space length
!
      MN = MMAX*NLAT*NT
      L2 = (NLAT+1)/2
      L1 = MIN0(NLAT, (NLON+1)/2)
      IF (ITYP.LE.2) THEN
          LWKMIN = (2*NT+1)*NLAT*NLON + NLAT* (4*NT*L1+1)
      ELSE
          LWKMIN = (2*NT+1)*L2*NLON + NLAT* (4*NT*L1+1)
      END IF
      IF (LWORK.LT.LWKMIN) RETURN
      IERROR = 0
!
!     set work space pointers for vector laplacian coefficients
!
      IF (ITYP.EQ.0 .OR. ITYP.EQ.3 .OR. ITYP.EQ.6) THEN
          IBR = 1
          IBI = IBR + MN
          ICR = IBI + MN
          ICI = ICR + MN
      ELSE IF (ITYP.EQ.1 .OR. ITYP.EQ.4 .OR. ITYP.EQ.7) THEN
          IBR = 1
          IBI = IBR + MN
          ICR = IBI + MN
          ICI = ICR
      ELSE
          IBR = 1
          IBI = 1
          ICR = IBI + MN
          ICI = ICR + MN
      END IF
      IFN = ICI + MN
      IWK = IFN + NLAT
      IF (ITYP.EQ.0 .OR. ITYP.EQ.3 .OR. ITYP.EQ.6) THEN
          LIWK = LWORK - 4*MN - NLAT
      ELSE
          LIWK = LWORK - 2*MN - NLAT
      END IF
      CALL IVLAPGS1(NLAT,NLON,ITYP,NT,V,W,IDVW,JDVW,WORK(IBR),   &
     &               WORK(IBI),WORK(ICR),WORK(ICI),MMAX,WORK(IFN),MDBC,  &
     &               NDBC,BR,BI,CR,CI,WVHSGS,LVHSGS,WORK(IWK),LIWK, &
     &               IERROR)
      RETURN
      END SUBROUTINE IVLAPGS

      SUBROUTINE IVLAPGS1(NLAT,NLON,ITYP,NT,V,W,IDVW,JDVW,BRVW,BIVW, &
     &                    CRVW,CIVW,MMAX,FNN,MDBC,NDBC,BR,BI,CR,CI, &
     &                    WSAVE,LSAVE,WK,LWK,IERROR)
        IMPLICIT NONE
        INTEGER NLON, NLAT, ITYP, NT, IDVW, JDVW, MMAX
        INTEGER MDBC, NDBC, LSAVE, LWK, IERROR, K, M, N
      REAL V
      REAL W
      REAL BRVW
      REAL BIVW
      REAL CRVW
      REAL CIVW
      REAL FNN
      REAL BR
      REAL BI
      REAL CR
      REAL CI
      REAL WSAVE
      REAL WK
      REAL FN
      DIMENSION V(IDVW,JDVW,NT),W(IDVW,JDVW,NT)
      DIMENSION FNN(NLAT),BRVW(MMAX,NLAT,NT),BIVW(MMAX,NLAT,NT)
      DIMENSION CRVW(MMAX,NLAT,NT),CIVW(MMAX,NLAT,NT)
      DIMENSION BR(MDBC,NDBC,NT),BI(MDBC,NDBC,NT)
      DIMENSION CR(MDBC,NDBC,NT),CI(MDBC,NDBC,NT)
      DIMENSION WSAVE(LSAVE),WK(LWK)
!
!     preset coefficient multiplyers
!
      DO  N = 2,NLAT
          FN = DBLE(N-1)
          FNN(N) = -1.0D0/ (FN* (FN+1.D0))
       END DO
!
!     set (u,v) coefficients from br,bi,cr,ci
!
      IF (ITYP.EQ.0 .OR. ITYP.EQ.3 .OR. ITYP.EQ.6) THEN
!
!     all coefficients needed
!
          DO 2 K = 1,NT
              DO 3 N = 1,NLAT
                  DO 4 M = 1,MMAX
                      BRVW(M,N,K) = 0.0D0
                      BIVW(M,N,K) = 0.0D0
                      CRVW(M,N,K) = 0.0D0
                      CIVW(M,N,K) = 0.0D0
    4             CONTINUE
    3         CONTINUE
              DO 5 N = 2,NLAT
                  BRVW(1,N,K) = FNN(N)*BR(1,N,K)
                  BIVW(1,N,K) = FNN(N)*BI(1,N,K)
                  CRVW(1,N,K) = FNN(N)*CR(1,N,K)
                  CIVW(1,N,K) = FNN(N)*CI(1,N,K)
    5         CONTINUE
              DO 6 M = 2,MMAX
                  DO 7 N = M,NLAT
                      BRVW(M,N,K) = FNN(N)*BR(M,N,K)
                      BIVW(M,N,K) = FNN(N)*BI(M,N,K)
                      CRVW(M,N,K) = FNN(N)*CR(M,N,K)
                      CIVW(M,N,K) = FNN(N)*CI(M,N,K)
    7             CONTINUE
    6         CONTINUE
    2     CONTINUE
      ELSE IF (ITYP.EQ.1 .OR. ITYP.EQ.4 .OR. ITYP.EQ.7) THEN
!
!     vorticity is zero so cr,ci=0 not used
!
          DO 12 K = 1,NT
              DO 13 N = 1,NLAT
                  DO 14 M = 1,MMAX
                      BRVW(M,N,K) = 0.0D0
                      BIVW(M,N,K) = 0.0D0
   14             CONTINUE
   13         CONTINUE
              DO 15 N = 2,NLAT
                  BRVW(1,N,K) = FNN(N)*BR(1,N,K)
                  BIVW(1,N,K) = FNN(N)*BI(1,N,K)
   15         CONTINUE
              DO 16 M = 2,MMAX
                  DO 17 N = M,NLAT
                      BRVW(M,N,K) = FNN(N)*BR(M,N,K)
                      BIVW(M,N,K) = FNN(N)*BI(M,N,K)
   17             CONTINUE
   16         CONTINUE
   12     CONTINUE
      ELSE
!
!     divergence is zero so br,bi=0 not used
!
          DO K = 1,NT
              DO N = 1,NLAT
                  DO  M = 1,MMAX
                      CRVW(M,N,K) = 0.0D0
                      CIVW(M,N,K) = 0.0D0
                  END DO
              END DO
              DO N = 2,NLAT
                  CRVW(1,N,K) = FNN(N)*CR(1,N,K)
                  CIVW(1,N,K) = FNN(N)*CI(1,N,K)
              END DO
              DO M = 2,MMAX
                  DO  N = M,NLAT
                      CRVW(M,N,K) = FNN(N)*CR(M,N,K)
                      CIVW(M,N,K) = FNN(N)*CI(M,N,K)
                  END DO
              END DO
          END DO
      END IF
!
!     sythesize coefs into vector field (v,w)
!
      CALL VHSGS(NLAT,NLON,ITYP,NT,V,W,IDVW,JDVW,BRVW,BIVW,CRVW,CIVW, &
     &           MMAX,NLAT,WSAVE,LSAVE,WK,LWK,IERROR)
      RETURN
      END SUBROUTINE IVLAPGS1
