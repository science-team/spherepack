!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
! ... file shaec.f
!
!     this file contains code and documentation for subroutines
!     shaec and shaeci
!
! ... files which must be loaded with shaec.f
!
!     sphcom.f, hrfft.f
!
!     subroutine shaec(nlat,nlon,isym,nt,g,idg,jdg,a,b,mdab,ndab,
!    +                 wshaec,lshaec,work,lwork,ierror)
!
!     subroutine shaec performs the spherical harmonic analysis
!     on the array g and stores the result in the arrays a and b.
!     the analysis is performed on an equally spaced grid.  the
!     associated legendre functions are recomputed rather than stored
!     as they are in subroutine shaes.  the analysis is described
!     below at output parameters a,b.
!
!     input parameters
!
!     nlat   the number of colatitudes on the full sphere including the
!            poles. for example, nlat = 37 for a five degree grid.
!            nlat determines the grid increment in colatitude as
!            pi/(nlat-1).  if nlat is odd the equator is located at
!            grid point i=(nlat+1)/2. if nlat is even the equator is
!            located half way between points i=nlat/2 and i=nlat/2+1.
!            nlat must be at least 3. note: on the half sphere, the
!            number of grid points in the colatitudinal direction is
!            nlat/2 if nlat is even or (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than or equal to 4. the efficiency of the computation is
!            improved when nlon is a product of small prime numbers.
!
!     isym   = 0  no symmetries exist about the equator. the analysis
!                 is performed on the entire sphere.  i.e. on the
!                 array g(i,j) for i=1,...,nlat and j=1,...,nlon.
!                 (see description of g below)
!
!            = 1  g is antisymmetric about the equator. the analysis
!                 is performed on the northern hemisphere only.  i.e.
!                 if nlat is odd the analysis is performed on the
!                 array g(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even the analysis is performed on the
!                 array g(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!
!            = 2  g is symmetric about the equator. the analysis is
!                 performed on the northern hemisphere only.  i.e.
!                 if nlat is odd the analysis is performed on the
!                 array g(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even the analysis is performed on the
!                 array g(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!     nt     the number of analyses.  in the program that calls shaec,
!            the arrays g,a and b can be three dimensional in which
!            case multiple analyses will be performed.  the third
!            index is the analysis index which assumes the values
!            k=1,...,nt.  for a single analysis set nt=1. the
!            discription of the remaining parameters is simplified
!            by assuming that nt=1 or that the arrays g,a and b
!            have only two dimensions.
!
!     g      a two or three dimensional array (see input parameter
!            nt) that contains the discrete function to be analyzed.
!            g(i,j) contains the value of the function at the colatitude
!            point theta(i) = (i-1)*pi/(nlat-1) and longitude point
!            phi(j) = (j-1)*2*pi/nlon. the index ranges are defined
!            above at the input parameter isym.
!
!
!     idg    the first dimension of the array g as it appears in the
!            program that calls shaec.  if isym equals zero then idg
!            must be at least nlat.  if isym is nonzero then idg
!            must be at least nlat/2 if nlat is even or at least
!            (nlat+1)/2 if nlat is odd.
!
!     jdg    the second dimension of the array g as it appears in the
!            program that calls shaec.  jdg must be at least nlon.
!
!     mdab   the first dimension of the arrays a and b as it appears
!            in the program that calls shaec. mdab must be at least
!            min0(nlat,(nlon+2)/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!     ndab   the second dimension of the arrays a and b as it appears
!            in the program that calls shaec. ndab must be at least nlat
!
!     wshaec an array which must be initialized by subroutine shaeci.
!            once initialized, wshaec can be used repeatedly by shaec
!            as long as nlon and nlat remain unchanged.  wshaec must
!            not be altered between calls of shaec.
!
!     lshaec the dimension of the array wshaec as it appears in the
!            program that calls shaec. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshaec must be at least
!
!            2*nlat*l2+3*((l1-2)*(nlat+nlat-l1-1))/2+nlon+15
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls shaec. define
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            if isym is zero then lwork must be at least
!
!                    nlat*(nt*nlon+max0(3*l2,nlon))
!
!            if isym is not zero then lwork must be at least
!
!                    l2*(nt*nlon+max0(3*nlat,nlon))
!
!     **************************************************************
!
!     output parameters
!
!     a,b    both a,b are two or three dimensional arrays (see input
!            parameter nt) that contain the spherical harmonic
!            coefficients in the representation of g(i,j) given in the
!            discription of subroutine shsec. for isym=0, a(m,n) and
!            b(m,n) are given by the equations listed below. symmetric
!            versions are used when isym is greater than zero.
!
!
!
!     definitions
!
!     1. the normalized associated legendre functions
!
!     pbar(m,n,theta) = sqrt((2*n+1)*factorial(n-m)/(2*factorial(n+m)))
!                       *sin(theta)**m/(2**n*factorial(n)) times the
!                       (n+m)th derivative of (x**2-1)**n with respect
!                       to x=cos(theta)
!
!     2. the normalized z functions for m even
!
!     zbar(m,n,theta) = 2/(nlat-1) times the sum from k=0 to k=nlat-1 of
!                       the integral from tau = 0 to tau = pi of
!                       cos(k*theta)*cos(k*tau)*pbar(m,n,tau)*sin(tau)
!                       (first and last terms in this sum are divided
!                       by 2)
!
!     3. the normalized z functions for m odd
!
!     zbar(m,n,theta) = 2/(nlat-1) times the sum from k=0 to k=nlat-1 of
!                       of the integral from tau = 0 to tau = pi of
!                       sin(k*theta)*sin(k*tau)*pbar(m,n,tau)*sin(tau)
!
!     4. the fourier transform of g(i,j).
!
!     c(m,i)          = 2/nlon times the sum from j=1 to j=nlon
!                       of g(i,j)*cos((m-1)*(j-1)*2*pi/nlon)
!                       (the first and last terms in this sum
!                       are divided by 2)
!
!     s(m,i)          = 2/nlon times the sum from j=2 to j=nlon
!                       of g(i,j)*sin((m-1)*(j-1)*2*pi/nlon)
!
!     5. the maximum (plus one) longitudinal wave number
!
!            mmax = min0(nlat,(nlon+2)/2) if nlon is even or
!            mmax = min0(nlat,(nlon+1)/2) if nlon is odd.
!
!
!     then for m=0,...,mmax-1 and n=m,...,nlat-1 the arrays a,b
!     are given by
!
!     a(m+1,n+1)      = the sum from i=1 to i=nlat of
!                       c(m+1,i)*zbar(m,n,theta(i))
!                       (first and last terms in this sum are
!                       divided by 2)
!
!     b(m+1,n+1)      = the sum from i=1 to i=nlat of
!                       s(m+1,i)*zbar(m,n,theta(i))
!
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of isym
!            = 4  error in the specification of nt
!            = 5  error in the specification of idg
!            = 6  error in the specification of jdg
!            = 7  error in the specification of mdab
!            = 8  error in the specification of ndab
!            = 9  error in the specification of lshaec
!            = 10 error in the specification of lwork
!
!
! ****************************************************************
!     subroutine shaeci(nlat,nlon,wshaec,lshaec,dwork,ldwork,ierror)
!
!     subroutine shaeci initializes the array wshaec which can then
!     be used repeatedly by subroutine shaec.
!
!     input parameters
!
!     nlat   the number of colatitudes on the full sphere including the
!            poles. for example, nlat = 37 for a five degree grid.
!            nlat determines the grid increment in colatitude as
!            pi/(nlat-1).  if nlat is odd the equator is located at
!            grid point i=(nlat+1)/2. if nlat is even the equator is
!            located half way between points i=nlat/2 and i=nlat/2+1.
!            nlat must be at least 3. note: on the half sphere, the
!            number of grid points in the colatitudinal direction is
!            nlat/2 if nlat is even or (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than or equal to 4. the efficiency of the computation is
!            improved when nlon is a product of small prime numbers.
!
!     lshaec the dimension of the array wshaec as it appears in the
!            program that calls shaeci. the array wshaec is an output
!            parameter which is described below. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshaec must be at least
!
!            2*nlat*l2+3*((l1-2)*(nlat+nlat-l1-1))/2+nlon+15
!
!     dwork  a double precision dwork array that does not have to be saved.
!
!     ldwork the dimension of the array dwork as it appears in the
!            program that calls shaeci.  ldwork  must be at least
!            nlat+1.
!
!
!     output parameters
!
!     wshaec an array which is initialized for use by subroutine shaec.
!            once initialized, wshaec can be used repeatedly by shaec
!            as long as nlon and nlat remain unchanged.  wshaec must
!            not be altered between calls of shaec.
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of lshaec
!            = 4  error in the specification of ldwork
!
!
! *******************************************************************
      SUBROUTINE SHAEC(Nlat,Nlon,Isym,Nt,G,Idg,Jdg,A,B,Mdab,Ndab,Wshaec,&
                     & Lshaec,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL A , B , G , Work , Wshaec
      INTEGER Idg , Ierror , imid , ist , Isym , iw1 , Jdg , labc , ls ,&
            & Lshaec , Lwork , lzz1 , Mdab , mmax , Ndab , Nlat , nln , &
            & Nlon , Nt
      DIMENSION G(Idg,Jdg,*) , A(Mdab,Ndab,*) , B(Mdab,Ndab,*) ,        &
              & Wshaec(*) , Work(*)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      IF ( (Isym==0 .AND. Idg<Nlat) .OR. (Isym/=0 .AND. Idg<(Nlat+1)/2) &
         & ) RETURN
      Ierror = 6
      IF ( Jdg<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,Nlon/2+1)
      IF ( Mdab<mmax ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      imid = (Nlat+1)/2
      lzz1 = 2*Nlat*imid
      labc = 3*((mmax-2)*(Nlat+Nlat-mmax-1))/2
      IF ( Lshaec<lzz1+labc+Nlon+15 ) RETURN
      Ierror = 10
      ls = Nlat
      IF ( Isym>0 ) ls = imid
      nln = Nt*ls*Nlon
      IF ( Lwork<nln+MAX0(ls*Nlon,3*Nlat*imid) ) RETURN
      Ierror = 0
      ist = 0
      IF ( Isym==0 ) ist = imid
      iw1 = lzz1 + labc + 1
      CALL SHAEC1(Nlat,Isym,Nt,G,Idg,Jdg,A,B,Mdab,Ndab,imid,ls,Nlon,    &
                & Work,Work(ist+1),Work(nln+1),Work(nln+1),Wshaec,      &
                & Wshaec(iw1))
    END SUBROUTINE SHAEC

    
    SUBROUTINE SHAEC1(Nlat,Isym,Nt,G,Idgs,Jdgs,A,B,Mdab,Ndab,Imid,Idg,&
                      & Jdg,Ge,Go,Work,Zb,Wzfin,Whrfft)
      IMPLICIT NONE
      REAL A , B , fsn , G , Ge , Go , tsn , Whrfft , Work , Wzfin , Zb
      INTEGER i , i3 , Idg , Idgs , Imid , imm1 , Isym , j , Jdg ,      &
            & Jdgs , k , ls , m , Mdab , mdo , mmax , modl , mp1 , mp2 ,&
            & Ndab
      INTEGER ndo , Nlat , nlon , nlp1 , np1 , Nt
!     whrfft must have at least nlon+15 locations
!     wzfin must have 2*l*(nlat+1)/2 + ((l-3)*l+2)/2 locations
!     zb must have 3*l*(nlat+1)/2 locations
!     work must have ls*nlon locations
!
      DIMENSION G(Idgs,Jdgs,1) , A(Mdab,Ndab,1) , B(Mdab,Ndab,1) ,      &
              & Ge(Idg,Jdg,1) , Go(Idg,Jdg,1) , Zb(Imid,Nlat,3) ,       &
              & Wzfin(1) , Whrfft(1) , Work(1)
      ls = Idg
      nlon = Jdg
      mmax = MIN0(Nlat,nlon/2+1)
      mdo = mmax
      IF ( mdo+mdo-1>nlon ) mdo = mmax - 1
      nlp1 = Nlat + 1
      tsn = 2./nlon
      fsn = 4./nlon
      modl = MOD(Nlat,2)
      imm1 = Imid
      IF ( modl/=0 ) imm1 = Imid - 1
      IF ( Isym/=0 ) THEN
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO j = 1 , nlon
                  Ge(i,j,k) = fsn*G(i,j,k)
               ENDDO
            ENDDO
         ENDDO
         IF ( Isym==1 ) GOTO 100
      ELSE
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO j = 1 , nlon
                  Ge(i,j,k) = tsn*(G(i,j,k)+G(nlp1-i,j,k))
                  Go(i,j,k) = tsn*(G(i,j,k)-G(nlp1-i,j,k))
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      IF ( modl/=0 ) THEN
         DO k = 1 , Nt
            DO j = 1 , nlon
               Ge(Imid,j,k) = tsn*G(Imid,j,k)
            ENDDO
         ENDDO
      ENDIF
 100  DO k = 1 , Nt
         CALL HRFFTF(ls,nlon,Ge(1,1,k),ls,Whrfft,Work)
         IF ( MOD(nlon,2)==0 ) THEN
            DO i = 1 , ls
               Ge(i,nlon,k) = .5*Ge(i,nlon,k)
            ENDDO
         ENDIF
      ENDDO
      DO k = 1 , Nt
         DO mp1 = 1 , mmax
            DO np1 = mp1 , Nlat
               A(mp1,np1,k) = 0.
               B(mp1,np1,k) = 0.
            ENDDO
         ENDDO
      ENDDO
      IF ( Isym/=1 ) THEN
         CALL ZFIN(2,Nlat,nlon,0,Zb,i3,Wzfin)
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 1 , Nlat , 2
                  A(1,np1,k) = A(1,np1,k) + Zb(i,np1,i3)*Ge(i,1,k)
               ENDDO
            ENDDO
         ENDDO
         ndo = Nlat
         IF ( MOD(Nlat,2)==0 ) ndo = Nlat - 1
         DO mp1 = 2 , mdo
            m = mp1 - 1
            CALL ZFIN(2,Nlat,nlon,m,Zb,i3,Wzfin)
            DO k = 1 , Nt
               DO i = 1 , Imid
                  DO np1 = mp1 , ndo , 2
                     A(mp1,np1,k) = A(mp1,np1,k) + Zb(i,np1,i3)         &
                                  & *Ge(i,2*mp1-2,k)
                     B(mp1,np1,k) = B(mp1,np1,k) + Zb(i,np1,i3)         &
                                  & *Ge(i,2*mp1-1,k)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
         IF ( mdo/=mmax .AND. mmax<=ndo ) THEN
            CALL ZFIN(2,Nlat,nlon,mdo,Zb,i3,Wzfin)
            DO k = 1 , Nt
               DO i = 1 , Imid
                  DO np1 = mmax , ndo , 2
                     A(mmax,np1,k) = A(mmax,np1,k) + Zb(i,np1,i3)       &
                                   & *Ge(i,2*mmax-2,k)
                  ENDDO
               ENDDO
            ENDDO
         ENDIF
         IF ( Isym==2 ) RETURN
      ENDIF
      CALL ZFIN(1,Nlat,nlon,0,Zb,i3,Wzfin)
      DO k = 1 , Nt
         DO i = 1 , imm1
            DO np1 = 2 , Nlat , 2
               A(1,np1,k) = A(1,np1,k) + Zb(i,np1,i3)*Go(i,1,k)
            ENDDO
         ENDDO
      ENDDO
      ndo = Nlat
      IF ( MOD(Nlat,2)/=0 ) ndo = Nlat - 1
      DO mp1 = 2 , mdo
         m = mp1 - 1
         mp2 = mp1 + 1
         CALL ZFIN(1,Nlat,nlon,m,Zb,i3,Wzfin)
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = mp2 , ndo , 2
                  A(mp1,np1,k) = A(mp1,np1,k) + Zb(i,np1,i3)            &
                               & *Go(i,2*mp1-2,k)
                  B(mp1,np1,k) = B(mp1,np1,k) + Zb(i,np1,i3)            &
                               & *Go(i,2*mp1-1,k)
               ENDDO
            ENDDO
         ENDDO
      ENDDO
      mp2 = mmax + 1
      IF ( mdo==mmax .OR. mp2>ndo ) RETURN
      CALL ZFIN(1,Nlat,nlon,mdo,Zb,i3,Wzfin)
      DO k = 1 , Nt
         DO i = 1 , imm1
            DO np1 = mp2 , ndo , 2
               A(mmax,np1,k) = A(mmax,np1,k) + Zb(i,np1,i3)             &
                             & *Go(i,2*mmax-2,k)
            ENDDO
         ENDDO
      ENDDO
    END SUBROUTINE SHAEC1

    
    SUBROUTINE SHAECI(Nlat,Nlon,Wshaec,Lshaec,Dwork,Ldwork,Ierror)
      IMPLICIT NONE
      INTEGER Ierror , imid , iw1 , labc , Ldwork , Lshaec , lzz1 ,     &
            & mmax , Nlat , Nlon
      REAL Wshaec
      DIMENSION Wshaec(Lshaec)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      imid = (Nlat+1)/2
      mmax = MIN0(Nlat,Nlon/2+1)
      lzz1 = 2*Nlat*imid
      labc = 3*((mmax-2)*(Nlat+Nlat-mmax-1))/2
      IF ( Lshaec<lzz1+labc+Nlon+15 ) RETURN
      Ierror = 4
      IF ( Ldwork<Nlat+1 ) RETURN
      Ierror = 0
      CALL ZFINIT(Nlat,Nlon,Wshaec,Dwork)
      iw1 = lzz1 + labc + 1
      CALL HRFFTI(Nlon,Wshaec(iw1))
    END SUBROUTINE SHAECI


    SUBROUTINE DSHAEC(Nlat,Nlon,Isym,Nt,G,Idg,Jdg,A,B,Mdab,Ndab,Wshaec,&
                     & Lshaec,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION A , B , G , Work , Wshaec
      INTEGER Idg , Ierror , imid , ist , Isym , iw1 , Jdg , labc , ls ,&
            & Lshaec , Lwork , lzz1 , Mdab , mmax , Ndab , Nlat , nln , &
            & Nlon , Nt
      DIMENSION G(Idg,Jdg,*) , A(Mdab,Ndab,*) , B(Mdab,Ndab,*) ,        &
              & Wshaec(*) , Work(*)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      IF ( (Isym==0 .AND. Idg<Nlat) .OR. (Isym/=0 .AND. Idg<(Nlat+1)/2) &
         & ) RETURN
      Ierror = 6
      IF ( Jdg<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,Nlon/2+1)
      IF ( Mdab<mmax ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      imid = (Nlat+1)/2
      lzz1 = 2*Nlat*imid
      labc = 3*((mmax-2)*(Nlat+Nlat-mmax-1))/2
      IF ( Lshaec<lzz1+labc+Nlon+15 ) RETURN
      Ierror = 10
      ls = Nlat
      IF ( Isym>0 ) ls = imid
      nln = Nt*ls*Nlon
      IF ( Lwork<nln+MAX0(ls*Nlon,3*Nlat*imid) ) RETURN
      Ierror = 0
      ist = 0
      IF ( Isym==0 ) ist = imid
      iw1 = lzz1 + labc + 1
      CALL DSHAEC1(Nlat,Isym,Nt,G,Idg,Jdg,A,B,Mdab,Ndab,imid,ls,Nlon,    &
                & Work,Work(ist+1),Work(nln+1),Work(nln+1),Wshaec,      &
                & Wshaec(iw1))
    END SUBROUTINE DSHAEC

    
    SUBROUTINE DSHAEC1(Nlat,Isym,Nt,G,Idgs,Jdgs,A,B,Mdab,Ndab,Imid,Idg,&
                      & Jdg,Ge,Go,Work,Zb,Wzfin,Whrfft)
      IMPLICIT NONE
      DOUBLE PRECISION A , B , fsn , G , Ge , Go , tsn , Whrfft , Work , Wzfin , Zb
      INTEGER i , i3 , Idg , Idgs , Imid , imm1 , Isym , j , Jdg ,      &
            & Jdgs , k , ls , m , Mdab , mdo , mmax , modl , mp1 , mp2 ,&
            & Ndab
      INTEGER ndo , Nlat , nlon , nlp1 , np1 , Nt
!     whrfft must have at least nlon+15 locations
!     wzfin must have 2*l*(nlat+1)/2 + ((l-3)*l+2)/2 locations
!     zb must have 3*l*(nlat+1)/2 locations
!     work must have ls*nlon locations
!
      DIMENSION G(Idgs,Jdgs,1) , A(Mdab,Ndab,1) , B(Mdab,Ndab,1) ,      &
              & Ge(Idg,Jdg,1) , Go(Idg,Jdg,1) , Zb(Imid,Nlat,3) ,       &
              & Wzfin(1) , Whrfft(1) , Work(1)
      ls = Idg
      nlon = Jdg
      mmax = MIN0(Nlat,nlon/2+1)
      mdo = mmax
      IF ( mdo+mdo-1>nlon ) mdo = mmax - 1
      nlp1 = Nlat + 1
      tsn = 2./nlon
      fsn = 4./nlon
      modl = MOD(Nlat,2)
      imm1 = Imid
      IF ( modl/=0 ) imm1 = Imid - 1
      IF ( Isym/=0 ) THEN
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO j = 1 , nlon
                  Ge(i,j,k) = fsn*G(i,j,k)
               ENDDO
            ENDDO
         ENDDO
         IF ( Isym==1 ) GOTO 100
      ELSE
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO j = 1 , nlon
                  Ge(i,j,k) = tsn*(G(i,j,k)+G(nlp1-i,j,k))
                  Go(i,j,k) = tsn*(G(i,j,k)-G(nlp1-i,j,k))
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      IF ( modl/=0 ) THEN
         DO k = 1 , Nt
            DO j = 1 , nlon
               Ge(Imid,j,k) = tsn*G(Imid,j,k)
            ENDDO
         ENDDO
      ENDIF
 100  DO k = 1 , Nt
         CALL DHRFFTF(ls,nlon,Ge(1,1,k),ls,Whrfft,Work)
         IF ( MOD(nlon,2)==0 ) THEN
            DO i = 1 , ls
               Ge(i,nlon,k) = .5*Ge(i,nlon,k)
            ENDDO
         ENDIF
      ENDDO
      DO k = 1 , Nt
         DO mp1 = 1 , mmax
            DO np1 = mp1 , Nlat
               A(mp1,np1,k) = 0.
               B(mp1,np1,k) = 0.
            ENDDO
         ENDDO
      ENDDO
      IF ( Isym/=1 ) THEN
         CALL DZFIN(2,Nlat,nlon,0,Zb,i3,Wzfin)
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 1 , Nlat , 2
                  A(1,np1,k) = A(1,np1,k) + Zb(i,np1,i3)*Ge(i,1,k)
               ENDDO
            ENDDO
         ENDDO
         ndo = Nlat
         IF ( MOD(Nlat,2)==0 ) ndo = Nlat - 1
         DO mp1 = 2 , mdo
            m = mp1 - 1
            CALL DZFIN(2,Nlat,nlon,m,Zb,i3,Wzfin)
            DO k = 1 , Nt
               DO i = 1 , Imid
                  DO np1 = mp1 , ndo , 2
                     A(mp1,np1,k) = A(mp1,np1,k) + Zb(i,np1,i3)         &
                                  & *Ge(i,2*mp1-2,k)
                     B(mp1,np1,k) = B(mp1,np1,k) + Zb(i,np1,i3)         &
                                  & *Ge(i,2*mp1-1,k)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
         IF ( mdo/=mmax .AND. mmax<=ndo ) THEN
            CALL DZFIN(2,Nlat,nlon,mdo,Zb,i3,Wzfin)
            DO k = 1 , Nt
               DO i = 1 , Imid
                  DO np1 = mmax , ndo , 2
                     A(mmax,np1,k) = A(mmax,np1,k) + Zb(i,np1,i3)       &
                                   & *Ge(i,2*mmax-2,k)
                  ENDDO
               ENDDO
            ENDDO
         ENDIF
         IF ( Isym==2 ) RETURN
      ENDIF
      CALL DZFIN(1,Nlat,nlon,0,Zb,i3,Wzfin)
      DO k = 1 , Nt
         DO i = 1 , imm1
            DO np1 = 2 , Nlat , 2
               A(1,np1,k) = A(1,np1,k) + Zb(i,np1,i3)*Go(i,1,k)
            ENDDO
         ENDDO
      ENDDO
      ndo = Nlat
      IF ( MOD(Nlat,2)/=0 ) ndo = Nlat - 1
      DO mp1 = 2 , mdo
         m = mp1 - 1
         mp2 = mp1 + 1
         CALL DZFIN(1,Nlat,nlon,m,Zb,i3,Wzfin)
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = mp2 , ndo , 2
                  A(mp1,np1,k) = A(mp1,np1,k) + Zb(i,np1,i3)            &
                               & *Go(i,2*mp1-2,k)
                  B(mp1,np1,k) = B(mp1,np1,k) + Zb(i,np1,i3)            &
                               & *Go(i,2*mp1-1,k)
               ENDDO
            ENDDO
         ENDDO
      ENDDO
      mp2 = mmax + 1
      IF ( mdo==mmax .OR. mp2>ndo ) RETURN
      CALL DZFIN(1,Nlat,nlon,mdo,Zb,i3,Wzfin)
      DO k = 1 , Nt
         DO i = 1 , imm1
            DO np1 = mp2 , ndo , 2
               A(mmax,np1,k) = A(mmax,np1,k) + Zb(i,np1,i3)             &
                             & *Go(i,2*mmax-2,k)
            ENDDO
         ENDDO
      ENDDO
    END SUBROUTINE DSHAEC1

    
    SUBROUTINE DSHAECI(Nlat,Nlon,Wshaec,Lshaec,Dwork,Ldwork,Ierror)
      IMPLICIT NONE
      INTEGER Ierror , imid , iw1 , labc , Ldwork , Lshaec , lzz1 ,     &
            & mmax , Nlat , Nlon
      DOUBLE PRECISION Wshaec
      DIMENSION Wshaec(Lshaec)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      imid = (Nlat+1)/2
      mmax = MIN0(Nlat,Nlon/2+1)
      lzz1 = 2*Nlat*imid
      labc = 3*((mmax-2)*(Nlat+Nlat-mmax-1))/2
      IF ( Lshaec<lzz1+labc+Nlon+15 ) RETURN
      Ierror = 4
      IF ( Ldwork<Nlat+1 ) RETURN
      Ierror = 0
      CALL DZFINIT(Nlat,Nlon,Wshaec,Dwork)
      iw1 = lzz1 + labc + 1
      CALL DHRFFTI(Nlon,Wshaec(iw1))
    END SUBROUTINE DSHAECI
