!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
! ... file shagc.f
!
!     this file contains code and documentation for subroutines
!     shagc and shagci
!
! ... files which must be loaded with shagc.f
!
!     sphcom.f, hrfft.f, gaqd.f
!
!
!     subroutine shagc(nlat,nlon,isym,nt,g,idg,jdg,a,b,mdab,ndab,
!    +                 wshagc,lshagc,work,lwork,ierror)
!
!     subroutine shagc performs the spherical harmonic analysis
!     on the array g and stores the result in the arrays a and b.
!     the analysis is performed on a gaussian grid in colatitude
!     and an equally spaced grid in longitude.  the associated
!     legendre functions are recomputed rather than stored as they
!     are in subroutine shags.  the analysis is described below
!     at output parameters a,b.
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are compu
!            in radians in theta(1),...,theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid poi
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than or equal to 4. the efficiency of the computation is
!            improved when nlon is a product of small prime numbers.
!
!     isym   = 0  no symmetries exist about the equator. the analysis
!                 is performed on the entire sphere.  i.e. on the
!                 array g(i,j) for i=1,...,nlat and j=1,...,nlon.
!                 (see description of g below)
!
!            = 1  g is antisymmetric about the equator. the analysis
!                 is performed on the northern hemisphere only.  i.e.
!                 if nlat is odd the analysis is performed on the
!                 array g(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even the analysis is performed on the
!                 array g(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!
!            = 2  g is symmetric about the equator. the analysis is
!                 performed on the northern hemisphere only.  i.e.
!                 if nlat is odd the analysis is performed on the
!                 array g(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even the analysis is performed on the
!                 array g(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!     nt     the number of analyses.  in the program that calls shagc,
!            the arrays g,a and b can be three dimensional in which
!            case multiple analyses will be performed.  the third
!            index is the analysis index which assumes the values
!            k=1,...,nt.  for a single analysis set nt=1. the
!            discription of the remaining parameters is simplified
!            by assuming that nt=1 or that the arrays g,a and b
!            have only two dimensions.
!
!     g      a two or three dimensional array (see input parameter
!            nt) that contains the discrete function to be analyzed.
!            g(i,j) contains the value of the function at the gaussian
!            point theta(i) and longitude point phi(j) = (j-1)*2*pi/nlon
!            the index ranges are defined above at the input parameter
!            isym.
!
!     idg    the first dimension of the array g as it appears in the
!            program that calls shagc. if isym equals zero then idg
!            must be at least nlat.  if isym is nonzero then idg must
!            be at least nlat/2 if nlat is even or at least (nlat+1)/2
!            if nlat is odd.
!
!     jdg    the second dimension of the array g as it appears in the
!            program that calls shagc. jdg must be at least nlon.
!
!     mdab   the first dimension of the arrays a and b as it appears
!            in the program that calls shagc. mdab must be at least
!            min0((nlon+2)/2,nlat) if nlon is even or at least
!            min0((nlon+1)/2,nlat) if nlon is odd
!
!     ndab   the second dimension of the arrays a and b as it appears
!            in the program that calls shaec. ndab must be at least nlat
!
!     wshagc an array which must be initialized by subroutine shagci.
!            once initialized, wshagc can be used repeatedly by shagc.
!            as long as nlat and nlon remain unchanged.  wshagc must
!            not be altered between calls of shagc.
!
!     lshagc the dimension of the array wshagc as it appears in the
!            program that calls shagc. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshagc must be at least
!
!                  nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+nlon+15
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls shagc. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            if isym is zero then lwork must be at least
!
!                      nlat*(nlon*nt+max0(3*l2,nlon))
!
!            if isym is not zero then lwork must be at least
!
!                      l2*(nlon*nt+max0(3*nlat,nlon))
!
!     **************************************************************
!
!     output parameters
!
!     a,b    both a,b are two or three dimensional arrays (see input
!            parameter nt) that contain the spherical harmonic
!            coefficients in the representation of g(i,j) given in the
!            discription of subroutine shagc. for isym=0, a(m,n) and
!            b(m,n) are given by the equations listed below. symmetric
!            versions are used when isym is greater than zero.
!
!     definitions
!
!     1. the normalized associated legendre functions
!
!     pbar(m,n,theta) = sqrt((2*n+1)*factorial(n-m)/(2*factorial(n+m)))
!                       *sin(theta)**m/(2**n*factorial(n)) times the
!                       (n+m)th derivative of (x**2-1)**n with respect
!                       to x=cos(theta).
!
!     2. the fourier transform of g(i,j).
!
!     c(m,i)          = 2/nlon times the sum from j=1 to j=nlon of
!                       g(i,j)*cos((m-1)*(j-1)*2*pi/nlon)
!                       (the first and last terms in this sum
!                       are divided by 2)
!
!     s(m,i)          = 2/nlon times the sum from j=2 to j=nlon of
!                       g(i,j)*sin((m-1)*(j-1)*2*pi/nlon)
!
!
!     3. the gaussian points and weights on the sphere
!        (computed by subroutine gaqd).
!
!        theta(1),...,theta(nlat) (gaussian pts in radians)
!        wts(1),...,wts(nlat) (corresponding gaussian weights)
!
!     4. the maximum (plus one) longitudinal wave number
!
!            mmax = min0(nlat,(nlon+2)/2) if nlon is even or
!            mmax = min0(nlat,(nlon+1)/2) if nlon is odd.
!
!
!     then for m=0,...,mmax-1 and n=m,...,nlat-1 the arrays a,b
!     are given by
!
!     a(m+1,n+1)     =  the sum from i=1 to i=nlat of
!                       c(m+1,i)*wts(i)*pbar(m,n,theta(i))
!
!     b(m+1,n+1)      = the sum from i=1 to nlat of
!                       s(m+1,i)*wts(i)*pbar(m,n,theta(i))
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of isym
!            = 4  error in the specification of nt
!            = 5  error in the specification of idg
!            = 6  error in the specification of jdg
!            = 7  error in the specification of mdab
!            = 8  error in the specification of ndab
!            = 9  error in the specification of lshagc
!            = 10 error in the specification of lwork
!
!
! ****************************************************************
!
!     subroutine shagci(nlat,nlon,wshagc,lshagc,dwork,ldwork,ierror)
!
!     subroutine shagci initializes the array wshagc which can then
!     be used repeatedly by subroutines shagc. it precomputes
!     and stores in wshagc quantities such as gaussian weights,
!     legendre polynomial coefficients, and fft trigonometric tables.
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are compu
!            in radians in theta(1),...,theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid poi
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than or equal to 4. the efficiency of the computation is
!            improved when nlon is a product of small prime numbers.
!
!     wshagc an array which must be initialized by subroutine shagci.
!            once initialized, wshagc can be used repeatedly by shagc
!            as long as nlat and nlon remain unchanged.  wshagc must
!            not be altered between calls of shagc.
!
!     lshagc the dimension of the array wshagc as it appears in the
!            program that calls shagc. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshagc must be at least
!
!                  nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+nlon+15
!
!     dwork   a double precision work array that does not have to be saved.
!
!     ldwork  the dimension of the array dwork as it appears in the
!            program that calls shagci. ldwork must be at least
!
!                nlat*(nlat+4)
!
!     output parameter
!
!     wshagc an array which must be initialized before calling shagc or
!            once initialized, wshagc can be used repeatedly by shagc or
!            as long as nlat and nlon remain unchanged.  wshagc must not
!            altered between calls of shagc.
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of lshagc
!            = 4  error in the specification of ldwork
!            = 5  failure in gaqd to compute gaussian points
!                 (due to failure in eigenvalue routine)
!
!
! ****************************************************************
      SUBROUTINE SHAGC(Nlat,Nlon,Isym,Nt,G,Idg,Jdg,A,B,Mdab,Ndab,Wshagc,&
                     & Lshagc,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL A , B , G , Work , Wshagc
      INTEGER Idg , Ierror , ifft , ipmn , Isym , iwts , Jdg , l , l1 , &
            & l2 , lat , late , Lshagc , Lwork , Mdab , Ndab , Nlat ,   &
            & Nlon , Nt

!     subroutine shagc performs the spherical harmonic analysis on
!     a gaussian grid on the array(s) in g and returns the coefficients
!     in array(s) a,b. the necessary legendre polynomials are computed
!     as needed in this version.
!
      DIMENSION G(Idg,Jdg,1) , A(Mdab,Ndab,1) , B(Mdab,Ndab,1) ,        &
              & Wshagc(Lshagc) , Work(Lwork)
!     check input parameters
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<1 ) RETURN
!     set upper limit on m for spherical harmonic basis
      l = MIN0((Nlon+2)/2,Nlat)
!     set gaussian point nearest equator pointer
      late = (Nlat+MOD(Nlat,2))/2
!     set number of grid points for analysis/synthesis
      lat = Nlat
      IF ( Isym/=0 ) lat = late
      Ierror = 5
      IF ( Idg<lat ) RETURN
      Ierror = 6
      IF ( Jdg<Nlon ) RETURN
      Ierror = 7
      IF ( Mdab<l ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      l1 = l
      l2 = late
      Ierror = 9
!     check permanent work space length
      IF ( Lshagc<Nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+Nlon+15 ) RETURN
      Ierror = 10
!     check temporary work space length
      IF ( Isym==0 ) THEN
         IF ( Lwork<Nlat*(Nlon*Nt+MAX0(3*l2,Nlon)) ) RETURN
!     isym.ne.0
      ELSEIF ( Lwork<l2*(Nlon*Nt+MAX0(3*Nlat,Nlon)) ) THEN
         RETURN
      ENDIF
      Ierror = 0
!     starting address for gaussian wts in shigc and fft values
      iwts = 1
      ifft = Nlat + 2*Nlat*late + 3*(l*(l-1)/2+(Nlat-l)*(l-1)) + 1
!     set pointers for internal storage of g and legendre polys
      ipmn = lat*Nlon*Nt + 1
      CALL SHAGC1(Nlat,Nlon,l,lat,Isym,G,Idg,Jdg,Nt,A,B,Mdab,Ndab,      &
                & Wshagc,Wshagc(iwts),Wshagc(ifft),late,Work(ipmn),Work)
    END SUBROUTINE SHAGC

    
    SUBROUTINE SHAGC1(Nlat,Nlon,L,Lat,Mode,Gs,Idg,Jdg,Nt,A,B,Mdab,    &
                      & Ndab,W,Wts,Wfft,Late,Pmn,G)
      IMPLICIT NONE
      REAL A , B , G , Gs , Pmn , sfn , t1 , t2 , W , Wfft , Wts
      INTEGER i , Idg , is , j , Jdg , k , km , L , Lat , Late , lm1 ,  &
            & lp1 , m , Mdab , Mode , mp1 , mp2 , ms , Ndab , nl2
      INTEGER Nlat , Nlon , np1 , ns , Nt
      DIMENSION Gs(Idg,Jdg,Nt) , A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt) ,    &
              & G(Lat,Nlon,Nt)
      DIMENSION W(1) , Wts(Nlat) , Wfft(1) , Pmn(Nlat,Late,3)
!     set gs array internally in shagc1
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Lat
               G(i,j,k) = Gs(i,j,k)
            ENDDO
         ENDDO
      ENDDO
!     do fourier transform
      DO k = 1 , Nt
         CALL HRFFTF(Lat,Nlon,G(1,1,k),Lat,Wfft,Pmn)
      ENDDO
!     scale result
      sfn = 2.0/FLOAT(Nlon)
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Lat
               G(i,j,k) = sfn*G(i,j,k)
            ENDDO
         ENDDO
      ENDDO
!     compute using gaussian quadrature
!     a(n,m) = s (ga(theta,m)*pnm(theta)*sin(theta)*dtheta)
!     b(n,m) = s (gb(theta,m)*pnm(theta)*sin(theta)*dtheta)
!     here ga,gb are the cos(phi),sin(phi) coefficients of
!     the fourier expansion of g(theta,phi) in phi.  as a result
!     of the above fourier transform they are stored in array
!     g as follows:
!     for each theta(i) and k= l-1
!     ga(0),ga(1),gb(1),ga(2),gb(2),...,ga(k-1),gb(k-1),ga(k)
!     correspond to (in the case nlon=l+l-2)
!     g(i,1),g(i,2),g(i,3),g(i,4),g(i,5),...,g(i,2l-4),g(i,2l-3),g(i,2l-
!     initialize coefficients to zero
      DO k = 1 , Nt
         DO np1 = 1 , Nlat
            DO mp1 = 1 , L
               A(mp1,np1,k) = 0.0
               B(mp1,np1,k) = 0.0
            ENDDO
         ENDDO
      ENDDO
!     set m+1 limit on b(m+1) calculation
      lm1 = L
      IF ( Nlon==L+L-2 ) lm1 = L - 1
      IF ( Mode==0 ) THEN
!     for full sphere (mode=0) and even/odd reduction:
!     overwrite g(i) with (g(i)+g(nlat-i+1))*wts(i)
!     overwrite g(nlat-i+1) with (g(i)-g(nlat-i+1))*wts(i)
         nl2 = Nlat/2
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , nl2
                  is = Nlat - i + 1
                  t1 = G(i,j,k)
                  t2 = G(is,j,k)
                  G(i,j,k) = Wts(i)*(t1+t2)
                  G(is,j,k) = Wts(i)*(t1-t2)
               ENDDO
!     adjust equator if necessary(nlat odd)
               IF ( MOD(Nlat,2)/=0 ) G(Late,j,k) = Wts(Late)*G(Late,j,k)
            ENDDO
         ENDDO
!     set m = 0 coefficients first
         m = 0
         CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
         DO k = 1 , Nt
            DO i = 1 , Late
               is = Nlat - i + 1
               DO np1 = 1 , Nlat , 2
!     n even
                  A(1,np1,k) = A(1,np1,k) + G(i,1,k)*Pmn(np1,i,km)
               ENDDO
               DO np1 = 2 , Nlat , 2
!     n odd
                  A(1,np1,k) = A(1,np1,k) + G(is,1,k)*Pmn(np1,i,km)
               ENDDO
            ENDDO
         ENDDO
!     compute coefficients for which b(m,n) is available
         DO mp1 = 2 , lm1
            m = mp1 - 1
            mp2 = m + 2
!     compute pmn for all i and n=m,...,l-1
            CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
               DO i = 1 , Late
                  is = Nlat - i + 1
!     n-m even
                  DO np1 = mp1 , Nlat , 2
                     A(mp1,np1,k) = A(mp1,np1,k) + G(i,2*m,k)           &
                                  & *Pmn(np1,i,km)
                     B(mp1,np1,k) = B(mp1,np1,k) + G(i,2*m+1,k)         &
                                  & *Pmn(np1,i,km)
                  ENDDO
!     n-m odd
                  DO np1 = mp2 , Nlat , 2
                     A(mp1,np1,k) = A(mp1,np1,k) + G(is,2*m,k)          &
                                  & *Pmn(np1,i,km)
                     B(mp1,np1,k) = B(mp1,np1,k) + G(is,2*m+1,k)        &
                                  & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
         IF ( Nlon==L+L-2 ) THEN
!     compute a(l,np1) coefficients only
            m = L - 1
            CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
               DO i = 1 , Late
                  is = Nlat - i + 1
!     n-m even
                  DO np1 = L , Nlat , 2
                     A(L,np1,k) = A(L,np1,k) + 0.5*G(i,Nlon,k)          &
                                & *Pmn(np1,i,km)
                  ENDDO
                  lp1 = L + 1
!     n-m odd
                  DO np1 = lp1 , Nlat , 2
                     A(L,np1,k) = A(L,np1,k) + 0.5*G(is,Nlon,k)         &
                                & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDIF
      ELSE
!     half sphere
!     overwrite g(i) with wts(i)*(g(i)+g(i)) for i=1,...,nlate/2
         nl2 = Nlat/2
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , nl2
                  G(i,j,k) = Wts(i)*(G(i,j,k)+G(i,j,k))
               ENDDO
!     adjust equator separately if a grid point
               IF ( nl2<Late ) G(Late,j,k) = Wts(Late)*G(Late,j,k)
            ENDDO
         ENDDO
!     set m = 0 coefficients first
         m = 0
         CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
         ms = 1
         IF ( Mode==1 ) ms = 2
         DO k = 1 , Nt
            DO i = 1 , Late
               DO np1 = ms , Nlat , 2
                  A(1,np1,k) = A(1,np1,k) + G(i,1,k)*Pmn(np1,i,km)
               ENDDO
            ENDDO
         ENDDO
!     compute coefficients for which b(m,n) is available
         DO mp1 = 2 , lm1
            m = mp1 - 1
            ms = mp1
            IF ( Mode==1 ) ms = mp1 + 1
!     compute pmn for all i and n=m,...,nlat-1
            CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
               DO i = 1 , Late
                  DO np1 = ms , Nlat , 2
                     A(mp1,np1,k) = A(mp1,np1,k) + G(i,2*m,k)           &
                                  & *Pmn(np1,i,km)
                     B(mp1,np1,k) = B(mp1,np1,k) + G(i,2*m+1,k)         &
                                  & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
         IF ( Nlon==L+L-2 ) THEN
!     compute coefficient a(l,np1) only
            m = L - 1
            CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
            ns = L
            IF ( Mode==1 ) ns = L + 1
            DO k = 1 , Nt
               DO i = 1 , Late
                  DO np1 = ns , Nlat , 2
                     A(L,np1,k) = A(L,np1,k) + 0.5*G(i,Nlon,k)          &
                                & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDIF
      ENDIF
      END SUBROUTINE SHAGC1

      
      SUBROUTINE SHAGCI(Nlat,Nlon,Wshagc,Lshagc,Dwork,Ldwork,Ierror)
      IMPLICIT NONE
      INTEGER i1 , i2 , i3 , i4 , i5 , i6 , i7 , idth , idwts , Ierror ,&
            & iw , l , l1 , l2 , late , Ldwork , Lshagc , Nlat , Nlon
      REAL Wshagc
!     this subroutine must be called before calling shagc with
!     fixed nlat,nlon. it precomputes quantites such as the gaussian
!     points and weights, m=0,m=1 legendre polynomials, recursion
!     recursion coefficients.
      DIMENSION Wshagc(Lshagc)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
!     set triangular truncation limit for spherical harmonic basis
      l = MIN0((Nlon+2)/2,Nlat)
!     set equator or nearest point (if excluded) pointer
      late = (Nlat+MOD(Nlat,2))/2
      l1 = l
      l2 = late
      Ierror = 3
!     check permanent work space length
      IF ( Lshagc<Nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+Nlon+15 ) RETURN
      Ierror = 4
      IF ( Ldwork<Nlat*(Nlat+4) ) RETURN
      Ierror = 0
!     set pointers
      i1 = 1
      i2 = i1 + Nlat
      i3 = i2 + Nlat*late
      i4 = i3 + Nlat*late
      i5 = i4 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i6 = i5 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i7 = i6 + l*(l-1)/2 + (Nlat-l)*(l-1)
!     set indices in temp work for double precision gaussian wts and pts
      idth = 1
      idwts = idth + Nlat
      iw = idwts + Nlat
      CALL SHAGCI1(Nlat,Nlon,l,late,Wshagc(i1),Wshagc(i2),Wshagc(i3),   &
                 & Wshagc(i4),Wshagc(i5),Wshagc(i6),Wshagc(i7),         &
                 & Dwork(idth),Dwork(idwts),Dwork(iw),Ierror)
      IF ( Ierror/=0 ) Ierror = 5
    END SUBROUTINE SHAGCI

    
    SUBROUTINE SHAGCI1(Nlat,Nlon,L,Late,Wts,P0n,P1n,Abel,Bbel,Cbel,   &
                       & Wfft,Dtheta,Dwts,Work,Ier)
      IMPLICIT NONE
      REAL Abel , Bbel , Cbel , P0n , P1n , Wfft , Wts
      INTEGER i , Ier , imn , IMNDX , INDX , L , Late , lw , m , mlim , &
            & n , Nlat , Nlon , np1
      DIMENSION Wts(Nlat) , P0n(Nlat,Late) , P1n(Nlat,Late) , Abel(1) , &
              & Bbel(1) , Cbel(1) , Wfft(1)
      DOUBLE PRECISION pb , Dtheta(Nlat) , Dwts(Nlat) , Work(*)
!     compute the nlat  gaussian points and weights, the
!     m=0,1 legendre polys for gaussian points and all n,
!     and the legendre recursion coefficients
!     define index function used in storing
!     arrays for recursion coefficients (functions of (m,n))
!     the index function indx(m,n) is defined so that
!     the pairs (m,n) map to [1,2,...,indx(l-1,l-1)] with no
!     "holes" as m varies from 2 to n and n varies from 2 to l-1.
!     (m=0,1 are set from p0n,p1n for all n)
!     define for 2.le.n.le.l-1
      INDX(m,n) = (n-1)*(n-2)/2 + m - 1
!     define index function for l.le.n.le.nlat
      IMNDX(m,n) = L*(L-1)/2 + (n-L-1)*(L-1) + m - 1
!     preset quantites for fourier transform
      CALL HRFFTI(Nlon,Wfft)
!     compute double precision gaussian points and weights
!     lw = 4*nlat*(nlat+1)+2
      lw = Nlat*(Nlat+2)
      CALL GAQD(Nlat,Dtheta,Dwts,Work,lw,Ier)
      IF ( Ier/=0 ) RETURN
!     store gaussian weights single precision to save computation
!     in inner loops in analysis
      DO i = 1 , Nlat
         Wts(i) = Dwts(i)
      ENDDO
!     initialize p0n,p1n using double precision dnlfk,dnlft
      DO np1 = 1 , Nlat
         DO i = 1 , Late
            P0n(np1,i) = 0.0
            P1n(np1,i) = 0.0
         ENDDO
      ENDDO
!     compute m=n=0 legendre polynomials for all theta(i)
      np1 = 1
      n = 0
      m = 0
      CALL DNLFK(m,n,Work)
      DO i = 1 , Late
         CALL DNLFT(m,n,Dtheta(i),Work,pb)
         P0n(1,i) = pb
      ENDDO
!     compute p0n,p1n for all theta(i) when n.gt.0
      DO np1 = 2 , Nlat
         n = np1 - 1
         m = 0
         CALL DNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DNLFT(m,n,Dtheta(i),Work,pb)
            P0n(np1,i) = pb
         ENDDO
!     compute m=1 legendre polynomials for all n and theta(i)
         m = 1
         CALL DNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DNLFT(m,n,Dtheta(i),Work,pb)
            P1n(np1,i) = pb
         ENDDO
      ENDDO
!     compute and store swarztrauber recursion coefficients
!     for 2.le.m.le.n and 2.le.n.le.nlat in abel,bbel,cbel
      DO n = 2 , Nlat
         mlim = MIN0(n,L)
         DO m = 2 , mlim
            imn = INDX(m,n)
            IF ( n>=L ) imn = IMNDX(m,n)
            Abel(imn) = SQRT(FLOAT((2*n+1)*(m+n-2)*(m+n-3))/FLOAT(((2*n-&
                      & 3)*(m+n-1)*(m+n))))
            Bbel(imn) = SQRT(FLOAT((2*n+1)*(n-m-1)*(n-m))/FLOAT(((2*n-3)&
                      & *(m+n-1)*(m+n))))
            Cbel(imn) = SQRT(FLOAT((n-m+1)*(n-m+2))/FLOAT(((n+m-1)*(n+m)&
                      & )))
         ENDDO
      ENDDO
      END SUBROUTINE SHAGCI1


      SUBROUTINE DSHAGC(Nlat,Nlon,Isym,Nt,G,Idg,Jdg,A,B,Mdab,Ndab,Wshagc,&
                     & Lshagc,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION A , B , G , Work , Wshagc
      INTEGER Idg , Ierror , ifft , ipmn , Isym , iwts , Jdg , l , l1 , &
            & l2 , lat , late , Lshagc , Lwork , Mdab , Ndab , Nlat ,   &
            & Nlon , Nt

!     subroutine shagc performs the spherical harmonic analysis on
!     a gaussian grid on the array(s) in g and returns the coefficients
!     in array(s) a,b. the necessary legendre polynomials are computed
!     as needed in this version.
!
      DIMENSION G(Idg,Jdg,1) , A(Mdab,Ndab,1) , B(Mdab,Ndab,1) ,        &
              & Wshagc(Lshagc) , Work(Lwork)
!     check input parameters
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<1 ) RETURN
!     set upper limit on m for spherical harmonic basis
      l = MIN0((Nlon+2)/2,Nlat)
!     set gaussian point nearest equator pointer
      late = (Nlat+MOD(Nlat,2))/2
!     set number of grid points for analysis/synthesis
      lat = Nlat
      IF ( Isym/=0 ) lat = late
      Ierror = 5
      IF ( Idg<lat ) RETURN
      Ierror = 6
      IF ( Jdg<Nlon ) RETURN
      Ierror = 7
      IF ( Mdab<l ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      l1 = l
      l2 = late
      Ierror = 9
!     check permanent work space length
      IF ( Lshagc<Nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+Nlon+15 ) RETURN
      Ierror = 10
!     check temporary work space length
      IF ( Isym==0 ) THEN
         IF ( Lwork<Nlat*(Nlon*Nt+MAX0(3*l2,Nlon)) ) RETURN
!     isym.ne.0
      ELSEIF ( Lwork<l2*(Nlon*Nt+MAX0(3*Nlat,Nlon)) ) THEN
         RETURN
      ENDIF
      Ierror = 0
!     starting address for gaussian wts in shigc and fft values
      iwts = 1
      ifft = Nlat + 2*Nlat*late + 3*(l*(l-1)/2+(Nlat-l)*(l-1)) + 1
!     set pointers for internal storage of g and legendre polys
      ipmn = lat*Nlon*Nt + 1
      CALL DSHAGC1(Nlat,Nlon,l,lat,Isym,G,Idg,Jdg,Nt,A,B,Mdab,Ndab,      &
                & Wshagc,Wshagc(iwts),Wshagc(ifft),late,Work(ipmn),Work)
    END SUBROUTINE DSHAGC

    
    SUBROUTINE DSHAGC1(Nlat,Nlon,L,Lat,Mode,Gs,Idg,Jdg,Nt,A,B,Mdab,    &
                      & Ndab,W,Wts,Wfft,Late,Pmn,G)
      IMPLICIT NONE
      DOUBLE PRECISION A , B , G , Gs , Pmn , sfn , t1 , t2 , W , Wfft , Wts
      INTEGER i , Idg , is , j , Jdg , k , km , L , Lat , Late , lm1 ,  &
            & lp1 , m , Mdab , Mode , mp1 , mp2 , ms , Ndab , nl2
      INTEGER Nlat , Nlon , np1 , ns , Nt
      DIMENSION Gs(Idg,Jdg,Nt) , A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt) ,    &
              & G(Lat,Nlon,Nt)
      DIMENSION W(1) , Wts(Nlat) , Wfft(1) , Pmn(Nlat,Late,3)
!     set gs array internally in shagc1
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Lat
               G(i,j,k) = Gs(i,j,k)
            ENDDO
         ENDDO
      ENDDO
!     do fourier transform
      DO k = 1 , Nt
         CALL DHRFFTF(Lat,Nlon,G(1,1,k),Lat,Wfft,Pmn)
      ENDDO
!     scale result
      sfn = 2.0/FLOAT(Nlon)
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Lat
               G(i,j,k) = sfn*G(i,j,k)
            ENDDO
         ENDDO
      ENDDO
!     compute using gaussian quadrature
!     a(n,m) = s (ga(theta,m)*pnm(theta)*sin(theta)*dtheta)
!     b(n,m) = s (gb(theta,m)*pnm(theta)*sin(theta)*dtheta)
!     here ga,gb are the cos(phi),sin(phi) coefficients of
!     the fourier expansion of g(theta,phi) in phi.  as a result
!     of the above fourier transform they are stored in array
!     g as follows:
!     for each theta(i) and k= l-1
!     ga(0),ga(1),gb(1),ga(2),gb(2),...,ga(k-1),gb(k-1),ga(k)
!     correspond to (in the case nlon=l+l-2)
!     g(i,1),g(i,2),g(i,3),g(i,4),g(i,5),...,g(i,2l-4),g(i,2l-3),g(i,2l-
!     initialize coefficients to zero
      DO k = 1 , Nt
         DO np1 = 1 , Nlat
            DO mp1 = 1 , L
               A(mp1,np1,k) = 0.0
               B(mp1,np1,k) = 0.0
            ENDDO
         ENDDO
      ENDDO
!     set m+1 limit on b(m+1) calculation
      lm1 = L
      IF ( Nlon==L+L-2 ) lm1 = L - 1
      IF ( Mode==0 ) THEN
!     for full sphere (mode=0) and even/odd reduction:
!     overwrite g(i) with (g(i)+g(nlat-i+1))*wts(i)
!     overwrite g(nlat-i+1) with (g(i)-g(nlat-i+1))*wts(i)
         nl2 = Nlat/2
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , nl2
                  is = Nlat - i + 1
                  t1 = G(i,j,k)
                  t2 = G(is,j,k)
                  G(i,j,k) = Wts(i)*(t1+t2)
                  G(is,j,k) = Wts(i)*(t1-t2)
               ENDDO
!     adjust equator if necessary(nlat odd)
               IF ( MOD(Nlat,2)/=0 ) G(Late,j,k) = Wts(Late)*G(Late,j,k)
            ENDDO
         ENDDO
!     set m = 0 coefficients first
         m = 0
         CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
         DO k = 1 , Nt
            DO i = 1 , Late
               is = Nlat - i + 1
               DO np1 = 1 , Nlat , 2
!     n even
                  A(1,np1,k) = A(1,np1,k) + G(i,1,k)*Pmn(np1,i,km)
               ENDDO
               DO np1 = 2 , Nlat , 2
!     n odd
                  A(1,np1,k) = A(1,np1,k) + G(is,1,k)*Pmn(np1,i,km)
               ENDDO
            ENDDO
         ENDDO
!     compute coefficients for which b(m,n) is available
         DO mp1 = 2 , lm1
            m = mp1 - 1
            mp2 = m + 2
!     compute pmn for all i and n=m,...,l-1
            CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
               DO i = 1 , Late
                  is = Nlat - i + 1
!     n-m even
                  DO np1 = mp1 , Nlat , 2
                     A(mp1,np1,k) = A(mp1,np1,k) + G(i,2*m,k)           &
                                  & *Pmn(np1,i,km)
                     B(mp1,np1,k) = B(mp1,np1,k) + G(i,2*m+1,k)         &
                                  & *Pmn(np1,i,km)
                  ENDDO
!     n-m odd
                  DO np1 = mp2 , Nlat , 2
                     A(mp1,np1,k) = A(mp1,np1,k) + G(is,2*m,k)          &
                                  & *Pmn(np1,i,km)
                     B(mp1,np1,k) = B(mp1,np1,k) + G(is,2*m+1,k)        &
                                  & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
         IF ( Nlon==L+L-2 ) THEN
!     compute a(l,np1) coefficients only
            m = L - 1
            CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
               DO i = 1 , Late
                  is = Nlat - i + 1
!     n-m even
                  DO np1 = L , Nlat , 2
                     A(L,np1,k) = A(L,np1,k) + 0.5*G(i,Nlon,k)          &
                                & *Pmn(np1,i,km)
                  ENDDO
                  lp1 = L + 1
!     n-m odd
                  DO np1 = lp1 , Nlat , 2
                     A(L,np1,k) = A(L,np1,k) + 0.5*G(is,Nlon,k)         &
                                & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDIF
      ELSE
!     half sphere
!     overwrite g(i) with wts(i)*(g(i)+g(i)) for i=1,...,nlate/2
         nl2 = Nlat/2
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , nl2
                  G(i,j,k) = Wts(i)*(G(i,j,k)+G(i,j,k))
               ENDDO
!     adjust equator separately if a grid point
               IF ( nl2<Late ) G(Late,j,k) = Wts(Late)*G(Late,j,k)
            ENDDO
         ENDDO
!     set m = 0 coefficients first
         m = 0
         CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
         ms = 1
         IF ( Mode==1 ) ms = 2
         DO k = 1 , Nt
            DO i = 1 , Late
               DO np1 = ms , Nlat , 2
                  A(1,np1,k) = A(1,np1,k) + G(i,1,k)*Pmn(np1,i,km)
               ENDDO
            ENDDO
         ENDDO
!     compute coefficients for which b(m,n) is available
         DO mp1 = 2 , lm1
            m = mp1 - 1
            ms = mp1
            IF ( Mode==1 ) ms = mp1 + 1
!     compute pmn for all i and n=m,...,nlat-1
            CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
               DO i = 1 , Late
                  DO np1 = ms , Nlat , 2
                     A(mp1,np1,k) = A(mp1,np1,k) + G(i,2*m,k)           &
                                  & *Pmn(np1,i,km)
                     B(mp1,np1,k) = B(mp1,np1,k) + G(i,2*m+1,k)         &
                                  & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
         IF ( Nlon==L+L-2 ) THEN
!     compute coefficient a(l,np1) only
            m = L - 1
            CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
            ns = L
            IF ( Mode==1 ) ns = L + 1
            DO k = 1 , Nt
               DO i = 1 , Late
                  DO np1 = ns , Nlat , 2
                     A(L,np1,k) = A(L,np1,k) + 0.5*G(i,Nlon,k)          &
                                & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDIF
      ENDIF
      END SUBROUTINE DSHAGC1

      
      SUBROUTINE DSHAGCI(Nlat,Nlon,Wshagc,Lshagc,Dwork,Ldwork,Ierror)
      IMPLICIT NONE
      INTEGER i1 , i2 , i3 , i4 , i5 , i6 , i7 , idth , idwts , Ierror ,&
            & iw , l , l1 , l2 , late , Ldwork , Lshagc , Nlat , Nlon
      DOUBLE PRECISION Wshagc
!     this subroutine must be called before calling shagc with
!     fixed nlat,nlon. it precomputes quantites such as the gaussian
!     points and weights, m=0,m=1 legendre polynomials, recursion
!     recursion coefficients.
      DIMENSION Wshagc(Lshagc)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
!     set triangular truncation limit for spherical harmonic basis
      l = MIN0((Nlon+2)/2,Nlat)
!     set equator or nearest point (if excluded) pointer
      late = (Nlat+MOD(Nlat,2))/2
      l1 = l
      l2 = late
      Ierror = 3
!     check permanent work space length
      IF ( Lshagc<Nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+Nlon+15 ) RETURN
      Ierror = 4
      IF ( Ldwork<Nlat*(Nlat+4) ) RETURN
      Ierror = 0
!     set pointers
      i1 = 1
      i2 = i1 + Nlat
      i3 = i2 + Nlat*late
      i4 = i3 + Nlat*late
      i5 = i4 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i6 = i5 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i7 = i6 + l*(l-1)/2 + (Nlat-l)*(l-1)
!     set indices in temp work for double precision gaussian wts and pts
      idth = 1
      idwts = idth + Nlat
      iw = idwts + Nlat
      CALL DSHAGCI1(Nlat,Nlon,l,late,Wshagc(i1),Wshagc(i2),Wshagc(i3),   &
                 & Wshagc(i4),Wshagc(i5),Wshagc(i6),Wshagc(i7),         &
                 & Dwork(idth),Dwork(idwts),Dwork(iw),Ierror)
      IF ( Ierror/=0 ) Ierror = 5
    END SUBROUTINE DSHAGCI

    
    SUBROUTINE DSHAGCI1(Nlat,Nlon,L,Late,Wts,P0n,P1n,Abel,Bbel,Cbel,   &
                       & Wfft,Dtheta,Dwts,Work,Ier)
      IMPLICIT NONE
      DOUBLE PRECISION Abel , Bbel , Cbel , P0n , P1n , Wfft , Wts
      INTEGER i , Ier , imn , IMNDX , INDX , L , Late , lw , m , mlim , &
            & n , Nlat , Nlon , np1
      DIMENSION Wts(Nlat) , P0n(Nlat,Late) , P1n(Nlat,Late) , Abel(1) , &
              & Bbel(1) , Cbel(1) , Wfft(1)
      DOUBLE PRECISION pb , Dtheta(Nlat) , Dwts(Nlat) , Work(*)
!     compute the nlat  gaussian points and weights, the
!     m=0,1 legendre polys for gaussian points and all n,
!     and the legendre recursion coefficients
!     define index function used in storing
!     arrays for recursion coefficients (functions of (m,n))
!     the index function indx(m,n) is defined so that
!     the pairs (m,n) map to [1,2,...,indx(l-1,l-1)] with no
!     "holes" as m varies from 2 to n and n varies from 2 to l-1.
!     (m=0,1 are set from p0n,p1n for all n)
!     define for 2.le.n.le.l-1
      INDX(m,n) = (n-1)*(n-2)/2 + m - 1
!     define index function for l.le.n.le.nlat
      IMNDX(m,n) = L*(L-1)/2 + (n-L-1)*(L-1) + m - 1
!     preset quantites for fourier transform
      CALL DHRFFTI(Nlon,Wfft)
!     compute double precision gaussian points and weights
!     lw = 4*nlat*(nlat+1)+2
      lw = Nlat*(Nlat+2)
      CALL DGAQD(Nlat,Dtheta,Dwts,Work,lw,Ier)
      IF ( Ier/=0 ) RETURN
!     store gaussian weights single precision to save computation
!     in inner loops in analysis
      DO i = 1 , Nlat
         Wts(i) = Dwts(i)
      ENDDO
!     initialize p0n,p1n using double precision dnlfk,dnlft
      DO np1 = 1 , Nlat
         DO i = 1 , Late
            P0n(np1,i) = 0.0
            P1n(np1,i) = 0.0
         ENDDO
      ENDDO
!     compute m=n=0 legendre polynomials for all theta(i)
      np1 = 1
      n = 0
      m = 0
      CALL DDNLFK(m,n,Work)
      DO i = 1 , Late
         CALL DDNLFT(m,n,Dtheta(i),Work,pb)
         P0n(1,i) = pb
      ENDDO
!     compute p0n,p1n for all theta(i) when n.gt.0
      DO np1 = 2 , Nlat
         n = np1 - 1
         m = 0
         CALL DDNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DDNLFT(m,n,Dtheta(i),Work,pb)
            P0n(np1,i) = pb
         ENDDO
!     compute m=1 legendre polynomials for all n and theta(i)
         m = 1
         CALL DDNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DDNLFT(m,n,Dtheta(i),Work,pb)
            P1n(np1,i) = pb
         ENDDO
      ENDDO
!     compute and store swarztrauber recursion coefficients
!     for 2.le.m.le.n and 2.le.n.le.nlat in abel,bbel,cbel
      DO n = 2 , Nlat
         mlim = MIN0(n,L)
         DO m = 2 , mlim
            imn = INDX(m,n)
            IF ( n>=L ) imn = IMNDX(m,n)
            Abel(imn) = SQRT(FLOAT((2*n+1)*(m+n-2)*(m+n-3))/FLOAT(((2*n-&
                      & 3)*(m+n-1)*(m+n))))
            Bbel(imn) = SQRT(FLOAT((2*n+1)*(n-m-1)*(n-m))/FLOAT(((2*n-3)&
                      & *(m+n-1)*(m+n))))
            Cbel(imn) = SQRT(FLOAT((n-m+1)*(n-m+2))/FLOAT(((n+m-1)*(n+m)&
                      & )))
         ENDDO
      ENDDO
      END SUBROUTINE DSHAGCI1

