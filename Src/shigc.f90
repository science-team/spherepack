!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
! ... file shigc.f
!
!     this file contains code and documentation for subroutine shigc
!
! ... files which must be loaded with shigc.f
!
!     sphcom.f, hrfft.f, gaqd.f
!
!     3/6/98
!
! *** shigc is functionally the same as shagci or shsgci.  It
!     it included in spherepack3.0 because legacy codes, using
!     the older version of spherepack call shigc to initialize
!     the saved work space wshigc, for either shagc or shsgc
!
!     subroutine shigc(nlat,nlon,wshigc,lshigc,dwork,ldwork,ierror)
!
!     subroutine shigc initializes the array wshigc which can then
!     be used repeatedly by subroutines shsgc or shagc. it precomputes
!     and stores in wshigc quantities such as gaussian weights,
!     legendre polynomial coefficients, and fft trigonometric tables.
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are compu
!            in radians in theta(1),...,theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid poi
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than or equal to 4. the efficiency of the computation is
!            improved when nlon is a product of small prime numbers.
!
!     wshigc an array which must be initialized by subroutine shigc.
!            once initialized, wshigc can be used repeatedly by shsgc
!            or shagc as long as nlat and nlon remain unchanged.  wshigc
!            must not be altered between calls of shsgc or shagc.
!
!     lshigc the dimension of the array wshigc as it appears in the
!            program that calls shsgc or shagc. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshigc must be at least
!
!                  nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+nlon+15
!
!     dwork  a double precision work array that does not have to be saved.
!
!     ldwork the dimension of the array dwork as it appears in the
!            program that calls shigc. ldwork must be at least
!
!               nlat*(nlat+4)
!
!     output parameter
!
!     wshigc an array which must be initialized before calling shsgc or shagc.
!            once initialized, wshigc can be used repeatedly by shsgc or shagc
!            as long as nlat and nlon remain unchanged.  wshigc must not
!            altered between calls of shsgc or shagc
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of lshigc
!            = 4  error in the specification of ldwork
!            = 5  failure in gaqd to compute gaussian points
!                 (due to failure in eigenvalue routine)
!
!
! ****************************************************************
      SUBROUTINE SHIGC(Nlat,Nlon,Wshigc,Lshigc,Dwork,Ldwork,Ierror)
      IMPLICIT NONE
      INTEGER i1 , i2 , i3 , i4 , i5 , i6 , i7 , idth , idwts , Ierror ,&
            & iw , l , l1 , l2 , late , Ldwork , Lshigc , Nlat , Nlon
      REAL Wshigc
!     this subroutine must be called before calling shsgc/shagc with
!     fixed nlat,nlon. it precomputes quantites such as the gaussian
!     points and weights, m=0,m=1 legendre polynomials, recursion
!     recursion coefficients.
      DIMENSION Wshigc(Lshigc)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
!     set triangular truncation limit for spherical harmonic basis
      l = MIN0((Nlon+2)/2,Nlat)
!     set equator or nearest point (if excluded) pointer
      late = (Nlat+MOD(Nlat,2))/2
      l1 = l
      l2 = late
      Ierror = 3
!     check permanent work space length
      IF ( Lshigc<Nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+Nlon+15 ) RETURN
      Ierror = 4
!     if (lwork.lt.4*nlat*(nlat+2)+2) return
      IF ( Ldwork<Nlat*(Nlat+4) ) RETURN
      Ierror = 0
!     set pointers
      i1 = 1
      i2 = i1 + Nlat
      i3 = i2 + Nlat*late
      i4 = i3 + Nlat*late
      i5 = i4 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i6 = i5 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i7 = i6 + l*(l-1)/2 + (Nlat-l)*(l-1)
!     set indices in temp work for double precision gaussian wts and pts
      idth = 1
!     idwts = idth+2*nlat
!     iw = idwts+2*nlat
      idwts = idth + Nlat
      iw = idwts + Nlat
      CALL SHIGC1(Nlat,Nlon,l,late,Wshigc(i1),Wshigc(i2),Wshigc(i3),    &
                & Wshigc(i4),Wshigc(i5),Wshigc(i6),Wshigc(i7),          &
                & Dwork(idth),Dwork(idwts),Dwork(iw),Ierror)
      IF ( Ierror/=0 ) Ierror = 5
    END SUBROUTINE SHIGC


    SUBROUTINE SHIGC1(Nlat,Nlon,L,Late,Wts,P0n,P1n,Abel,Bbel,Cbel,    &
                      & Wfft,Dtheta,Dwts,Work,Ier)
      IMPLICIT NONE
      REAL Abel , Bbel , Cbel , P0n , P1n , Wfft , Wts
      INTEGER i , Ier , imn , IMNDX , INDX , L , Late , lw , m , mlim , &
            & n , Nlat , Nlon , np1
      DIMENSION Wts(Nlat) , P0n(Nlat,Late) , P1n(Nlat,Late) , Abel(1) , &
              & Bbel(1) , Cbel(1) , Wfft(1) , Dtheta(Nlat) , Dwts(Nlat)
      DOUBLE PRECISION pb , Dtheta , Dwts , Work(*)
!     compute the nlat  gaussian points and weights, the
!     m=0,1 legendre polys for gaussian points and all n,
!     and the legendre recursion coefficients
!     define index function used in storing
!     arrays for recursion coefficients (functions of (m,n))
!     the index function indx(m,n) is defined so that
!     the pairs (m,n) map to [1,2,...,indx(l-1,l-1)] with no
!     "holes" as m varies from 2 to n and n varies from 2 to l-1.
!     (m=0,1 are set from p0n,p1n for all n)
!     define for 2.le.n.le.l-1
      INDX(m,n) = (n-1)*(n-2)/2 + m - 1
!     define index function for l.le.n.le.nlat
      IMNDX(m,n) = L*(L-1)/2 + (n-L-1)*(L-1) + m - 1
!     preset quantites for fourier transform
      CALL HRFFTI(Nlon,Wfft)
!     compute double precision gaussian points and weights
!     lw = 4*nlat*(nlat+1)+2
      lw = Nlat*(Nlat+2)
      CALL GAQD(Nlat,Dtheta,Dwts,Work,lw,Ier)
      IF ( Ier/=0 ) RETURN
!     store gaussian weights single precision to save computation
!     in inner loops in analysis
      DO i = 1 , Nlat
         Wts(i) = Dwts(i)
      ENDDO
!     initialize p0n,p1n using double precision dnlfk,dnlft
      DO np1 = 1 , Nlat
         DO i = 1 , Late
            P0n(np1,i) = 0.0
            P1n(np1,i) = 0.0
         ENDDO
      ENDDO
!     compute m=n=0 legendre polynomials for all theta(i)
      np1 = 1
      n = 0
      m = 0
      CALL DNLFK(m,n,Work)
      DO i = 1 , Late
         CALL DNLFT(m,n,Dtheta(i),Work,pb)
         P0n(1,i) = pb
      ENDDO
!     compute p0n,p1n for all theta(i) when n.gt.0
      DO np1 = 2 , Nlat
         n = np1 - 1
         m = 0
         CALL DNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DNLFT(m,n,Dtheta(i),Work,pb)
            P0n(np1,i) = pb
         ENDDO
!     compute m=1 legendre polynomials for all n and theta(i)
         m = 1
         CALL DNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DNLFT(m,n,Dtheta(i),Work,pb)
            P1n(np1,i) = pb
         ENDDO
      ENDDO
!     compute and store swarztrauber recursion coefficients
!     for 2.le.m.le.n and 2.le.n.le.nlat in abel,bbel,cbel
      DO n = 2 , Nlat
         mlim = MIN0(n,L)
         DO m = 2 , mlim
            imn = INDX(m,n)
            IF ( n>=L ) imn = IMNDX(m,n)
            Abel(imn) = SQRT(FLOAT((2*n+1)*(m+n-2)*(m+n-3))/FLOAT(((2*n-&
                      & 3)*(m+n-1)*(m+n))))
            Bbel(imn) = SQRT(FLOAT((2*n+1)*(n-m-1)*(n-m))/FLOAT(((2*n-3)&
                      & *(m+n-1)*(m+n))))
            Cbel(imn) = SQRT(FLOAT((n-m+1)*(n-m+2))/FLOAT(((n+m-1)*(n+m)&
                      & )))
         ENDDO
      ENDDO
      END SUBROUTINE SHIGC1


      SUBROUTINE DSHIGC(Nlat,Nlon,Wshigc,Lshigc,Dwork,Ldwork,Ierror)
      IMPLICIT NONE
      INTEGER i1 , i2 , i3 , i4 , i5 , i6 , i7 , idth , idwts , Ierror ,&
            & iw , l , l1 , l2 , late , Ldwork , Lshigc , Nlat , Nlon
      DOUBLE PRECISION Wshigc
!     this subroutine must be called before calling shsgc/shagc with
!     fixed nlat,nlon. it precomputes quantites such as the gaussian
!     points and weights, m=0,m=1 legendre polynomials, recursion
!     recursion coefficients.
      DIMENSION Wshigc(Lshigc)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
!     set triangular truncation limit for spherical harmonic basis
      l = MIN0((Nlon+2)/2,Nlat)
!     set equator or nearest point (if excluded) pointer
      late = (Nlat+MOD(Nlat,2))/2
      l1 = l
      l2 = late
      Ierror = 3
!     check permanent work space length
      IF ( Lshigc<Nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+Nlon+15 ) RETURN
      Ierror = 4
!     if (lwork.lt.4*nlat*(nlat+2)+2) return
      IF ( Ldwork<Nlat*(Nlat+4) ) RETURN
      Ierror = 0
!     set pointers
      i1 = 1
      i2 = i1 + Nlat
      i3 = i2 + Nlat*late
      i4 = i3 + Nlat*late
      i5 = i4 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i6 = i5 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i7 = i6 + l*(l-1)/2 + (Nlat-l)*(l-1)
!     set indices in temp work for double precision gaussian wts and pts
      idth = 1
!     idwts = idth+2*nlat
!     iw = idwts+2*nlat
      idwts = idth + Nlat
      iw = idwts + Nlat
      CALL DSHIGC1(Nlat,Nlon,l,late,Wshigc(i1),Wshigc(i2),Wshigc(i3),    &
                & Wshigc(i4),Wshigc(i5),Wshigc(i6),Wshigc(i7),          &
                & Dwork(idth),Dwork(idwts),Dwork(iw),Ierror)
      IF ( Ierror/=0 ) Ierror = 5
    END SUBROUTINE DSHIGC


      SUBROUTINE DSHIGC1(Nlat,Nlon,L,Late,Wts,P0n,P1n,Abel,Bbel,Cbel,    &
                      & Wfft,Dtheta,Dwts,Work,Ier)
      IMPLICIT NONE
      DOUBLE PRECISION Abel , Bbel , Cbel , P0n , P1n , Wfft , Wts
      INTEGER i , Ier , imn , IMNDX , INDX , L , Late , lw , m , mlim , &
            & n , Nlat , Nlon , np1
      DIMENSION Wts(Nlat) , P0n(Nlat,Late) , P1n(Nlat,Late) , Abel(1) , &
              & Bbel(1) , Cbel(1) , Wfft(1) , Dtheta(Nlat) , Dwts(Nlat)
      DOUBLE PRECISION pb , Dtheta , Dwts , Work(*)
!     compute the nlat  gaussian points and weights, the
!     m=0,1 legendre polys for gaussian points and all n,
!     and the legendre recursion coefficients
!     define index function used in storing
!     arrays for recursion coefficients (functions of (m,n))
!     the index function indx(m,n) is defined so that
!     the pairs (m,n) map to [1,2,...,indx(l-1,l-1)] with no
!     "holes" as m varies from 2 to n and n varies from 2 to l-1.
!     (m=0,1 are set from p0n,p1n for all n)
!     define for 2.le.n.le.l-1
      INDX(m,n) = (n-1)*(n-2)/2 + m - 1
!     define index function for l.le.n.le.nlat
      IMNDX(m,n) = L*(L-1)/2 + (n-L-1)*(L-1) + m - 1
!     preset quantites for fourier transform
      CALL DHRFFTI(Nlon,Wfft)
!     compute double precision gaussian points and weights
!     lw = 4*nlat*(nlat+1)+2
      lw = Nlat*(Nlat+2)
      CALL DGAQD(Nlat,Dtheta,Dwts,Work,lw,Ier)
      IF ( Ier/=0 ) RETURN
!     store gaussian weights single precision to save computation
!     in inner loops in analysis
      DO i = 1 , Nlat
         Wts(i) = Dwts(i)
      ENDDO
!     initialize p0n,p1n using double precision dnlfk,dnlft
      DO np1 = 1 , Nlat
         DO i = 1 , Late
            P0n(np1,i) = 0.0
            P1n(np1,i) = 0.0
         ENDDO
      ENDDO
!     compute m=n=0 legendre polynomials for all theta(i)
      np1 = 1
      n = 0
      m = 0
      CALL DDNLFK(m,n,Work)
      DO i = 1 , Late
         CALL DDNLFT(m,n,Dtheta(i),Work,pb)
         P0n(1,i) = pb
      ENDDO
!     compute p0n,p1n for all theta(i) when n.gt.0
      DO np1 = 2 , Nlat
         n = np1 - 1
         m = 0
         CALL DDNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DDNLFT(m,n,Dtheta(i),Work,pb)
            P0n(np1,i) = pb
         ENDDO
!     compute m=1 legendre polynomials for all n and theta(i)
         m = 1
         CALL DDNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DDNLFT(m,n,Dtheta(i),Work,pb)
            P1n(np1,i) = pb
         ENDDO
      ENDDO
!     compute and store swarztrauber recursion coefficients
!     for 2.le.m.le.n and 2.le.n.le.nlat in abel,bbel,cbel
      DO n = 2 , Nlat
         mlim = MIN0(n,L)
         DO m = 2 , mlim
            imn = INDX(m,n)
            IF ( n>=L ) imn = IMNDX(m,n)
            Abel(imn) = SQRT(FLOAT((2*n+1)*(m+n-2)*(m+n-3))/FLOAT(((2*n-&
                      & 3)*(m+n-1)*(m+n))))
            Bbel(imn) = SQRT(FLOAT((2*n+1)*(n-m-1)*(n-m))/FLOAT(((2*n-3)&
                      & *(m+n-1)*(m+n))))
            Cbel(imn) = SQRT(FLOAT((n-m+1)*(n-m+2))/FLOAT(((n+m-1)*(n+m)&
                      & )))
         ENDDO
      ENDDO
      END SUBROUTINE DSHIGC1
