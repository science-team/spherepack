!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
! ... file shsgc.f
!
!     this file contains code and documentation for subroutines
!     shsgc and shsgci
!
! ... files which must be loaded with shsgc.f
!
!     sphcom.f, hrfft.f, gaqd.f
!
!     subroutine shsgc(nlat,nlon,isym,nt,g,idg,jdg,a,b,mdab,ndab,
!    +                 wshsgc,lshsgc,work,lwork,ierror)
!
!     subroutine shsgc performs the spherical harmonic synthesis
!     on the arrays a and b and stores the result in the array g.
!     the synthesis is performed on an equally spaced longitude grid
!     and a gaussian colatitude grid.  the associated legendre functions
!     are recomputed rather than stored as they are in subroutine
!     shsgs.  the synthesis is described below at output parameter
!     g.
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are compu
!            in radians in theta(1),...,theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid poi
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than or equal to 4. the efficiency of the computation is
!            improved when nlon is a product of small prime numbers.
!
!     isym   = 0  no symmetries exist about the equator. the synthesis
!                 is performed on the entire sphere.  i.e. on the
!                 array g(i,j) for i=1,...,nlat and j=1,...,nlon.
!                 (see description of g below)
!
!            = 1  g is antisymmetric about the equator. the synthesis
!                 is performed on the northern hemisphere only.  i.e.
!                 if nlat is odd the synthesis is performed on the
!                 array g(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even the synthesis is performed on the
!                 array g(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!
!            = 2  g is symmetric about the equator. the synthesis is
!                 performed on the northern hemisphere only.  i.e.
!                 if nlat is odd the synthesis is performed on the
!                 array g(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even the synthesis is performed on the
!                 array g(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!     nt     the number of syntheses.  in the program that calls shsgc,
!            the arrays g,a and b can be three dimensional in which
!            case multiple synthesis will be performed.  the third
!            index is the synthesis index which assumes the values
!            k=1,...,nt.  for a single synthesis set nt=1. the
!            discription of the remaining parameters is simplified
!            by assuming that nt=1 or that the arrays g,a and b
!            have only two dimensions.
!
!     idg    the first dimension of the array g as it appears in the
!            program that calls shsgc. if isym equals zero then idg
!            must be at least nlat.  if isym is nonzero then idg must
!            be at least nlat/2 if nlat is even or at least (nlat+1)/2
!            if nlat is odd.
!
!     jdg    the second dimension of the array g as it appears in the
!            program that calls shsgc. jdg must be at least nlon.
!
!     mdab   the first dimension of the arrays a and b as it appears
!            in the program that calls shsgc. mdab must be at least
!            min0((nlon+2)/2,nlat) if nlon is even or at least
!            min0((nlon+1)/2,nlat) if nlon is odd
!
!     ndab   the second dimension of the arrays a and b as it appears
!            in the program that calls shsgc. ndab must be at least nlat
!
!     a,b    two or three dimensional arrays (see the input parameter
!            nt) that contain the coefficients in the spherical harmonic
!            expansion of g(i,j) given below at the definition of the
!            output parameter g.  a(m,n) and b(m,n) are defined for
!            indices m=1,...,mmax and n=m,...,nlat where mmax is the
!            maximum (plus one) longitudinal wave number given by
!            mmax = min0(nlat,(nlon+2)/2) if nlon is even or
!            mmax = min0(nlat,(nlon+1)/2) if nlon is odd.
!
!     wshsgc an array which must be initialized by subroutine shsgci.
!            once initialized, wshsgc can be used repeatedly by shsgc
!            as long as nlat and nlon remain unchanged.  wshsgc must
!            not be altered between calls of shsgc.
!
!     lshsgc the dimension of the array wshsgc as it appears in the
!            program that calls shsgc. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshsgc must be at least
!
!               nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+nlon+15
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls shsgc. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            if isym is zero then lwork must be at least
!
!                      nlat*(nlon*nt+max0(3*l2,nlon))
!
!            if isym is not zero then lwork must be at least
!
!                      l2*(nlon*nt+max0(3*nlat,nlon))
!
!     **************************************************************
!
!     output parameters
!
!     g      a two or three dimensional array (see input parameter nt)
!            that contains the discrete function which is synthesized.
!            g(i,j) contains the value of the function at the gaussian
!            colatitude point theta(i) and longitude point
!            phi(j) = (j-1)*2*pi/nlon. the index ranges are defined
!            above at the input parameter isym.  for isym=0, g(i,j)
!            is given by the the equations listed below.  symmetric
!            versions are used when isym is greater than zero.
!
!     the normalized associated legendre functions are given by
!
!     pbar(m,n,theta) = sqrt((2*n+1)*factorial(n-m)/(2*factorial(n+m)))
!                       *sin(theta)**m/(2**n*factorial(n)) times the
!                       (n+m)th derivative of (x**2-1)**n with respect
!                       to x=cos(theta)
!
!
!     define the maximum (plus one) longitudinal wave number
!     as   mmax = min0(nlat,(nlon+2)/2) if nlon is even or
!          mmax = min0(nlat,(nlon+1)/2) if nlon is odd.
!
!     then g(i,j) = the sum from n=0 to n=nlat-1 of
!
!                   .5*pbar(0,n,theta(i))*a(1,n+1)
!
!              plus the sum from m=1 to m=mmax-1 of
!
!                   the sum from n=m to n=nlat-1 of
!
!              pbar(m,n,theta(i))*(a(m+1,n+1)*cos(m*phi(j))
!                                    -b(m+1,n+1)*sin(m*phi(j)))
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of isym
!            = 4  error in the specification of nt
!            = 5  error in the specification of idg
!            = 6  error in the specification of jdg
!            = 7  error in the specification of mdab
!            = 8  error in the specification of ndab
!            = 9  error in the specification of lwshig
!            = 10 error in the specification of lwork
!
!
! ****************************************************************
!
!     subroutine shsgci(nlat,nlon,wshsgc,lshsgc,dwork,ldwork,ierror)
!
!     subroutine shsgci initializes the array wshsgc which can then
!     be used repeatedly by subroutines shsgc. it precomputes
!     and stores in wshsgc quantities such as gaussian weights,
!     legendre polynomial coefficients, and fft trigonometric tables.
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are compu
!            in radians in theta(1),...,theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid poi
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than or equal to 4. the efficiency of the computation is
!            improved when nlon is a product of small prime numbers.
!
!     wshsgc an array which must be initialized by subroutine shsgci.
!            once initialized, wshsgc can be used repeatedly by shsgc
!            as long as nlat and nlon remain unchanged.  wshsgc must
!            not be altered between calls of shsgc.
!
!     lshsgc the dimension of the array wshsgc as it appears in the
!            program that calls shsgc. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshsgc must be at least
!
!                  nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+nlon+15
!
!     dwork  a double precision work array that does not have to be saved.
!
!     ldwork the dimension of the array dwork as it appears in the
!            program that calls shsgci. ldwork must be at least
!
!                 nlat*(nlat+4)
!
!     output parameter
!
!     wshsgc an array which must be initialized before calling shsgc.
!            once initialized, wshsgc can be used repeatedly by shsgc
!            as long as nlat and nlon remain unchanged.  wshsgc must not
!            altered between calls of shsgc.
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of lshsgc
!            = 4  error in the specification of ldwork
!            = 5  failure in gaqd to compute gaussian points
!                 (due to failure in eigenvalue routine)
!
!
! ****************************************************************
      SUBROUTINE SHSGC(Nlat,Nlon,Mode,Nt,G,Idg,Jdg,A,B,Mdab,Ndab,Wshsgc,&
                     & Lshsgc,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL A , B , G , Work , Wshsgc
      INTEGER Idg , Ierror , ifft , ipmn , Jdg , l , l1 , l2 , lat ,    &
            & late , Lshsgc , Lwork , Mdab , Mode , Ndab , Nlat , Nlon ,&
            & Nt
!     subroutine shsgc performs the spherical harmonic synthesis on
!     a gaussian grid using the coefficients in array(s) a,b and returns
!     the results in array(s) g.  the legendre polynomials are computed
!     as needed in this version.
!
      DIMENSION G(Idg,Jdg,1) , A(Mdab,Ndab,1) , B(Mdab,Ndab,1) ,        &
              & Wshsgc(Lshsgc) , Work(Lwork)
!     check input parameters
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Mode<0 .OR. Mode>2 ) RETURN
      Ierror = 4
      IF ( Nt<1 ) RETURN
!     set limit for m iin a(m,n),b(m,n) computation
      l = MIN0((Nlon+2)/2,Nlat)
!     set gaussian point nearest equator pointer
      late = (Nlat+MOD(Nlat,2))/2
!     set number of grid points for analysis/synthesis
      lat = Nlat
      IF ( Mode/=0 ) lat = late
      Ierror = 5
      IF ( Idg<lat ) RETURN
      Ierror = 6
      IF ( Jdg<Nlon ) RETURN
      Ierror = 7
      IF ( Mdab<l ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      l1 = l
      l2 = late
      Ierror = 9
!     check permanent work space length
      IF ( Lshsgc<Nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+Nlon+15 ) RETURN
      Ierror = 10
!     check temporary work space length
      IF ( Mode==0 ) THEN
         IF ( Lwork<Nlat*(Nlon*Nt+MAX0(3*l2,Nlon)) ) RETURN
!     mode.ne.0
      ELSEIF ( Lwork<l2*(Nlon*Nt+MAX0(3*Nlat,Nlon)) ) THEN
         RETURN
      ENDIF
      Ierror = 0
!     starting address  fft values
      ifft = Nlat + 2*Nlat*late + 3*(l*(l-1)/2+(Nlat-l)*(l-1)) + 1
!     set pointers for internal storage of g and legendre polys
      ipmn = lat*Nlon*Nt + 1
      CALL SHSGC1(Nlat,Nlon,l,lat,Mode,G,Idg,Jdg,Nt,A,B,Mdab,Ndab,      &
                & Wshsgc,Wshsgc(ifft),late,Work(ipmn),Work)
    END SUBROUTINE SHSGC

    
    SUBROUTINE SHSGC1(Nlat,Nlon,L,Lat,Mode,Gs,Idg,Jdg,Nt,A,B,Mdab,    &
                      & Ndab,W,Wfft,Late,Pmn,G)
      IMPLICIT NONE
      REAL A , B , G , Gs , Pmn , t1 , t2 , t3 , t4 , W , Wfft
      INTEGER i , Idg , is , j , Jdg , k , km , L , Lat , Late , lm1 ,  &
            & lp1 , m , Mdab , meo , Mode , mp1 , mp2 , ms , Ndab
      INTEGER nl2 , Nlat , Nlon , np1 , ns , Nt
      DIMENSION Gs(Idg,Jdg,Nt) , A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt)
      DIMENSION W(1) , Pmn(Nlat,Late,3) , G(Lat,Nlon,Nt) , Wfft(1)
!     reconstruct fourier coefficients in g on gaussian grid
!     using coefficients in a,b
!     set m+1 limit for b coefficient calculation
      lm1 = L
      IF ( Nlon==L+L-2 ) lm1 = L - 1
!     initialize to zero
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Lat
               G(i,j,k) = 0.0
            ENDDO
         ENDDO
      ENDDO
      IF ( Mode==0 ) THEN
!     set first column in g
         m = 0
!     compute pmn for all i and n=m,...,l-1
         CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
         DO k = 1 , Nt
!     n even
            DO np1 = 1 , Nlat , 2
               DO i = 1 , Late
                  G(i,1,k) = G(i,1,k) + A(1,np1,k)*Pmn(np1,i,km)
               ENDDO
            ENDDO
!     n odd
            nl2 = Nlat/2
            DO np1 = 2 , Nlat , 2
               DO i = 1 , nl2
                  is = Nlat - i + 1
                  G(is,1,k) = G(is,1,k) + A(1,np1,k)*Pmn(np1,i,km)
               ENDDO
            ENDDO
!     restore m=0 coefficents (reverse implicit even/odd reduction)
            DO i = 1 , nl2
               is = Nlat - i + 1
               t1 = G(i,1,k)
               t3 = G(is,1,k)
               G(i,1,k) = t1 + t3
               G(is,1,k) = t1 - t3
            ENDDO
         ENDDO
!     sweep  columns of g for which b is available
         DO mp1 = 2 , lm1
            m = mp1 - 1
            mp2 = m + 2
!     compute pmn for all i and n=m,...,l-1
            CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
!     for n-m even store (g(i,p,k)+g(nlat-i+1,p,k))/2 in g(i,p,k) p=2*m,
!     for i=1,...,late
               DO np1 = mp1 , Nlat , 2
                  DO i = 1 , Late
                     G(i,2*m,k) = G(i,2*m,k) + A(mp1,np1,k)             &
                                & *Pmn(np1,i,km)
                     G(i,2*m+1,k) = G(i,2*m+1,k) + B(mp1,np1,k)         &
                                  & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
!     for n-m odd store g(i,p,k)-g(nlat-i+1,p,k) in g(nlat-i+1,p,k)
!     for i=1,...,nlat/2 (p=2*m,p=2*m+1)
               DO np1 = mp2 , Nlat , 2
                  DO i = 1 , nl2
                     is = Nlat - i + 1
                     G(is,2*m,k) = G(is,2*m,k) + A(mp1,np1,k)           &
                                 & *Pmn(np1,i,km)
                     G(is,2*m+1,k) = G(is,2*m+1,k) + B(mp1,np1,k)       &
                                   & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
!     now set fourier coefficients using even-odd reduction above
               DO i = 1 , nl2
                  is = Nlat - i + 1
                  t1 = G(i,2*m,k)
                  t2 = G(i,2*m+1,k)
                  t3 = G(is,2*m,k)
                  t4 = G(is,2*m+1,k)
                  G(i,2*m,k) = t1 + t3
                  G(i,2*m+1,k) = t2 + t4
                  G(is,2*m,k) = t1 - t3
                  G(is,2*m+1,k) = t2 - t4
               ENDDO
            ENDDO
         ENDDO
!     set last column (using a only)
         IF ( Nlon==L+L-2 ) THEN
            m = L - 1
            CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
!     n-m even
               DO np1 = L , Nlat , 2
                  DO i = 1 , Late
                     G(i,Nlon,k) = G(i,Nlon,k) + 2.0*A(L,np1,k)         &
                                 & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
               lp1 = L + 1
!     n-m odd
               DO np1 = lp1 , Nlat , 2
                  DO i = 1 , nl2
                     is = Nlat - i + 1
                     G(is,Nlon,k) = G(is,Nlon,k) + 2.0*A(L,np1,k)       &
                                  & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
               DO i = 1 , nl2
                  is = Nlat - i + 1
                  t1 = G(i,Nlon,k)
                  t3 = G(is,Nlon,k)
                  G(i,Nlon,k) = t1 + t3
                  G(is,Nlon,k) = t1 - t3
               ENDDO
            ENDDO
         ENDIF
      ELSE
!     half sphere (mode.ne.0)
!     set first column in g
         m = 0
         meo = 1
         IF ( Mode==1 ) meo = 2
         ms = m + meo
!     compute pmn for all i and n=m,...,l-1
         CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
         DO k = 1 , Nt
            DO np1 = ms , Nlat , 2
               DO i = 1 , Late
                  G(i,1,k) = G(i,1,k) + A(1,np1,k)*Pmn(np1,i,km)
               ENDDO
            ENDDO
         ENDDO
!     sweep interior columns of g
         DO mp1 = 2 , lm1
            m = mp1 - 1
            ms = m + meo
!     compute pmn for all i and n=m,...,l-1
            CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
               DO np1 = ms , Nlat , 2
                  DO i = 1 , Late
                     G(i,2*m,k) = G(i,2*m,k) + A(mp1,np1,k)             &
                                & *Pmn(np1,i,km)
                     G(i,2*m+1,k) = G(i,2*m+1,k) + B(mp1,np1,k)         &
                                  & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
         IF ( Nlon==L+L-2 ) THEN
!     set last column
            m = L - 1
            CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
            ns = L
            IF ( Mode==1 ) ns = L + 1
            DO k = 1 , Nt
               DO i = 1 , Late
                  DO np1 = ns , Nlat , 2
                     G(i,Nlon,k) = G(i,Nlon,k) + 2.0*A(L,np1,k)         &
                                 & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDIF
      ENDIF
!     do inverse fourier transform
      DO k = 1 , Nt
         CALL HRFFTB(Lat,Nlon,G(1,1,k),Lat,Wfft,Pmn)
      ENDDO
!     scale output in gs
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Lat
               Gs(i,j,k) = 0.5*G(i,j,k)
            ENDDO
         ENDDO
      ENDDO
    END SUBROUTINE SHSGC1

    
    SUBROUTINE SHSGCI(Nlat,Nlon,Wshsgc,Lshsgc,Dwork,Ldwork,Ierror)
      IMPLICIT NONE
      INTEGER i1 , i2 , i3 , i4 , i5 , i6 , i7 , idth , idwts , Ierror ,&
            & iw , l , l1 , l2 , late , Ldwork , Lshsgc , Nlat , Nlon
      REAL Wshsgc
!     this subroutine must be called before calling shsgc with
!     fixed nlat,nlon. it precomputes quantites such as the gaussian
!     points and weights, m=0,m=1 legendre polynomials, recursion
!     recursion coefficients.
      DIMENSION Wshsgc(Lshsgc)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
!     set triangular truncation limit for spherical harmonic basis
      l = MIN0((Nlon+2)/2,Nlat)
!     set equator or nearest point (if excluded) pointer
      late = (Nlat+MOD(Nlat,2))/2
      l1 = l
      l2 = late
      Ierror = 3
!     check permanent work space length
      IF ( Lshsgc<Nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+Nlon+15 ) RETURN
      Ierror = 4
      IF ( Ldwork<Nlat*(Nlat+4) ) RETURN
      Ierror = 0
!     set pointers
      i1 = 1
      i2 = i1 + Nlat
      i3 = i2 + Nlat*late
      i4 = i3 + Nlat*late
      i5 = i4 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i6 = i5 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i7 = i6 + l*(l-1)/2 + (Nlat-l)*(l-1)
!     set indices in temp work for double precision gaussian wts and pts
      idth = 1
      idwts = idth + Nlat
      iw = idwts + Nlat
      CALL SHSGCI1(Nlat,Nlon,l,late,Wshsgc(i1),Wshsgc(i2),Wshsgc(i3),   &
                 & Wshsgc(i4),Wshsgc(i5),Wshsgc(i6),Wshsgc(i7),         &
                 & Dwork(idth),Dwork(idwts),Dwork(iw),Ierror)
      IF ( Ierror/=0 ) Ierror = 5
      END SUBROUTINE SHSGCI

      
      SUBROUTINE SHSGCI1(Nlat,Nlon,L,Late,Wts,P0n,P1n,Abel,Bbel,Cbel,   &
                       & Wfft,Dtheta,Dwts,Work,Ier)
      IMPLICIT NONE
      REAL Abel , Bbel , Cbel , P0n , P1n , Wfft , Wts
      INTEGER i , Ier , imn , IMNDX , INDX , L , Late , lw , m , mlim , &
            & n , Nlat , Nlon , np1
      DIMENSION Wts(Nlat) , P0n(Nlat,Late) , P1n(Nlat,Late) , Abel(1) , &
              & Bbel(1) , Cbel(1) , Wfft(1) , Dtheta(Nlat) , Dwts(Nlat)
      DOUBLE PRECISION pb , Dtheta , Dwts , Work(*)
!     compute the nlat  gaussian points and weights, the
!     m=0,1 legendre polys for gaussian points and all n,
!     and the legendre recursion coefficients
!     define index function used in storing
!     arrays for recursion coefficients (functions of (m,n))
!     the index function indx(m,n) is defined so that
!     the pairs (m,n) map to [1,2,...,indx(l-1,l-1)] with no
!     "holes" as m varies from 2 to n and n varies from 2 to l-1.
!     (m=0,1 are set from p0n,p1n for all n)
!     define for 2.le.n.le.l-1
      INDX(m,n) = (n-1)*(n-2)/2 + m - 1
!     define index function for l.le.n.le.nlat
      IMNDX(m,n) = L*(L-1)/2 + (n-L-1)*(L-1) + m - 1
!     preset quantites for fourier transform
      CALL HRFFTI(Nlon,Wfft)
!     compute double precision gaussian points and weights
!     lw = 4*nlat*(nlat+1)+2
      lw = Nlat*(Nlat+2)
      CALL GAQD(Nlat,Dtheta,Dwts,Work,lw,Ier)
      IF ( Ier/=0 ) RETURN
!     store gaussian weights single precision to save computation
!     in inner loops in analysis
      DO i = 1 , Nlat
         Wts(i) = Dwts(i)
      ENDDO
!     initialize p0n,p1n using double precision dnlfk,dnlft
      DO np1 = 1 , Nlat
         DO i = 1 , Late
            P0n(np1,i) = 0.0
            P1n(np1,i) = 0.0
         ENDDO
      ENDDO
!     compute m=n=0 legendre polynomials for all theta(i)
      np1 = 1
      n = 0
      m = 0
      CALL DNLFK(m,n,Work)
      DO i = 1 , Late
         CALL DNLFT(m,n,Dtheta(i),Work,pb)
         P0n(1,i) = pb
      ENDDO
!     compute p0n,p1n for all theta(i) when n.gt.0
      DO np1 = 2 , Nlat
         n = np1 - 1
         m = 0
         CALL DNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DNLFT(m,n,Dtheta(i),Work,pb)
            P0n(np1,i) = pb
         ENDDO
!     compute m=1 legendre polynomials for all n and theta(i)
         m = 1
         CALL DNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DNLFT(m,n,Dtheta(i),Work,pb)
            P1n(np1,i) = pb
         ENDDO
      ENDDO
!     compute and store swarztrauber recursion coefficients
!     for 2.le.m.le.n and 2.le.n.le.nlat in abel,bbel,cbel
      DO n = 2 , Nlat
         mlim = MIN0(n,L)
         DO m = 2 , mlim
            imn = INDX(m,n)
            IF ( n>=L ) imn = IMNDX(m,n)
            Abel(imn) = SQRT(FLOAT((2*n+1)*(m+n-2)*(m+n-3))/FLOAT(((2*n-&
                      & 3)*(m+n-1)*(m+n))))
            Bbel(imn) = SQRT(FLOAT((2*n+1)*(n-m-1)*(n-m))/FLOAT(((2*n-3)&
                      & *(m+n-1)*(m+n))))
            Cbel(imn) = SQRT(FLOAT((n-m+1)*(n-m+2))/FLOAT(((n+m-1)*(n+m)&
                      & )))
         ENDDO
      ENDDO
      END SUBROUTINE SHSGCI1


      SUBROUTINE DSHSGC(Nlat,Nlon,Mode,Nt,G,Idg,Jdg,A,B,Mdab,Ndab,Wshsgc,&
                     & Lshsgc,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION A , B , G , Work , Wshsgc
      INTEGER Idg , Ierror , ifft , ipmn , Jdg , l , l1 , l2 , lat ,    &
            & late , Lshsgc , Lwork , Mdab , Mode , Ndab , Nlat , Nlon ,&
            & Nt
!     subroutine shsgc performs the spherical harmonic synthesis on
!     a gaussian grid using the coefficients in array(s) a,b and returns
!     the results in array(s) g.  the legendre polynomials are computed
!     as needed in this version.
!
      DIMENSION G(Idg,Jdg,1) , A(Mdab,Ndab,1) , B(Mdab,Ndab,1) ,        &
              & Wshsgc(Lshsgc) , Work(Lwork)
!     check input parameters
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Mode<0 .OR. Mode>2 ) RETURN
      Ierror = 4
      IF ( Nt<1 ) RETURN
!     set limit for m iin a(m,n),b(m,n) computation
      l = MIN0((Nlon+2)/2,Nlat)
!     set gaussian point nearest equator pointer
      late = (Nlat+MOD(Nlat,2))/2
!     set number of grid points for analysis/synthesis
      lat = Nlat
      IF ( Mode/=0 ) lat = late
      Ierror = 5
      IF ( Idg<lat ) RETURN
      Ierror = 6
      IF ( Jdg<Nlon ) RETURN
      Ierror = 7
      IF ( Mdab<l ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      l1 = l
      l2 = late
      Ierror = 9
!     check permanent work space length
      IF ( Lshsgc<Nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+Nlon+15 ) RETURN
      Ierror = 10
!     check temporary work space length
      IF ( Mode==0 ) THEN
         IF ( Lwork<Nlat*(Nlon*Nt+MAX0(3*l2,Nlon)) ) RETURN
!     mode.ne.0
      ELSEIF ( Lwork<l2*(Nlon*Nt+MAX0(3*Nlat,Nlon)) ) THEN
         RETURN
      ENDIF
      Ierror = 0
!     starting address  fft values
      ifft = Nlat + 2*Nlat*late + 3*(l*(l-1)/2+(Nlat-l)*(l-1)) + 1
!     set pointers for internal storage of g and legendre polys
      ipmn = lat*Nlon*Nt + 1
      CALL DSHSGC1(Nlat,Nlon,l,lat,Mode,G,Idg,Jdg,Nt,A,B,Mdab,Ndab,      &
                & Wshsgc,Wshsgc(ifft),late,Work(ipmn),Work)
    END SUBROUTINE DSHSGC

    
    SUBROUTINE DSHSGC1(Nlat,Nlon,L,Lat,Mode,Gs,Idg,Jdg,Nt,A,B,Mdab,    &
                      & Ndab,W,Wfft,Late,Pmn,G)
      IMPLICIT NONE
      DOUBLE PRECISION A , B , G , Gs , Pmn , t1 , t2 , t3 , t4 , W , Wfft
      INTEGER i , Idg , is , j , Jdg , k , km , L , Lat , Late , lm1 ,  &
            & lp1 , m , Mdab , meo , Mode , mp1 , mp2 , ms , Ndab
      INTEGER nl2 , Nlat , Nlon , np1 , ns , Nt
      DIMENSION Gs(Idg,Jdg,Nt) , A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt)
      DIMENSION W(1) , Pmn(Nlat,Late,3) , G(Lat,Nlon,Nt) , Wfft(1)
!     reconstruct fourier coefficients in g on gaussian grid
!     using coefficients in a,b
!     set m+1 limit for b coefficient calculation
      lm1 = L
      IF ( Nlon==L+L-2 ) lm1 = L - 1
!     initialize to zero
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Lat
               G(i,j,k) = 0.0
            ENDDO
         ENDDO
      ENDDO
      IF ( Mode==0 ) THEN
!     set first column in g
         m = 0
!     compute pmn for all i and n=m,...,l-1
         CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
         DO k = 1 , Nt
!     n even
            DO np1 = 1 , Nlat , 2
               DO i = 1 , Late
                  G(i,1,k) = G(i,1,k) + A(1,np1,k)*Pmn(np1,i,km)
               ENDDO
            ENDDO
!     n odd
            nl2 = Nlat/2
            DO np1 = 2 , Nlat , 2
               DO i = 1 , nl2
                  is = Nlat - i + 1
                  G(is,1,k) = G(is,1,k) + A(1,np1,k)*Pmn(np1,i,km)
               ENDDO
            ENDDO
!     restore m=0 coefficents (reverse implicit even/odd reduction)
            DO i = 1 , nl2
               is = Nlat - i + 1
               t1 = G(i,1,k)
               t3 = G(is,1,k)
               G(i,1,k) = t1 + t3
               G(is,1,k) = t1 - t3
            ENDDO
         ENDDO
!     sweep  columns of g for which b is available
         DO mp1 = 2 , lm1
            m = mp1 - 1
            mp2 = m + 2
!     compute pmn for all i and n=m,...,l-1
            CALL LEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
!     for n-m even store (g(i,p,k)+g(nlat-i+1,p,k))/2 in g(i,p,k) p=2*m,
!     for i=1,...,late
               DO np1 = mp1 , Nlat , 2
                  DO i = 1 , Late
                     G(i,2*m,k) = G(i,2*m,k) + A(mp1,np1,k)             &
                                & *Pmn(np1,i,km)
                     G(i,2*m+1,k) = G(i,2*m+1,k) + B(mp1,np1,k)         &
                                  & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
!     for n-m odd store g(i,p,k)-g(nlat-i+1,p,k) in g(nlat-i+1,p,k)
!     for i=1,...,nlat/2 (p=2*m,p=2*m+1)
               DO np1 = mp2 , Nlat , 2
                  DO i = 1 , nl2
                     is = Nlat - i + 1
                     G(is,2*m,k) = G(is,2*m,k) + A(mp1,np1,k)           &
                                 & *Pmn(np1,i,km)
                     G(is,2*m+1,k) = G(is,2*m+1,k) + B(mp1,np1,k)       &
                                   & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
!     now set fourier coefficients using even-odd reduction above
               DO i = 1 , nl2
                  is = Nlat - i + 1
                  t1 = G(i,2*m,k)
                  t2 = G(i,2*m+1,k)
                  t3 = G(is,2*m,k)
                  t4 = G(is,2*m+1,k)
                  G(i,2*m,k) = t1 + t3
                  G(i,2*m+1,k) = t2 + t4
                  G(is,2*m,k) = t1 - t3
                  G(is,2*m+1,k) = t2 - t4
               ENDDO
            ENDDO
         ENDDO
!     set last column (using a only)
         IF ( Nlon==L+L-2 ) THEN
            m = L - 1
            CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
!     n-m even
               DO np1 = L , Nlat , 2
                  DO i = 1 , Late
                     G(i,Nlon,k) = G(i,Nlon,k) + 2.0*A(L,np1,k)         &
                                 & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
               lp1 = L + 1
!     n-m odd
               DO np1 = lp1 , Nlat , 2
                  DO i = 1 , nl2
                     is = Nlat - i + 1
                     G(is,Nlon,k) = G(is,Nlon,k) + 2.0*A(L,np1,k)       &
                                  & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
               DO i = 1 , nl2
                  is = Nlat - i + 1
                  t1 = G(i,Nlon,k)
                  t3 = G(is,Nlon,k)
                  G(i,Nlon,k) = t1 + t3
                  G(is,Nlon,k) = t1 - t3
               ENDDO
            ENDDO
         ENDIF
      ELSE
!     half sphere (mode.ne.0)
!     set first column in g
         m = 0
         meo = 1
         IF ( Mode==1 ) meo = 2
         ms = m + meo
!     compute pmn for all i and n=m,...,l-1
         CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
         DO k = 1 , Nt
            DO np1 = ms , Nlat , 2
               DO i = 1 , Late
                  G(i,1,k) = G(i,1,k) + A(1,np1,k)*Pmn(np1,i,km)
               ENDDO
            ENDDO
         ENDDO
!     sweep interior columns of g
         DO mp1 = 2 , lm1
            m = mp1 - 1
            ms = m + meo
!     compute pmn for all i and n=m,...,l-1
            CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
            DO k = 1 , Nt
               DO np1 = ms , Nlat , 2
                  DO i = 1 , Late
                     G(i,2*m,k) = G(i,2*m,k) + A(mp1,np1,k)             &
                                & *Pmn(np1,i,km)
                     G(i,2*m+1,k) = G(i,2*m+1,k) + B(mp1,np1,k)         &
                                  & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
         IF ( Nlon==L+L-2 ) THEN
!     set last column
            m = L - 1
            CALL DLEGIN(Mode,L,Nlat,m,W,Pmn,km)
            ns = L
            IF ( Mode==1 ) ns = L + 1
            DO k = 1 , Nt
               DO i = 1 , Late
                  DO np1 = ns , Nlat , 2
                     G(i,Nlon,k) = G(i,Nlon,k) + 2.0*A(L,np1,k)         &
                                 & *Pmn(np1,i,km)
                  ENDDO
               ENDDO
            ENDDO
         ENDIF
      ENDIF
!     do inverse fourier transform
      DO k = 1 , Nt
         CALL DHRFFTB(Lat,Nlon,G(1,1,k),Lat,Wfft,Pmn)
      ENDDO
!     scale output in gs
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Lat
               Gs(i,j,k) = 0.5*G(i,j,k)
            ENDDO
         ENDDO
      ENDDO
    END SUBROUTINE DSHSGC1

    
    SUBROUTINE DSHSGCI(Nlat,Nlon,Wshsgc,Lshsgc,Dwork,Ldwork,Ierror)
      IMPLICIT NONE
      INTEGER i1 , i2 , i3 , i4 , i5 , i6 , i7 , idth , idwts , Ierror ,&
            & iw , l , l1 , l2 , late , Ldwork , Lshsgc , Nlat , Nlon
      DOUBLE PRECISION Wshsgc
!     this subroutine must be called before calling shsgc with
!     fixed nlat,nlon. it precomputes quantites such as the gaussian
!     points and weights, m=0,m=1 legendre polynomials, recursion
!     recursion coefficients.
      DIMENSION Wshsgc(Lshsgc)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
!     set triangular truncation limit for spherical harmonic basis
      l = MIN0((Nlon+2)/2,Nlat)
!     set equator or nearest point (if excluded) pointer
      late = (Nlat+MOD(Nlat,2))/2
      l1 = l
      l2 = late
      Ierror = 3
!     check permanent work space length
      IF ( Lshsgc<Nlat*(2*l2+3*l1-2)+3*l1*(1-l1)/2+Nlon+15 ) RETURN
      Ierror = 4
      IF ( Ldwork<Nlat*(Nlat+4) ) RETURN
      Ierror = 0
!     set pointers
      i1 = 1
      i2 = i1 + Nlat
      i3 = i2 + Nlat*late
      i4 = i3 + Nlat*late
      i5 = i4 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i6 = i5 + l*(l-1)/2 + (Nlat-l)*(l-1)
      i7 = i6 + l*(l-1)/2 + (Nlat-l)*(l-1)
!     set indices in temp work for double precision gaussian wts and pts
      idth = 1
      idwts = idth + Nlat
      iw = idwts + Nlat
      CALL DSHSGCI1(Nlat,Nlon,l,late,Wshsgc(i1),Wshsgc(i2),Wshsgc(i3),   &
                 & Wshsgc(i4),Wshsgc(i5),Wshsgc(i6),Wshsgc(i7),         &
                 & Dwork(idth),Dwork(idwts),Dwork(iw),Ierror)
      IF ( Ierror/=0 ) Ierror = 5
      END SUBROUTINE DSHSGCI

      
      SUBROUTINE DSHSGCI1(Nlat,Nlon,L,Late,Wts,P0n,P1n,Abel,Bbel,Cbel,   &
                       & Wfft,Dtheta,Dwts,Work,Ier)
      IMPLICIT NONE
      DOUBLE PRECISION Abel , Bbel , Cbel , P0n , P1n , Wfft , Wts
      INTEGER i , Ier , imn , IMNDX , INDX , L , Late , lw , m , mlim , &
            & n , Nlat , Nlon , np1
      DIMENSION Wts(Nlat) , P0n(Nlat,Late) , P1n(Nlat,Late) , Abel(1) , &
              & Bbel(1) , Cbel(1) , Wfft(1) , Dtheta(Nlat) , Dwts(Nlat)
      DOUBLE PRECISION pb , Dtheta , Dwts , Work(*)
!     compute the nlat  gaussian points and weights, the
!     m=0,1 legendre polys for gaussian points and all n,
!     and the legendre recursion coefficients
!     define index function used in storing
!     arrays for recursion coefficients (functions of (m,n))
!     the index function indx(m,n) is defined so that
!     the pairs (m,n) map to [1,2,...,indx(l-1,l-1)] with no
!     "holes" as m varies from 2 to n and n varies from 2 to l-1.
!     (m=0,1 are set from p0n,p1n for all n)
!     define for 2.le.n.le.l-1
      INDX(m,n) = (n-1)*(n-2)/2 + m - 1
!     define index function for l.le.n.le.nlat
      IMNDX(m,n) = L*(L-1)/2 + (n-L-1)*(L-1) + m - 1
!     preset quantites for fourier transform
      CALL DHRFFTI(Nlon,Wfft)
!     compute double precision gaussian points and weights
!     lw = 4*nlat*(nlat+1)+2
      lw = Nlat*(Nlat+2)
      CALL DGAQD(Nlat,Dtheta,Dwts,Work,lw,Ier)
      IF ( Ier/=0 ) RETURN
!     store gaussian weights single precision to save computation
!     in inner loops in analysis
      DO i = 1 , Nlat
         Wts(i) = Dwts(i)
      ENDDO
!     initialize p0n,p1n using double precision dnlfk,dnlft
      DO np1 = 1 , Nlat
         DO i = 1 , Late
            P0n(np1,i) = 0.0
            P1n(np1,i) = 0.0
         ENDDO
      ENDDO
!     compute m=n=0 legendre polynomials for all theta(i)
      np1 = 1
      n = 0
      m = 0
      CALL DDNLFK(m,n,Work)
      DO i = 1 , Late
         CALL DDNLFT(m,n,Dtheta(i),Work,pb)
         P0n(1,i) = pb
      ENDDO
!     compute p0n,p1n for all theta(i) when n.gt.0
      DO np1 = 2 , Nlat
         n = np1 - 1
         m = 0
         CALL DDNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DDNLFT(m,n,Dtheta(i),Work,pb)
            P0n(np1,i) = pb
         ENDDO
!     compute m=1 legendre polynomials for all n and theta(i)
         m = 1
         CALL DDNLFK(m,n,Work)
         DO i = 1 , Late
            CALL DDNLFT(m,n,Dtheta(i),Work,pb)
            P1n(np1,i) = pb
         ENDDO
      ENDDO
!     compute and store swarztrauber recursion coefficients
!     for 2.le.m.le.n and 2.le.n.le.nlat in abel,bbel,cbel
      DO n = 2 , Nlat
         mlim = MIN0(n,L)
         DO m = 2 , mlim
            imn = INDX(m,n)
            IF ( n>=L ) imn = IMNDX(m,n)
            Abel(imn) = SQRT(FLOAT((2*n+1)*(m+n-2)*(m+n-3))/FLOAT(((2*n-&
                      & 3)*(m+n-1)*(m+n))))
            Bbel(imn) = SQRT(FLOAT((2*n+1)*(n-m-1)*(n-m))/FLOAT(((2*n-3)&
                      & *(m+n-1)*(m+n))))
            Cbel(imn) = SQRT(FLOAT((n-m+1)*(n-m+2))/FLOAT(((n+m-1)*(n+m)&
                      & )))
         ENDDO
      ENDDO
      END SUBROUTINE DSHSGCI1
