!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
!
! ... file slapes.f
!
!     this file includes documentation and code for
!     subroutine slapes          i
!
! ... files which must be loaded with slapec.f
!
!     sphcom.f, hrfft.f, shaes.f, shses.f
!
!
!
!     subroutine slapes(nlat,nlon,isym,nt,slap,ids,jds,a,b,mdab,ndab,
!    +                  wshses,lshses,work,lwork,ierror)
!
!
!     given the scalar spherical harmonic coefficients a and b, precomputed
!     by subroutine shaes for a scalar field sf, subroutine slapes computes
!     the laplacian of sf in the scalar array slap.  slap(i,j) is the
!     laplacian of sf at the colatitude
!
!         theta(i) = (i-1)*pi/(nlat-1)
!
!     and east longitude
!
!         lambda(j) = (j-1)*2*pi/nlon
!
!     on the sphere.  i.e.
!
!         slap(i,j) =
!
!                  2                2
!         [1/sint*d (sf(i,j)/dlambda + d(sint*d(sf(i,j))/dtheta)/dtheta]/sint
!
!
!     where sint = sin(theta(i)).  the scalar laplacian in slap has the
!     same symmetry or absence of symmetry about the equator as the scalar
!     field sf.  the input parameters isym,nt,mdab,ndab must have the
!     same values used by shaes to compute a and b for sf. the associated
!     legendre functions are stored rather than recomputed as they are
!     in subroutine slapec.
 
!
!     input parameters
!
!     nlat   the number of colatitudes on the full sphere including the
!            poles. for example, nlat = 37 for a five degree grid.
!            nlat determines the grid increment in colatitude as
!            pi/(nlat-1).  if nlat is odd the equator is located at
!            grid point i=(nlat+1)/2. if nlat is even the equator is
!            located half way between points i=nlat/2 and i=nlat/2+1.
!            nlat must be at least 3. note: on the half sphere, the
!            number of grid points in the colatitudinal direction is
!            nlat/2 if nlat is even or (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct longitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!     isym   this parameter should have the same value input to subroutine
!            shaes to compute the coefficients a and b for the scalar field
!            sf.  isym is set as follows:
!
!            = 0  no symmetries exist in sf about the equator. scalar
!                 synthesis is used to compute slap on the entire sphere.
!                 i.e., in the array slap(i,j) for i=1,...,nlat and
!                 j=1,...,nlon.
!
!           = 1  sf and slap are antisymmetric about the equator. the
!                synthesis used to compute slap is performed on the
!                northern hemisphere only.  if nlat is odd, slap(i,j) is
!                computed for i=1,...,(nlat+1)/2 and j=1,...,nlon.  if
!                nlat is even, slap(i,j) is computed for i=1,...,nlat/2
!                and j=1,...,nlon.
!
!
!           = 2  sf and slap are symmetric about the equator. the
!                synthesis used to compute slap is performed on the
!                northern hemisphere only.  if nlat is odd, slap(i,j) is
!                computed for i=1,...,(nlat+1)/2 and j=1,...,nlon.  if
!                nlat is even, slap(i,j) is computed for i=1,...,nlat/2
!                and j=1,...,nlon.
!
!
!     nt     the number of analyses.  in the program that calls slapes
!            the arrays slap,a, and b can be three dimensional in which
!            case multiple synthesis will be performed.  the third index
!            is the synthesis index which assumes the values k=1,...,nt.
!            for a single analysis set nt=1. the description of the
!            remaining parameters is simplified by assuming that nt=1
!            or that all the arrays are two dimensional.
!
!   ids      the first dimension of the array slap as it appears in the
!            program that calls slapes.  if isym = 0 then ids must be at
!            least nlat.  if isym > 0 and nlat is even then ids must be
!            at least nlat/2. if isym > 0 and nlat is odd then ids must
!            be at least (nlat+1)/2.
!
!   jds      the second dimension of the array slap as it appears in the
!            program that calls slapes. jds must be at least nlon.
!
!
!   a,b      two or three dimensional arrays (see input parameter nt)
!            that contain scalar spherical harmonic coefficients
!            of the scalar field sf as computed by subroutine shaes.
!     ***    a,b must be computed by shaes prior to calling slapes.
!
!
!    mdab    the first dimension of the arrays a and b as it appears
!            in the program that calls slapes.  mdab must be at
!            least min0(nlat,(nlon+2)/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!    ndab    the second dimension of the arrays a and b as it appears
!            in the program that calls slapes. ndbc must be at least
!            least nlat.
!
!            mdab,ndab should have the same values input to shaes to
!            compute the coefficients a and b.
!
!
!    wshses  an array which must be initialized by subroutine shsesi
!            before calling slapes.  once initialized, wshses
!            can be used repeatedly by slapes as long as nlat and nlon
!            remain unchanged.  wshses must not be altered between calls
!            of slapes.
!
!    lshses  the dimension of the array wshses as it appears in the
!            program that calls slapes.  let
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshses must be greater than or equal to
!
!               (l1*l2*(nlat+nlat-l1+1))/2+nlon+15.
!
!
!     work   a work array that does not have to be saved.
!
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls slapes. define
!
!               l2 = nlat/2                    if nlat is even or
!               l2 = (nlat+1)/2                if nlat is odd
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            if isym is zero then lwork must be at least
!
!               (nt+1)*nlat*nlon + nlat*(2*nt*l1+1)
!
!            if isym is nonzero lwork must be at least
!
!               (nt+1)*l2*nlon + nlat*(2*nt*l1+1)
!
!     **************************************************************
!
!     output parameters
!
!
!    slap    a two or three dimensional arrays (see input parameter nt) that
!            contain the scalar laplacian of the scalar field sf.  slap(i,j)
!            is the scalar laplacian at the colatitude
!
!                 theta(i) = (i-1)*pi/(nlat-1)
!
!            and longitude
!
!                 lambda(j) = (j-1)*2*pi/nlon
!
!            for i=1,...,nlat and j=1,...,nlon.
!
!
!  ierror    a parameter which flags errors in input parameters as follows:
!
!            = 0  no errors detected
!
!            = 1  error in the specification of nlat
!
!            = 2  error in the specification of nlon
!
!            = 3  error in the specification of ityp
!
!            = 4  error in the specification of nt
!
!            = 5  error in the specification of ids
!
!            = 6  error in the specification of jds
!
!            = 7  error in the specification of mdbc
!
!            = 8  error in the specification of ndbc
!
!            = 9  error in the specification of lshses
!
!            = 10 error in the specification of lwork
!
!
! **********************************************************************
!
!     end of documentation for slapes
!
! **********************************************************************
!
      SUBROUTINE SLAPES(Nlat,Nlon,Isym,Nt,Slap,Ids,Jds,A,B,Mdab,Ndab,   &
                      & Wshses,Lshses,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL A , B , Slap , Work , Wshses
      INTEGER ia , ib , Ids , Ierror , ifn , imid , Isym , iwk , Jds ,  &
            & l1 , l2 , lpimn , ls , Lshses , lwk , lwkmin , Lwork ,    &
            & Mdab , mmax , mn
      INTEGER Ndab , Nlat , nln , Nlon , Nt
      DIMENSION Slap(Ids,Jds,Nt) , A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt)
      DIMENSION Wshses(Lshses) , Work(Lwork)
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Isym==0 .AND. Ids<Nlat) .OR. (Isym>0 .AND. Ids<imid) )      &
         & RETURN
      Ierror = 6
      IF ( Jds<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,Nlon/2+1)
      IF ( Mdab<mmax ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      imid = (Nlat+1)/2
      lpimn = (imid*mmax*(Nlat+Nlat-mmax+1))/2
      IF ( Lshses<lpimn+Nlon+15 ) RETURN
      Ierror = 10
!
!     set and verify unsaved work space length
!
      ls = Nlat
      IF ( Isym>0 ) ls = imid
      nln = Nt*ls*Nlon
      mn = mmax*Nlat*Nt
!     lwkmin = nln+ls*nlon+2*mn+nlat
!     if (lwork .lt. lwkmin) return
      l2 = (Nlat+1)/2
      l1 = MIN0(Nlat,Nlon/2+1)
      IF ( Isym==0 ) THEN
         lwkmin = (Nt+1)*Nlat*Nlon + Nlat*(2*Nt*l1+1)
      ELSE
         lwkmin = (Nt+1)*l2*Nlon + Nlat*(2*Nt*l1+1)
      ENDIF
      IF ( Lwork<lwkmin ) RETURN
      Ierror = 0
!
!     set work space pointers
!
      ia = 1
      ib = ia + mn
      ifn = ib + mn
      iwk = ifn + Nlat
      lwk = Lwork - 2*mn - Nlat
      CALL SLAPES1(Nlat,Nlon,Isym,Nt,Slap,Ids,Jds,A,B,Mdab,Ndab,Work(ia)&
                 & ,Work(ib),mmax,Work(ifn),Wshses,Lshses,Work(iwk),lwk,&
                 & Ierror)
      END SUBROUTINE SLAPES

      
      SUBROUTINE SLAPES1(Nlat,Nlon,Isym,Nt,Slap,Ids,Jds,A,B,Mdab,Ndab,  &
                       & Alap,Blap,Mmax,Fnn,Wsave,Lsave,Wk,Lwk,Ierror)
      IMPLICIT NONE
      REAL A , Alap , B , Blap , fn , Fnn , Slap , Wk , Wsave
      INTEGER Ids , Ierror , Isym , Jds , k , Lsave , Lwk , m , Mdab ,  &
            & Mmax , n , Ndab , Nlat , Nlon , Nt
      DIMENSION Slap(Ids,Jds,Nt) , A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt)
      DIMENSION Alap(Mmax,Nlat,Nt) , Blap(Mmax,Nlat,Nt) , Fnn(Nlat)
      DIMENSION Wsave(Lsave) , Wk(Lwk)
!
!     set coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Fnn(n) = fn*(fn+1.)
      ENDDO
!
!     compute scalar laplacian coefficients for each vector field
!
      DO k = 1 , Nt
         DO n = 1 , Nlat
            DO m = 1 , Mmax
               Alap(m,n,k) = 0.0
               Blap(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            Alap(1,n,k) = -Fnn(n)*A(1,n,k)
            Blap(1,n,k) = -Fnn(n)*B(1,n,k)
         ENDDO
!
!     compute m>0 coefficients
!
         DO m = 2 , Mmax
            DO n = m , Nlat
               Alap(m,n,k) = -Fnn(n)*A(m,n,k)
               Blap(m,n,k) = -Fnn(n)*B(m,n,k)
            ENDDO
         ENDDO
      ENDDO
!
!     synthesize alap,blap into slap
!
      CALL SHSES(Nlat,Nlon,Isym,Nt,Slap,Ids,Jds,Alap,Blap,Mmax,Nlat,    &
               & Wsave,Lsave,Wk,Lwk,Ierror)
      END SUBROUTINE SLAPES1


      SUBROUTINE DSLAPES(Nlat,Nlon,Isym,Nt,Slap,Ids,Jds,A,B,Mdab,Ndab,   &
                      & Wshses,Lshses,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION A , B , Slap , Work , Wshses
      INTEGER ia , ib , Ids , Ierror , ifn , imid , Isym , iwk , Jds ,  &
            & l1 , l2 , lpimn , ls , Lshses , lwk , lwkmin , Lwork ,    &
            & Mdab , mmax , mn
      INTEGER Ndab , Nlat , nln , Nlon , Nt
      DIMENSION Slap(Ids,Jds,Nt) , A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt)
      DIMENSION Wshses(Lshses) , Work(Lwork)
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Isym==0 .AND. Ids<Nlat) .OR. (Isym>0 .AND. Ids<imid) )      &
         & RETURN
      Ierror = 6
      IF ( Jds<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,Nlon/2+1)
      IF ( Mdab<mmax ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      imid = (Nlat+1)/2
      lpimn = (imid*mmax*(Nlat+Nlat-mmax+1))/2
      IF ( Lshses<lpimn+Nlon+15 ) RETURN
      Ierror = 10
!
!     set and verify unsaved work space length
!
      ls = Nlat
      IF ( Isym>0 ) ls = imid
      nln = Nt*ls*Nlon
      mn = mmax*Nlat*Nt
!     lwkmin = nln+ls*nlon+2*mn+nlat
!     if (lwork .lt. lwkmin) return
      l2 = (Nlat+1)/2
      l1 = MIN0(Nlat,Nlon/2+1)
      IF ( Isym==0 ) THEN
         lwkmin = (Nt+1)*Nlat*Nlon + Nlat*(2*Nt*l1+1)
      ELSE
         lwkmin = (Nt+1)*l2*Nlon + Nlat*(2*Nt*l1+1)
      ENDIF
      IF ( Lwork<lwkmin ) RETURN
      Ierror = 0
!
!     set work space pointers
!
      ia = 1
      ib = ia + mn
      ifn = ib + mn
      iwk = ifn + Nlat
      lwk = Lwork - 2*mn - Nlat
      CALL DSLAPES1(Nlat,Nlon,Isym,Nt,Slap,Ids,Jds,A,B,Mdab,Ndab,Work(ia)&
                 & ,Work(ib),mmax,Work(ifn),Wshses,Lshses,Work(iwk),lwk,&
                 & Ierror)
      END SUBROUTINE DSLAPES

      
      SUBROUTINE DSLAPES1(Nlat,Nlon,Isym,Nt,Slap,Ids,Jds,A,B,Mdab,Ndab,  &
                       & Alap,Blap,Mmax,Fnn,Wsave,Lsave,Wk,Lwk,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION A , Alap , B , Blap , fn , Fnn , Slap , Wk , Wsave
      INTEGER Ids , Ierror , Isym , Jds , k , Lsave , Lwk , m , Mdab ,  &
            & Mmax , n , Ndab , Nlat , Nlon , Nt
      DIMENSION Slap(Ids,Jds,Nt) , A(Mdab,Ndab,Nt) , B(Mdab,Ndab,Nt)
      DIMENSION Alap(Mmax,Nlat,Nt) , Blap(Mmax,Nlat,Nt) , Fnn(Nlat)
      DIMENSION Wsave(Lsave) , Wk(Lwk)
!
!     set coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Fnn(n) = fn*(fn+1.)
      ENDDO
!
!     compute scalar laplacian coefficients for each vector field
!
      DO k = 1 , Nt
         DO n = 1 , Nlat
            DO m = 1 , Mmax
               Alap(m,n,k) = 0.0
               Blap(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            Alap(1,n,k) = -Fnn(n)*A(1,n,k)
            Blap(1,n,k) = -Fnn(n)*B(1,n,k)
         ENDDO
!
!     compute m>0 coefficients
!
         DO m = 2 , Mmax
            DO n = m , Nlat
               Alap(m,n,k) = -Fnn(n)*A(m,n,k)
               Blap(m,n,k) = -Fnn(n)*B(m,n,k)
            ENDDO
         ENDDO
      ENDDO
!
!     synthesize alap,blap into slap
!
      CALL DSHSES(Nlat,Nlon,Isym,Nt,Slap,Ids,Jds,Alap,Blap,Mmax,Nlat,    &
               & Wsave,Lsave,Wk,Lwk,Ierror)
      END SUBROUTINE DSLAPES1
