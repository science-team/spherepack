!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         spherepack3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
! ... file vhaes.f
!
!     this file contains code and documentation for subroutines
!     vhaes and vhaesi
!
! ... files which must be loaded with vhaes.f
!
!     sphcom.f, hrfft.f
!
!
!     subroutine vhaes(nlat,nlon,ityp,nt,v,w,idvw,jdvw,br,bi,cr,ci,
!    +                 mdab,ndab,wvhaes,lvhaes,work,lwork,ierror)
!
!     subroutine vhaes performs the vector spherical harmonic analysis
!     on the vector field (v,w) and stores the result in the arrays
!     br, bi, cr, and ci. v(i,j) and w(i,j) are the colatitudinal
!     (measured from the north pole) and east longitudinal components
!     respectively, located at colatitude theta(i) = (i-1)*pi/(nlat-1)
!     and longitude phi(j) = (j-1)*2*pi/nlon. the spectral
!     representation of (v,w) is given at output parameters v,w in
!     subroutine vhses.
!
!     input parameters
!
!     nlat   the number of colatitudes on the full sphere including the
!            poles. for example, nlat = 37 for a five degree grid.
!            nlat determines the grid increment in colatitude as
!            pi/(nlat-1).  if nlat is odd the equator is located at
!            grid point i=(nlat+1)/2. if nlat is even the equator is
!            located half way between points i=nlat/2 and i=nlat/2+1.
!            nlat must be at least 3. note: on the half sphere, the
!            number of grid points in the colatitudinal direction is
!            nlat/2 if nlat is even or (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!     ityp   = 0  no symmetries exist about the equator. the analysis
!                 is performed on the entire sphere.  i.e. on the
!                 arrays v(i,j),w(i,j) for i=1,...,nlat and
!                 j=1,...,nlon.
!
!            = 1  no symmetries exist about the equator. the analysis
!                 is performed on the entire sphere.  i.e. on the
!                 arrays v(i,j),w(i,j) for i=1,...,nlat and
!                 j=1,...,nlon. the curl of (v,w) is zero. that is,
!                 (d/dtheta (sin(theta) w) - dv/dphi)/sin(theta) = 0.
!                 the coefficients cr and ci are zero.
!
!            = 2  no symmetries exist about the equator. the analysis
!                 is performed on the entire sphere.  i.e. on the
!                 arrays v(i,j),w(i,j) for i=1,...,nlat and
!                 j=1,...,nlon. the divergence of (v,w) is zero. i.e.,
!                 (d/dtheta (sin(theta) v) + dw/dphi)/sin(theta) = 0.
!                 the coefficients br and bi are zero.
!
!            = 3  v is symmetric and w is antisymmetric about the
!                 equator. the analysis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the analysis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the analysis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 4  v is symmetric and w is antisymmetric about the
!                 equator. the analysis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the analysis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the analysis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the curl of (v,w) is zero. that is,
!                 (d/dtheta (sin(theta) w) - dv/dphi)/sin(theta) = 0.
!                 the coefficients cr and ci are zero.
!
!            = 5  v is symmetric and w is antisymmetric about the
!                 equator. the analysis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the analysis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the analysis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the divergence of (v,w) is zero. i.e.,
!                 (d/dtheta (sin(theta) v) + dw/dphi)/sin(theta) = 0.
!                 the coefficients br and bi are zero.
!
!            = 6  v is antisymmetric and w is symmetric about the
!                 equator. the analysis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the analysis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the analysis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 7  v is antisymmetric and w is symmetric about the
!                 equator. the analysis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the analysis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the analysis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the curl of (v,w) is zero. that is,
!                 (d/dtheta (sin(theta) w) - dv/dphi)/sin(theta) = 0.
!                 the coefficients cr and ci are zero.
!
!            = 8  v is antisymmetric and w is symmetric about the
!                 equator. the analysis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the analysis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the analysis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the divergence of (v,w) is zero. i.e.,
!                 (d/dtheta (sin(theta) v) + dw/dphi)/sin(theta) = 0.
!                 the coefficients br and bi are zero.
!
!
!     nt     the number of analyses.  in the program that calls vhaes,
!            the arrays v,w,br,bi,cr, and ci can be three dimensional
!            in which case multiple analyses will be performed.
!            the third index is the analysis index which assumes the
!            values k=1,...,nt.  for a single analysis set nt=1. the
!            discription of the remaining parameters is simplified
!            by assuming that nt=1 or that all the arrays are two
!            dimensional.
!
!     v,w    two or three dimensional arrays (see input parameter nt)
!            that contain the vector function to be analyzed.
!            v is the colatitudnal component and w is the east
!            longitudinal component. v(i,j),w(i,j) contain the
!            components at colatitude theta(i) = (i-1)*pi/(nlat-1)
!            and longitude phi(j) = (j-1)*2*pi/nlon. the index ranges
!            are defined above at the input parameter ityp.
!
!     idvw   the first dimension of the arrays v,w as it appears in
!            the program that calls vhaes. if ityp .le. 2 then idvw
!            must be at least nlat.  if ityp .gt. 2 and nlat is
!            even then idvw must be at least nlat/2. if ityp .gt. 2
!            and nlat is odd then idvw must be at least (nlat+1)/2.
!
!     jdvw   the second dimension of the arrays v,w as it appears in
!            the program that calls vhaes. jdvw must be at least nlon.
!
!     mdab   the first dimension of the arrays br,bi,cr, and ci as it
!            appears in the program that calls vhaes. mdab must be at
!            least min0(nlat,nlon/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!     ndab   the second dimension of the arrays br,bi,cr, and ci as it
!            appears in the program that calls vhaes. ndab must be at
!            least nlat.
!
!     lvhaes an array which must be initialized by subroutine vhaesi.
!            once initialized, wvhaes can be used repeatedly by vhaes
!            as long as nlon and nlat remain unchanged.  wvhaes must
!            not be altered between calls of vhaes.
!
!     lvhaes the dimension of the array wvhaes as it appears in the
!            program that calls vhaes. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lvhaes must be at least
!
!            l1*l2(nlat+nlat-l1+1)+nlon+15
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls vhaes. define
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            if ityp .le. 2 then lwork must be at least
!
!                       (2*nt+1)*nlat*nlon
!
!            if ityp .gt. 2 then lwork must be at least
!
!                        (2*nt+1)*l2*nlon
!
!     **************************************************************
!
!     output parameters
!
!     br,bi  two or three dimensional arrays (see input parameter nt)
!     cr,ci  that contain the vector spherical harmonic coefficients
!            in the spectral representation of v(i,j) and w(i,j) given
!            in the discription of subroutine vhses. br(mp1,np1),
!            bi(mp1,np1),cr(mp1,np1), and ci(mp1,np1) are computed
!            for mp1=1,...,mmax and np1=mp1,...,nlat except for np1=nlat
!            and odd mp1. mmax=min0(nlat,nlon/2) if nlon is even or
!            mmax=min0(nlat,(nlon+1)/2) if nlon is odd.
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of ityp
!            = 4  error in the specification of nt
!            = 5  error in the specification of idvw
!            = 6  error in the specification of jdvw
!            = 7  error in the specification of mdab
!            = 8  error in the specification of ndab
!            = 9  error in the specification of lvhaes
!            = 10 error in the specification of lwork
!
! ********************************************************
!
!     subroutine vhaesi(nlat,nlon,wvhaes,lvhaes,work,lwork,dwork,
!    +                  ldwork,ierror)
!
!     subroutine vhaesi initializes the array wvhaes which can then be
!     used repeatedly by subroutine vhaes until nlat or nlon is changed.
!
!     input parameters
!
!     nlat   the number of colatitudes on the full sphere including the
!            poles. for example, nlat = 37 for a five degree grid.
!            nlat determines the grid increment in colatitude as
!            pi/(nlat-1).  if nlat is odd the equator is located at
!            grid point i=(nlat+1)/2. if nlat is even the equator is
!            located half way between points i=nlat/2 and i=nlat/2+1.
!            nlat must be at least 3. note: on the half sphere, the
!            number of grid points in the colatitudinal direction is
!            nlat/2 if nlat is even or (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!     lvhaes the dimension of the array wvhaes as it appears in the
!            program that calls vhaes. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lvhaes must be at least
!
!               l1*l2*(nlat+nlat-l1+1)+nlon+15
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls vhaes. lwork must be at least
!
!              3*(max0(l1-2,0)*(nlat+nlat-l1-1))/2+5*l2*nlat
!
!     dwork  an unsaved double precision work space
!
!     ldwork the length of the array dwork as it appears in the
!            program that calls vhaesi.  ldwork must be at least
!            2*(nlat+1)
!
!
!     **************************************************************
!
!     output parameters
!
!     wvhaes an array which is initialized for use by subroutine vhaes.
!            once initialized, wvhaes can be used repeatedly by vhaes
!            as long as nlat or nlon remain unchanged.  wvhaes must not
!            be altered between calls of vhaes.
!
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of lvhaes
!            = 4  error in the specification of lwork
!            = 5  error in the specification of ldwork
!
!
      SUBROUTINE VHAES(Nlat,Nlon,Ityp,Nt,V,W,Idvw,Jdvw,Br,Bi,Cr,Ci,Mdab,&
                     & Ndab,Wvhaes,Lvhaes,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL Bi , Br , Ci , Cr , V , W , Work , Wvhaes
      INTEGER idv , Idvw , idz , Ierror , imid , ist , Ityp , iw1 ,     &
            & iw2 , iw3 , iw4 , Jdvw , jw1 , jw2 , lnl , Lvhaes ,       &
            & Lwork , lzimn , Mdab , mmax
      INTEGER Ndab , Nlat , Nlon , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Work(1) , Wvhaes(1)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      IF ( Ityp<0 .OR. Ityp>8 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Ityp<=2 .AND. Idvw<Nlat) .OR. (Ityp>2 .AND. Idvw<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdab<mmax ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
      IF ( Lvhaes<lzimn+lzimn+Nlon+15 ) RETURN
      Ierror = 10
      idv = Nlat
      IF ( Ityp>2 ) idv = imid
      lnl = Nt*idv*Nlon
      IF ( Lwork<lnl+lnl+idv*Nlon ) RETURN
      Ierror = 0
      ist = 0
      IF ( Ityp<=2 ) ist = imid
      iw1 = ist + 1
      iw2 = lnl + 1
      iw3 = iw2 + ist
      iw4 = iw2 + lnl
      jw1 = lzimn + 1
      jw2 = jw1 + lzimn
      CALL VHAES1(Nlat,Nlon,Ityp,Nt,imid,Idvw,Jdvw,V,W,Mdab,Ndab,Br,Bi, &
                & Cr,Ci,idv,Work,Work(iw1),Work(iw2),Work(iw3),Work(iw4)&
                & ,idz,Wvhaes,Wvhaes(jw1),Wvhaes(jw2))
      END SUBROUTINE VHAES
 
      SUBROUTINE VHAES1(Nlat,Nlon,Ityp,Nt,Imid,Idvw,Jdvw,V,W,Mdab,Ndab, &
                      & Br,Bi,Cr,Ci,Idv,Ve,Vo,We,Wo,Work,Idz,Zv,Zw,     &
                      & Wrfft)
      IMPLICIT NONE
      REAL Bi , Br , Ci , Cr , fsn , tsn , V , Ve , Vo , W , We , Wo ,  &
         & Work , Wrfft , Zv , Zw
      INTEGER i , Idv , Idvw , Idz , Imid , imm1 , Ityp , itypp , j ,   &
            & Jdvw , k , m , mb , Mdab , mlat , mlon , mmax , mp1 ,     &
            & mp2 , Ndab
      INTEGER ndo1 , ndo2 , Nlat , Nlon , nlp1 , np1 , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Ve(Idv,Nlon,1) , Vo(Idv,Nlon,1) , We(Idv,Nlon,1) ,      &
              & Wo(Idv,Nlon,1) , Work(1) , Wrfft(1) , Zv(Idz,1) ,       &
              & Zw(Idz,1)
      nlp1 = Nlat + 1
      tsn = 2./Nlon
      fsn = 4./Nlon
      mlat = MOD(Nlat,2)
      mlon = MOD(Nlon,2)
      mmax = MIN0(Nlat,(Nlon+1)/2)
      imm1 = Imid
      IF ( mlat/=0 ) imm1 = Imid - 1
      IF ( Ityp>2 ) THEN
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO j = 1 , Nlon
                  Ve(i,j,k) = fsn*V(i,j,k)
                  Vo(i,j,k) = fsn*V(i,j,k)
                  We(i,j,k) = fsn*W(i,j,k)
                  Wo(i,j,k) = fsn*W(i,j,k)
               ENDDO
            ENDDO
         ENDDO
      ELSE
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO j = 1 , Nlon
                  Ve(i,j,k) = tsn*(V(i,j,k)+V(nlp1-i,j,k))
                  Vo(i,j,k) = tsn*(V(i,j,k)-V(nlp1-i,j,k))
                  We(i,j,k) = tsn*(W(i,j,k)+W(nlp1-i,j,k))
                  Wo(i,j,k) = tsn*(W(i,j,k)-W(nlp1-i,j,k))
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      IF ( mlat/=0 ) THEN
         DO k = 1 , Nt
            DO j = 1 , Nlon
               Ve(Imid,j,k) = tsn*V(Imid,j,k)
               We(Imid,j,k) = tsn*W(Imid,j,k)
            ENDDO
         ENDDO
      ENDIF
      DO k = 1 , Nt
         CALL HRFFTF(Idv,Nlon,Ve(1,1,k),Idv,Wrfft,Work)
         CALL HRFFTF(Idv,Nlon,We(1,1,k),Idv,Wrfft,Work)
      ENDDO
      ndo1 = Nlat
      ndo2 = Nlat
      IF ( mlat/=0 ) ndo1 = Nlat - 1
      IF ( mlat==0 ) ndo2 = Nlat - 1
      IF ( Ityp/=2 .AND. Ityp/=5 .AND. Ityp/=8 ) THEN
         DO k = 1 , Nt
            DO mp1 = 1 , mmax
               DO np1 = mp1 , Nlat
                  Br(mp1,np1,k) = 0.
                  Bi(mp1,np1,k) = 0.
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      IF ( Ityp/=1 .AND. Ityp/=4 .AND. Ityp/=7 ) THEN
         DO k = 1 , Nt
            DO mp1 = 1 , mmax
               DO np1 = mp1 , Nlat
                  Cr(mp1,np1,k) = 0.
                  Ci(mp1,np1,k) = 0.
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      itypp = Ityp + 1
      IF ( itypp==2 ) THEN
!
!     case ityp=1 ,  no symmetries but cr and ci equal zero
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Ve(i,1,k)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Vo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *We(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *We(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==3 ) THEN
!
!     case ityp=2 ,  no symmetries but br and bi equal zero
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*We(i,1,k)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*Wo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==4 ) THEN
!
!     case ityp=3 ,  v even , w odd
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Ve(i,1,k)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*Wo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==5 ) THEN
!
!     case ityp=4 ,  v even, w odd, and cr and ci equal 0.
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Ve(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==6 ) THEN
!
!     case ityp=5   v even, w odd, and br and bi equal zero
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*Wo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==7 ) THEN
!
!     case ityp=6 ,  v odd , w even
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*We(i,1,k)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Vo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *We(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *We(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==8 ) THEN
!
!     case ityp=7   v odd, w even, and cr and ci equal zero
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Vo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *We(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *We(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==9 ) THEN
!
!     case ityp=8   v odd, w even, and both br and bi equal zero
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*We(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         GOTO 99999
      ENDIF
!
!     case ityp=0 ,  no symmetries
!
!     case m=0
!
      DO k = 1 , Nt
         DO i = 1 , Imid
            DO np1 = 2 , ndo2 , 2
               Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Ve(i,1,k)
               Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*We(i,1,k)
            ENDDO
         ENDDO
      ENDDO
      DO k = 1 , Nt
         DO i = 1 , imm1
            DO np1 = 3 , ndo1 , 2
               Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Vo(i,1,k)
               Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*Wo(i,1,k)
            ENDDO
         ENDDO
      ENDDO
!
!     case m = 1 through nlat-1
!
      IF ( mmax<2 ) RETURN
      DO mp1 = 2 , mmax
         m = mp1 - 1
         mb = m*(Nlat-1) - (m*(m-1))/2
         mp2 = mp1 + 1
         IF ( mp1<=ndo1 ) THEN
            DO k = 1 , Nt
               DO i = 1 , imm1
                  DO np1 = mp1 , ndo1 , 2
                     Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)       &
                                   & *Vo(i,2*mp1-2,k) + Zw(np1+mb,i)    &
                                   & *We(i,2*mp1-1,k)
                     Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)       &
                                   & *Vo(i,2*mp1-1,k) - Zw(np1+mb,i)    &
                                   & *We(i,2*mp1-2,k)
                     Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)       &
                                   & *Wo(i,2*mp1-2,k) + Zw(np1+mb,i)    &
                                   & *Ve(i,2*mp1-1,k)
                     Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)       &
                                   & *Wo(i,2*mp1-1,k) - Zw(np1+mb,i)    &
                                   & *Ve(i,2*mp1-2,k)
                  ENDDO
               ENDDO
            ENDDO
            IF ( mlat/=0 ) THEN
               DO k = 1 , Nt
                  DO np1 = mp1 , ndo1 , 2
                     Br(mp1,np1,k) = Br(mp1,np1,k) + Zw(np1+mb,Imid)    &
                                   & *We(Imid,2*mp1-1,k)
                     Bi(mp1,np1,k) = Bi(mp1,np1,k) - Zw(np1+mb,Imid)    &
                                   & *We(Imid,2*mp1-2,k)
                     Cr(mp1,np1,k) = Cr(mp1,np1,k) + Zw(np1+mb,Imid)    &
                                   & *Ve(Imid,2*mp1-1,k)
                     Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zw(np1+mb,Imid)    &
                                   & *Ve(Imid,2*mp1-2,k)
                  ENDDO
               ENDDO
            ENDIF
         ENDIF
         IF ( mp2<=ndo2 ) THEN
            DO k = 1 , Nt
               DO i = 1 , imm1
                  DO np1 = mp2 , ndo2 , 2
                     Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)       &
                                   & *Ve(i,2*mp1-2,k) + Zw(np1+mb,i)    &
                                   & *Wo(i,2*mp1-1,k)
                     Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)       &
                                   & *Ve(i,2*mp1-1,k) - Zw(np1+mb,i)    &
                                   & *Wo(i,2*mp1-2,k)
                     Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)       &
                                   & *We(i,2*mp1-2,k) + Zw(np1+mb,i)    &
                                   & *Vo(i,2*mp1-1,k)
                     Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)       &
                                   & *We(i,2*mp1-1,k) - Zw(np1+mb,i)    &
                                   & *Vo(i,2*mp1-2,k)
                  ENDDO
               ENDDO
            ENDDO
            IF ( mlat/=0 ) THEN
               DO k = 1 , Nt
                  DO np1 = mp2 , ndo2 , 2
                     Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,Imid)    &
                                   & *Ve(Imid,2*mp1-2,k)
                     Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,Imid)    &
                                   & *Ve(Imid,2*mp1-1,k)
                     Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,Imid)    &
                                   & *We(Imid,2*mp1-2,k)
                     Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,Imid)    &
                                   & *We(Imid,2*mp1-1,k)
                  ENDDO
               ENDDO
            ENDIF
         ENDIF
      ENDDO
      RETURN
99999 END SUBROUTINE VHAES1


!
!     dwork must be of length at least 2*(nlat+1)
!
      SUBROUTINE VHAESI(Nlat,Nlon,Wvhaes,Lvhaes,Work,Lwork,Dwork,Ldwork,&
                      & Ierror)
      IMPLICIT NONE
      INTEGER idz , Ierror , imid , iw1 , labc , Ldwork , Lvhaes ,      &
            & Lwork , lzimn , mmax , Nlat , Nlon
      REAL Work , Wvhaes
      DIMENSION Wvhaes(Lvhaes) , Work(Lwork)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      mmax = MIN0(Nlat,(Nlon+1)/2)
      imid = (Nlat+1)/2
      lzimn = (imid*mmax*(Nlat+Nlat-mmax+1))/2
      IF ( Lvhaes<lzimn+lzimn+Nlon+15 ) RETURN
      Ierror = 4
      labc = 3*(MAX0(mmax-2,0)*(Nlat+Nlat-mmax-1))/2
      IF ( Lwork<5*Nlat*imid+labc ) RETURN
      Ierror = 5
      IF ( Ldwork<2*(Nlat+1) ) RETURN
      Ierror = 0
      iw1 = 3*Nlat*imid + 1
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      CALL VEA1(Nlat,Nlon,imid,Wvhaes,Wvhaes(lzimn+1),idz,Work,Work(iw1)&
              & ,Dwork)
      CALL HRFFTI(Nlon,Wvhaes(2*lzimn+1))
      END SUBROUTINE VHAESI


      SUBROUTINE VEA1(Nlat,Nlon,Imid,Zv,Zw,Idz,Zin,Wzvin,Dwork)
      IMPLICIT NONE
      INTEGER i , i3 , Idz , Imid , m , mmax , mn , mp1 , Nlat , Nlon , &
            & np1
      REAL Wzvin , Zin , Zv , Zw
      DIMENSION Zv(Idz,1) , Zw(Idz,1) , Zin(Imid,Nlat,3) , Wzvin(1)
      DOUBLE PRECISION Dwork(*)
      mmax = MIN0(Nlat,(Nlon+1)/2)
      CALL ZVINIT(Nlat,Nlon,Wzvin,Dwork)
      DO mp1 = 1 , mmax
         m = mp1 - 1
         CALL ZVIN(0,Nlat,Nlon,m,Zin,i3,Wzvin)
         DO np1 = mp1 , Nlat
            mn = m*(Nlat-1) - (m*(m-1))/2 + np1
            DO i = 1 , Imid
               Zv(mn,i) = Zin(i,np1,i3)
            ENDDO
         ENDDO
      ENDDO
      CALL ZWINIT(Nlat,Nlon,Wzvin,Dwork)
      DO mp1 = 1 , mmax
         m = mp1 - 1
         CALL ZWIN(0,Nlat,Nlon,m,Zin,i3,Wzvin)
         DO np1 = mp1 , Nlat
            mn = m*(Nlat-1) - (m*(m-1))/2 + np1
            DO i = 1 , Imid
               Zw(mn,i) = Zin(i,np1,i3)
            ENDDO
         ENDDO
      ENDDO
      END SUBROUTINE VEA1


      SUBROUTINE DVHAES(Nlat,Nlon,Ityp,Nt,V,W,Idvw,Jdvw,Br,Bi,Cr,Ci,Mdab,&
                     & Ndab,Wvhaes,Lvhaes,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Br , Ci , Cr , V , W , Work , Wvhaes
      INTEGER idv , Idvw , idz , Ierror , imid , ist , Ityp , iw1 ,     &
            & iw2 , iw3 , iw4 , Jdvw , jw1 , jw2 , lnl , Lvhaes ,       &
            & Lwork , lzimn , Mdab , mmax
      INTEGER Ndab , Nlat , Nlon , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Work(1) , Wvhaes(1)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      IF ( Ityp<0 .OR. Ityp>8 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Ityp<=2 .AND. Idvw<Nlat) .OR. (Ityp>2 .AND. Idvw<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdab<mmax ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
      IF ( Lvhaes<lzimn+lzimn+Nlon+15 ) RETURN
      Ierror = 10
      idv = Nlat
      IF ( Ityp>2 ) idv = imid
      lnl = Nt*idv*Nlon
      IF ( Lwork<lnl+lnl+idv*Nlon ) RETURN
      Ierror = 0
      ist = 0
      IF ( Ityp<=2 ) ist = imid
      iw1 = ist + 1
      iw2 = lnl + 1
      iw3 = iw2 + ist
      iw4 = iw2 + lnl
      jw1 = lzimn + 1
      jw2 = jw1 + lzimn
      CALL DVHAES1(Nlat,Nlon,Ityp,Nt,imid,Idvw,Jdvw,V,W,Mdab,Ndab,Br,Bi, &
                & Cr,Ci,idv,Work,Work(iw1),Work(iw2),Work(iw3),Work(iw4)&
                & ,idz,Wvhaes,Wvhaes(jw1),Wvhaes(jw2))
    END SUBROUTINE DVHAES
 

    SUBROUTINE DVHAES1(Nlat,Nlon,Ityp,Nt,Imid,Idvw,Jdvw,V,W,Mdab,Ndab, &
                      & Br,Bi,Cr,Ci,Idv,Ve,Vo,We,Wo,Work,Idz,Zv,Zw,     &
                      & Wrfft)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Br , Ci , Cr , fsn , tsn , V , Ve , Vo , W , We , Wo ,  &
         & Work , Wrfft , Zv , Zw
      INTEGER i , Idv , Idvw , Idz , Imid , imm1 , Ityp , itypp , j ,   &
            & Jdvw , k , m , mb , Mdab , mlat , mlon , mmax , mp1 ,     &
            & mp2 , Ndab
      INTEGER ndo1 , ndo2 , Nlat , Nlon , nlp1 , np1 , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Ve(Idv,Nlon,1) , Vo(Idv,Nlon,1) , We(Idv,Nlon,1) ,      &
              & Wo(Idv,Nlon,1) , Work(1) , Wrfft(1) , Zv(Idz,1) ,       &
              & Zw(Idz,1)
      nlp1 = Nlat + 1
      tsn = 2./Nlon
      fsn = 4./Nlon
      mlat = MOD(Nlat,2)
      mlon = MOD(Nlon,2)
      mmax = MIN0(Nlat,(Nlon+1)/2)
      imm1 = Imid
      IF ( mlat/=0 ) imm1 = Imid - 1
      IF ( Ityp>2 ) THEN
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO j = 1 , Nlon
                  Ve(i,j,k) = fsn*V(i,j,k)
                  Vo(i,j,k) = fsn*V(i,j,k)
                  We(i,j,k) = fsn*W(i,j,k)
                  Wo(i,j,k) = fsn*W(i,j,k)
               ENDDO
            ENDDO
         ENDDO
      ELSE
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO j = 1 , Nlon
                  Ve(i,j,k) = tsn*(V(i,j,k)+V(nlp1-i,j,k))
                  Vo(i,j,k) = tsn*(V(i,j,k)-V(nlp1-i,j,k))
                  We(i,j,k) = tsn*(W(i,j,k)+W(nlp1-i,j,k))
                  Wo(i,j,k) = tsn*(W(i,j,k)-W(nlp1-i,j,k))
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      IF ( mlat/=0 ) THEN
         DO k = 1 , Nt
            DO j = 1 , Nlon
               Ve(Imid,j,k) = tsn*V(Imid,j,k)
               We(Imid,j,k) = tsn*W(Imid,j,k)
            ENDDO
         ENDDO
      ENDIF
      DO k = 1 , Nt
         CALL DHRFFTF(Idv,Nlon,Ve(1,1,k),Idv,Wrfft,Work)
         CALL DHRFFTF(Idv,Nlon,We(1,1,k),Idv,Wrfft,Work)
      ENDDO
      ndo1 = Nlat
      ndo2 = Nlat
      IF ( mlat/=0 ) ndo1 = Nlat - 1
      IF ( mlat==0 ) ndo2 = Nlat - 1
      IF ( Ityp/=2 .AND. Ityp/=5 .AND. Ityp/=8 ) THEN
         DO k = 1 , Nt
            DO mp1 = 1 , mmax
               DO np1 = mp1 , Nlat
                  Br(mp1,np1,k) = 0.
                  Bi(mp1,np1,k) = 0.
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      IF ( Ityp/=1 .AND. Ityp/=4 .AND. Ityp/=7 ) THEN
         DO k = 1 , Nt
            DO mp1 = 1 , mmax
               DO np1 = mp1 , Nlat
                  Cr(mp1,np1,k) = 0.
                  Ci(mp1,np1,k) = 0.
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      itypp = Ityp + 1
      IF ( itypp==2 ) THEN
!
!     case ityp=1 ,  no symmetries but cr and ci equal zero
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Ve(i,1,k)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Vo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *We(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *We(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==3 ) THEN
!
!     case ityp=2 ,  no symmetries but br and bi equal zero
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*We(i,1,k)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*Wo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==4 ) THEN
!
!     case ityp=3 ,  v even , w odd
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Ve(i,1,k)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*Wo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==5 ) THEN
!
!     case ityp=4 ,  v even, w odd, and cr and ci equal 0.
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Ve(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Ve(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Wo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==6 ) THEN
!
!     case ityp=5   v even, w odd, and br and bi equal zero
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*Wo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *Wo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Ve(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *Ve(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==7 ) THEN
!
!     case ityp=6 ,  v odd , w even
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*We(i,1,k)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Vo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *We(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *We(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==8 ) THEN
!
!     case ityp=7   v odd, w even, and cr and ci equal zero
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , imm1
               DO np1 = 3 , ndo1 , 2
                  Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Vo(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp1<=ndo1 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *We(i,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)    &
                                      & *Vo(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *We(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        Br(mp1,np1,k) = Br(mp1,np1,k) + Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                        Bi(mp1,np1,k) = Bi(mp1,np1,k) - Zw(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         RETURN
      ELSEIF ( itypp==9 ) THEN
!
!     case ityp=8   v odd, w even, and both br and bi equal zero
!
!     case m=0
!
         DO k = 1 , Nt
            DO i = 1 , Imid
               DO np1 = 2 , ndo2 , 2
                  Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*We(i,1,k)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax<2 ) RETURN
         DO mp1 = 2 , mmax
            m = mp1 - 1
            mb = m*(Nlat-1) - (m*(m-1))/2
            mp2 = mp1 + 1
            IF ( mp2<=ndo2 ) THEN
               DO k = 1 , Nt
                  DO i = 1 , imm1
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-2,k) + Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-1,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)    &
                                      & *We(i,2*mp1-1,k) - Zw(np1+mb,i) &
                                      & *Vo(i,2*mp1-2,k)
                     ENDDO
                  ENDDO
               ENDDO
               IF ( mlat/=0 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-2,k)
                        Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,Imid) &
                                      & *We(Imid,2*mp1-1,k)
                     ENDDO
                  ENDDO
               ENDIF
            ENDIF
         ENDDO
         GOTO 99999
      ENDIF
!
!     case ityp=0 ,  no symmetries
!
!     case m=0
!
      DO k = 1 , Nt
         DO i = 1 , Imid
            DO np1 = 2 , ndo2 , 2
               Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Ve(i,1,k)
               Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*We(i,1,k)
            ENDDO
         ENDDO
      ENDDO
      DO k = 1 , Nt
         DO i = 1 , imm1
            DO np1 = 3 , ndo1 , 2
               Br(1,np1,k) = Br(1,np1,k) + Zv(np1,i)*Vo(i,1,k)
               Cr(1,np1,k) = Cr(1,np1,k) - Zv(np1,i)*Wo(i,1,k)
            ENDDO
         ENDDO
      ENDDO
!
!     case m = 1 through nlat-1
!
      IF ( mmax<2 ) RETURN
      DO mp1 = 2 , mmax
         m = mp1 - 1
         mb = m*(Nlat-1) - (m*(m-1))/2
         mp2 = mp1 + 1
         IF ( mp1<=ndo1 ) THEN
            DO k = 1 , Nt
               DO i = 1 , imm1
                  DO np1 = mp1 , ndo1 , 2
                     Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)       &
                                   & *Vo(i,2*mp1-2,k) + Zw(np1+mb,i)    &
                                   & *We(i,2*mp1-1,k)
                     Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)       &
                                   & *Vo(i,2*mp1-1,k) - Zw(np1+mb,i)    &
                                   & *We(i,2*mp1-2,k)
                     Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)       &
                                   & *Wo(i,2*mp1-2,k) + Zw(np1+mb,i)    &
                                   & *Ve(i,2*mp1-1,k)
                     Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)       &
                                   & *Wo(i,2*mp1-1,k) - Zw(np1+mb,i)    &
                                   & *Ve(i,2*mp1-2,k)
                  ENDDO
               ENDDO
            ENDDO
            IF ( mlat/=0 ) THEN
               DO k = 1 , Nt
                  DO np1 = mp1 , ndo1 , 2
                     Br(mp1,np1,k) = Br(mp1,np1,k) + Zw(np1+mb,Imid)    &
                                   & *We(Imid,2*mp1-1,k)
                     Bi(mp1,np1,k) = Bi(mp1,np1,k) - Zw(np1+mb,Imid)    &
                                   & *We(Imid,2*mp1-2,k)
                     Cr(mp1,np1,k) = Cr(mp1,np1,k) + Zw(np1+mb,Imid)    &
                                   & *Ve(Imid,2*mp1-1,k)
                     Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zw(np1+mb,Imid)    &
                                   & *Ve(Imid,2*mp1-2,k)
                  ENDDO
               ENDDO
            ENDIF
         ENDIF
         IF ( mp2<=ndo2 ) THEN
            DO k = 1 , Nt
               DO i = 1 , imm1
                  DO np1 = mp2 , ndo2 , 2
                     Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,i)       &
                                   & *Ve(i,2*mp1-2,k) + Zw(np1+mb,i)    &
                                   & *Wo(i,2*mp1-1,k)
                     Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,i)       &
                                   & *Ve(i,2*mp1-1,k) - Zw(np1+mb,i)    &
                                   & *Wo(i,2*mp1-2,k)
                     Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,i)       &
                                   & *We(i,2*mp1-2,k) + Zw(np1+mb,i)    &
                                   & *Vo(i,2*mp1-1,k)
                     Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,i)       &
                                   & *We(i,2*mp1-1,k) - Zw(np1+mb,i)    &
                                   & *Vo(i,2*mp1-2,k)
                  ENDDO
               ENDDO
            ENDDO
            IF ( mlat/=0 ) THEN
               DO k = 1 , Nt
                  DO np1 = mp2 , ndo2 , 2
                     Br(mp1,np1,k) = Br(mp1,np1,k) + Zv(np1+mb,Imid)    &
                                   & *Ve(Imid,2*mp1-2,k)
                     Bi(mp1,np1,k) = Bi(mp1,np1,k) + Zv(np1+mb,Imid)    &
                                   & *Ve(Imid,2*mp1-1,k)
                     Cr(mp1,np1,k) = Cr(mp1,np1,k) - Zv(np1+mb,Imid)    &
                                   & *We(Imid,2*mp1-2,k)
                     Ci(mp1,np1,k) = Ci(mp1,np1,k) - Zv(np1+mb,Imid)    &
                                   & *We(Imid,2*mp1-1,k)
                  ENDDO
               ENDDO
            ENDIF
         ENDIF
      ENDDO
      RETURN
99999 END SUBROUTINE DVHAES1


!
!     dwork must be of length at least 2*(nlat+1)
!
      SUBROUTINE DVHAESI(Nlat,Nlon,Wvhaes,Lvhaes,Work,Lwork,Dwork,Ldwork,&
                      & Ierror)
      IMPLICIT NONE
      INTEGER idz , Ierror , imid , iw1 , labc , Ldwork , Lvhaes ,      &
            & Lwork , lzimn , mmax , Nlat , Nlon
      DOUBLE PRECISION Work , Wvhaes
      DIMENSION Wvhaes(Lvhaes) , Work(Lwork)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      mmax = MIN0(Nlat,(Nlon+1)/2)
      imid = (Nlat+1)/2
      lzimn = (imid*mmax*(Nlat+Nlat-mmax+1))/2
      IF ( Lvhaes<lzimn+lzimn+Nlon+15 ) RETURN
      Ierror = 4
      labc = 3*(MAX0(mmax-2,0)*(Nlat+Nlat-mmax-1))/2
      IF ( Lwork<5*Nlat*imid+labc ) RETURN
      Ierror = 5
      IF ( Ldwork<2*(Nlat+1) ) RETURN
      Ierror = 0
      iw1 = 3*Nlat*imid + 1
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      CALL DVEA1(Nlat,Nlon,imid,Wvhaes,Wvhaes(lzimn+1),idz,Work,Work(iw1)&
              & ,Dwork)
      CALL DHRFFTI(Nlon,Wvhaes(2*lzimn+1))
      END SUBROUTINE DVHAESI


      SUBROUTINE DVEA1(Nlat,Nlon,Imid,Zv,Zw,Idz,Zin,Wzvin,Dwork)
      IMPLICIT NONE
      INTEGER i , i3 , Idz , Imid , m , mmax , mn , mp1 , Nlat , Nlon , &
            & np1
      DOUBLE PRECISION Wzvin , Zin , Zv , Zw
      DIMENSION Zv(Idz,1) , Zw(Idz,1) , Zin(Imid,Nlat,3) , Wzvin(1)
      DOUBLE PRECISION Dwork(*)
      mmax = MIN0(Nlat,(Nlon+1)/2)
      CALL DZVINIT(Nlat,Nlon,Wzvin,Dwork)
      DO mp1 = 1 , mmax
         m = mp1 - 1
         CALL DZVIN(0,Nlat,Nlon,m,Zin,i3,Wzvin)
         DO np1 = mp1 , Nlat
            mn = m*(Nlat-1) - (m*(m-1))/2 + np1
            DO i = 1 , Imid
               Zv(mn,i) = Zin(i,np1,i3)
            ENDDO
         ENDDO
      ENDDO
      CALL DZWINIT(Nlat,Nlon,Wzvin,Dwork)
      DO mp1 = 1 , mmax
         m = mp1 - 1
         CALL DZWIN(0,Nlat,Nlon,m,Zin,i3,Wzvin)
         DO np1 = mp1 , Nlat
            mn = m*(Nlat-1) - (m*(m-1))/2 + np1
            DO i = 1 , Imid
               Zw(mn,i) = Zin(i,np1,i3)
            ENDDO
         ENDDO
      ENDDO
      END SUBROUTINE DVEA1
