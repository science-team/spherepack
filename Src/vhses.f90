!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
! ... file vhses.f
!
!     this file contains code and documentation for subroutines
!     vhses and vhsesi
!
! ... files which must be loaded with vhses.f
!
!     sphcom.f, hrfft.f
!
!
!     subroutine vhses(nlat,nlon,ityp,nt,v,w,idvw,jdvw,br,bi,cr,ci,
!    +                 mdab,ndab,wvhses,lvhses,work,lwork,ierror)
!
!     subroutine vhses performs the vector spherical harmonic synthesis
!     of the arrays br, bi, cr, and ci and stores the result in the
!     arrays v and w. v(i,j) and w(i,j) are the colatitudinal
!     (measured from the north pole) and east longitudinal components
!     respectively, located at colatitude theta(i) = (i-1)*pi/(nlat-1)
!     and longitude phi(j) = (j-1)*2*pi/nlon. the spectral
!     representation of (v,w) is given below at output parameters v,w.
!
!     input parameters
!
!     nlat   the number of colatitudes on the full sphere including the
!            poles. for example, nlat = 37 for a five degree grid.
!            nlat determines the grid increment in colatitude as
!            pi/(nlat-1).  if nlat is odd the equator is located at
!            grid point i=(nlat+1)/2. if nlat is even the equator is
!            located half way between points i=nlat/2 and i=nlat/2+1.
!            nlat must be at least 3. note: on the half sphere, the
!            number of grid points in the colatitudinal direction is
!            nlat/2 if nlat is even or (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!     ityp   = 0  no symmetries exist about the equator. the synthesis
!                 is performed on the entire sphere.  i.e. on the
!                 arrays v(i,j),w(i,j) for i=1,...,nlat and
!                 j=1,...,nlon.
!
!            = 1  no symmetries exist about the equator. the synthesis
!                 is performed on the entire sphere.  i.e. on the
!                 arrays v(i,j),w(i,j) for i=1,...,nlat and
!                 j=1,...,nlon. the curl of (v,w) is zero. that is,
!                 (d/dtheta (sin(theta) w) - dv/dphi)/sin(theta) = 0.
!                 the coefficients cr and ci are zero.
!
!            = 2  no symmetries exist about the equator. the synthesis
!                 is performed on the entire sphere.  i.e. on the
!                 arrays v(i,j),w(i,j) for i=1,...,nlat and
!                 j=1,...,nlon. the divergence of (v,w) is zero. i.e.,
!                 (d/dtheta (sin(theta) v) + dw/dphi)/sin(theta) = 0.
!                 the coefficients br and bi are zero.
!
!            = 3  v is symmetric and w is antisymmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 4  v is symmetric and w is antisymmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the curl of (v,w) is zero. that is,
!                 (d/dtheta (sin(theta) w) - dv/dphi)/sin(theta) = 0.
!                 the coefficients cr and ci are zero.
!
!            = 5  v is symmetric and w is antisymmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the divergence of (v,w) is zero. i.e.,
!                 (d/dtheta (sin(theta) v) + dw/dphi)/sin(theta) = 0.
!                 the coefficients br and bi are zero.
!
!            = 6  v is antisymmetric and w is symmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 7  v is antisymmetric and w is symmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the curl of (v,w) is zero. that is,
!                 (d/dtheta (sin(theta) w) - dv/dphi)/sin(theta) = 0.
!                 the coefficients cr and ci are zero.
!
!            = 8  v is antisymmetric and w is symmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the divergence of (v,w) is zero. i.e.,
!                 (d/dtheta (sin(theta) v) + dw/dphi)/sin(theta) = 0.
!                 the coefficients br and bi are zero.
!
!
!     nt     the number of syntheses.  in the program that calls vhses,
!            the arrays v,w,br,bi,cr, and ci can be three dimensional
!            in which case multiple syntheses will be performed.
!            the third index is the synthesis index which assumes the
!            values k=1,...,nt.  for a single synthesis set nt=1. the
!            discription of the remaining parameters is simplified
!            by assuming that nt=1 or that all the arrays are two
!            dimensional.
!
!     idvw   the first dimension of the arrays v,w as it appears in
!            the program that calls vhaes. if ityp .le. 2 then idvw
!            must be at least nlat.  if ityp .gt. 2 and nlat is
!            even then idvw must be at least nlat/2. if ityp .gt. 2
!            and nlat is odd then idvw must be at least (nlat+1)/2.
!
!     jdvw   the second dimension of the arrays v,w as it appears in
!            the program that calls vhses. jdvw must be at least nlon.
!
!     br,bi  two or three dimensional arrays (see input parameter nt)
!     cr,ci  that contain the vector spherical harmonic coefficients
!            in the spectral representation of v(i,j) and w(i,j) given
!            below at the discription of output parameters v and w.
!
!     mdab   the first dimension of the arrays br,bi,cr, and ci as it
!            appears in the program that calls vhses. mdab must be at
!            least min0(nlat,nlon/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!     ndab   the second dimension of the arrays br,bi,cr, and ci as it
!            appears in the program that calls vhses. ndab must be at
!            least nlat.
!
!     wvhses an array which must be initialized by subroutine vhsesi.
!            once initialized, wvhses can be used repeatedly by vhses
!            as long as nlon and nlat remain unchanged.  wvhses must
!            not be altered between calls of vhses.
!
!     lvhses the dimension of the array wvhses as it appears in the
!            program that calls vhses. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lvhses must be at least
!
!                 l1*l2*(nlat+nlat-l1+1)+nlon+15
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls vhses. define
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            if ityp .le. 2 then lwork must be at least
!
!                       (2*nt+1)*nlat*nlon
!
!            if ityp .gt. 2 then lwork must be at least
!
!                        (2*nt+1)*l2*nlon
!
!     **************************************************************
!
!     output parameters
!
!     v,w    two or three dimensional arrays (see input parameter nt)
!            in which the synthesis is stored. v is the colatitudinal
!            component and w is the east longitudinal component.
!            v(i,j),w(i,j) contain the components at colatitude
!            theta(i) = (i-1)*pi/(nlat-1) and longitude phi(j) =
!            (j-1)*2*pi/nlon. the index ranges are defined above at
!            the input parameter ityp. v and w are computed from the
!            formulas given below
!
!
!     define
!
!     1.  theta is colatitude and phi is east longitude
!
!     2.  the normalized associated legendre funnctions
!
!         pbar(m,n,theta) = sqrt((2*n+1)*factorial(n-m)
!                        /(2*factorial(n+m)))*sin(theta)**m/(2**n*
!                        factorial(n)) times the (n+m)th derivative
!                        of (x**2-1)**n with respect to x=cos(theta)
!
!     3.  vbar(m,n,theta) = the derivative of pbar(m,n,theta) with
!                           respect to theta divided by the square
!                           root of n(n+1).
!
!         vbar(m,n,theta) is more easily computed in the form
!
!         vbar(m,n,theta) = (sqrt((n+m)*(n-m+1))*pbar(m-1,n,theta)
!         -sqrt((n-m)*(n+m+1))*pbar(m+1,n,theta))/(2*sqrt(n*(n+1)))
!
!     4.  wbar(m,n,theta) = m/(sin(theta))*pbar(m,n,theta) divided
!                           by the square root of n(n+1).
!
!         wbar(m,n,theta) is more easily computed in the form
!
!         wbar(m,n,theta) = sqrt((2n+1)/(2n-1))*(sqrt((n+m)*(n+m-1))
!         *pbar(m-1,n-1,theta)+sqrt((n-m)*(n-m-1))*pbar(m+1,n-1,theta))
!         /(2*sqrt(n*(n+1)))
!
!
!    the colatitudnal dependence of the normalized surface vector
!                spherical harmonics are defined by
!
!     5.    bbar(m,n,theta) = (vbar(m,n,theta),i*wbar(m,n,theta))
!
!     6.    cbar(m,n,theta) = (i*wbar(m,n,theta),-vbar(m,n,theta))
!
!
!    the coordinate to index mappings
!
!     7.   theta(i) = (i-1)*pi/(nlat-1) and phi(j) = (j-1)*2*pi/nlon
!
!
!     the maximum (plus one) longitudinal wave number
!
!     8.     mmax = min0(nlat,nlon/2) if nlon is even or
!            mmax = min0(nlat,(nlon+1)/2) if nlon is odd.
!
!    if we further define the output vector as
!
!     9.    h(i,j) = (v(i,j),w(i,j))
!
!    and the complex coefficients
!
!     10.   b(m,n) = cmplx(br(m+1,n+1),bi(m+1,n+1))
!
!     11.   c(m,n) = cmplx(cr(m+1,n+1),ci(m+1,n+1))
!
!
!    then for i=1,...,nlat and  j=1,...,nlon
!
!        the expansion for real h(i,j) takes the form
!
!     h(i,j) = the sum from n=1 to n=nlat-1 of the real part of
!
!         .5*(b(0,n)*bbar(0,n,theta(i))+c(0,n)*cbar(0,n,theta(i)))
!
!     plus the sum from m=1 to m=mmax-1 of the sum from n=m to
!     n=nlat-1 of the real part of
!
!              b(m,n)*bbar(m,n,theta(i))*exp(i*m*phi(j))
!             +c(m,n)*cbar(m,n,theta(i))*exp(i*m*phi(j))
!
!   *************************************************************
!
!   in terms of real variables this expansion takes the form
!
!             for i=1,...,nlat and  j=1,...,nlon
!
!     v(i,j) = the sum from n=1 to n=nlat-1 of
!
!               .5*br(1,n+1)*vbar(0,n,theta(i))
!
!     plus the sum from m=1 to m=mmax-1 of the sum from n=m to
!     n=nlat-1 of the real part of
!
!       (br(m+1,n+1)*vbar(m,n,theta(i))-ci(m+1,n+1)*wbar(m,n,theta(i)))
!                                          *cos(m*phi(j))
!      -(bi(m+1,n+1)*vbar(m,n,theta(i))+cr(m+1,n+1)*wbar(m,n,theta(i)))
!                                          *sin(m*phi(j))
!
!    and for i=1,...,nlat and  j=1,...,nlon
!
!     w(i,j) = the sum from n=1 to n=nlat-1 of
!
!              -.5*cr(1,n+1)*vbar(0,n,theta(i))
!
!     plus the sum from m=1 to m=mmax-1 of the sum from n=m to
!     n=nlat-1 of the real part of
!
!      -(cr(m+1,n+1)*vbar(m,n,theta(i))+bi(m+1,n+1)*wbar(m,n,theta(i)))
!                                          *cos(m*phi(j))
!      +(ci(m+1,n+1)*vbar(m,n,theta(i))-br(m+1,n+1)*wbar(m,n,theta(i)))
!                                          *sin(m*phi(j))
!
!
!      br(m+1,nlat),bi(m+1,nlat),cr(m+1,nlat), and ci(m+1,nlat) are
!      assumed zero for m even.
!
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of ityp
!            = 4  error in the specification of nt
!            = 5  error in the specification of idvw
!            = 6  error in the specification of jdvw
!            = 7  error in the specification of mdab
!            = 8  error in the specification of ndab
!            = 9  error in the specification of lvhses
!            = 10 error in the specification of lwork
!
! ************************************************************
!
!     subroutine vhsesi(nlat,nlon,wvhses,lvhses,work,lwork,dwork,
!    +                  ldwork,ierror)
!
!     subroutine vhsesi initializes the array wvhses which can then be
!     used repeatedly by subroutine vhses until nlat or nlon is changed.
!
!     input parameters
!
!     nlat   the number of colatitudes on the full sphere including the
!            poles. for example, nlat = 37 for a five degree grid.
!            nlat determines the grid increment in colatitude as
!            pi/(nlat-1).  if nlat is odd the equator is located at
!            grid point i=(nlat+1)/2. if nlat is even the equator is
!            located half way between points i=nlat/2 and i=nlat/2+1.
!            nlat must be at least 3. note: on the half sphere, the
!            number of grid points in the colatitudinal direction is
!            nlat/2 if nlat is even or (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!     lvhses the dimension of the array wvhses as it appears in the
!            program that calls vhses. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lvhses must be at least
!
!                  l1*l2*(nlat+nlat-l1+1)+nlon+15
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls vhses. lwork must be at least
!
!              3*(max0(l1-2,0)*(nlat+nlat-l1-1))/2+5*l2*nlat
!
!     dwork  an unsaved double precision work space
!
!     ldwork the length of the array dwork as it appears in the
!            program that calls vhsesi.  ldwork must be at least
!            2*(nlat+1)
!
!
!     **************************************************************
!
!     output parameters
!
!     wvhses an array which is initialized for use by subroutine vhses.
!            once initialized, wvhses can be used repeatedly by vhses
!            as long as nlat or nlon remain unchanged.  wvhses must not
!            be altered between calls of vhses.
!
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of lvhses
!            = 4  error in the specification of lwork
!            = 5  error in the specification of ldwork
!
! *****************************************
      SUBROUTINE VHSES(Nlat,Nlon,Ityp,Nt,V,W,Idvw,Jdvw,Br,Bi,Cr,Ci,Mdab,&
                     & Ndab,Wvhses,Lvhses,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL Bi , Br , Ci , Cr , V , W , Work , Wvhses
      INTEGER idv , Idvw , idz , Ierror , imid , ist , Ityp , iw1 ,     &
            & iw2 , iw3 , iw4 , Jdvw , jw1 , jw2 , lnl , Lvhses ,       &
            & Lwork , lzimn , Mdab , mmax
      INTEGER Ndab , Nlat , Nlon , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Work(1) , Wvhses(1)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      IF ( Ityp<0 .OR. Ityp>8 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Ityp<=2 .AND. Idvw<Nlat) .OR. (Ityp>2 .AND. Idvw<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdab<mmax ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
      IF ( Lvhses<lzimn+lzimn+Nlon+15 ) RETURN
      Ierror = 10
      idv = Nlat
      IF ( Ityp>2 ) idv = imid
      lnl = Nt*idv*Nlon
      IF ( Lwork<lnl+lnl+idv*Nlon ) RETURN
      Ierror = 0
      ist = 0
      IF ( Ityp<=2 ) ist = imid
      iw1 = ist + 1
      iw2 = lnl + 1
      iw3 = iw2 + ist
      iw4 = iw2 + lnl
      jw1 = lzimn + 1
      jw2 = jw1 + lzimn
      CALL VHSES1(Nlat,Nlon,Ityp,Nt,imid,Idvw,Jdvw,V,W,Mdab,Ndab,Br,Bi, &
                & Cr,Ci,idv,Work,Work(iw1),Work(iw2),Work(iw3),Work(iw4)&
                & ,idz,Wvhses,Wvhses(jw1),Wvhses(jw2))
      END SUBROUTINE VHSES

      
      SUBROUTINE VHSES1(Nlat,Nlon,Ityp,Nt,Imid,Idvw,Jdvw,V,W,Mdab,Ndab, &
                      & Br,Bi,Cr,Ci,Idv,Ve,Vo,We,Wo,Work,Idz,Vb,Wb,     &
                      & Wrfft)
      IMPLICIT NONE
      REAL Bi , Br , Ci , Cr , V , Vb , Ve , Vo , W , Wb , We , Wo ,    &
         & Work , Wrfft
      INTEGER i , Idv , Idvw , Idz , Imid , imm1 , Ityp , itypp , j ,   &
            & Jdvw , k , m , mb , Mdab , mlat , mlon , mmax , mn , mp1 ,&
            & mp2
      INTEGER Ndab , ndo1 , ndo2 , Nlat , Nlon , nlp1 , np1 , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Ve(Idv,Nlon,1) , Vo(Idv,Nlon,1) , We(Idv,Nlon,1) ,      &
              & Wo(Idv,Nlon,1) , Work(1) , Wrfft(1) , Vb(Imid,1) ,      &
              & Wb(Imid,1)
      nlp1 = Nlat + 1
      mlat = MOD(Nlat,2)
      mlon = MOD(Nlon,2)
      mmax = MIN0(Nlat,(Nlon+1)/2)
      imm1 = Imid
      IF ( mlat/=0 ) imm1 = Imid - 1
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Idv
               Ve(i,j,k) = 0.
               We(i,j,k) = 0.
            ENDDO
         ENDDO
      ENDDO
      ndo1 = Nlat
      ndo2 = Nlat
      IF ( mlat/=0 ) ndo1 = Nlat - 1
      IF ( mlat==0 ) ndo2 = Nlat - 1
      itypp = Ityp + 1
      IF ( itypp==2 ) THEN
!
!     case ityp=1   no symmetries,  cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==3 ) THEN
!
!     case ityp=2   no symmetries,  br and bi are equal to zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==4 ) THEN
!
!     case ityp=3   v even,  w odd
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==5 ) THEN
!
!     case ityp=4   v even,  w odd, and both cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==6 ) THEN
!
!     case ityp=5   v even,  w odd,     br and bi equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==7 ) THEN
!
!     case ityp=6   v odd  ,  w even
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==8 ) THEN
!
!     case ityp=7   v odd, w even   cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==9 ) THEN
!
!     case ityp=8   v odd,  w even   br and bi equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSE
!
!     case ityp=0   no symmetries
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ENDIF
      DO k = 1 , Nt
         CALL HRFFTB(Idv,Nlon,Ve(1,1,k),Idv,Wrfft,Work)
         CALL HRFFTB(Idv,Nlon,We(1,1,k),Idv,Wrfft,Work)
      ENDDO
      IF ( Ityp>2 ) THEN
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , imm1
                  V(i,j,k) = .5*Ve(i,j,k)
                  W(i,j,k) = .5*We(i,j,k)
               ENDDO
            ENDDO
         ENDDO
      ELSE
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , imm1
                  V(i,j,k) = .5*(Ve(i,j,k)+Vo(i,j,k))
                  W(i,j,k) = .5*(We(i,j,k)+Wo(i,j,k))
                  V(nlp1-i,j,k) = .5*(Ve(i,j,k)-Vo(i,j,k))
                  W(nlp1-i,j,k) = .5*(We(i,j,k)-Wo(i,j,k))
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      IF ( mlat==0 ) RETURN
      DO k = 1 , Nt
         DO j = 1 , Nlon
            V(Imid,j,k) = .5*Ve(Imid,j,k)
            W(Imid,j,k) = .5*We(Imid,j,k)
         ENDDO
      ENDDO
      END SUBROUTINE VHSES1

      
      SUBROUTINE VHSESI(Nlat,Nlon,Wvhses,Lvhses,Work,Lwork,Dwork,Ldwork,&
                      & Ierror)
      IMPLICIT NONE
      INTEGER idz , Ierror , imid , iw1 , labc , Ldwork , Lvhses ,      &
            & Lwork , lzimn , mmax , Nlat , Nlon
      REAL Work , Wvhses
      DIMENSION Wvhses(Lvhses) , Work(Lwork)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      mmax = MIN0(Nlat,(Nlon+1)/2)
      imid = (Nlat+1)/2
      lzimn = (imid*mmax*(Nlat+Nlat-mmax+1))/2
      IF ( Lvhses<lzimn+lzimn+Nlon+15 ) RETURN
      Ierror = 4
      labc = 3*(MAX0(mmax-2,0)*(Nlat+Nlat-mmax-1))/2
      IF ( Lwork<5*Nlat*imid+labc ) RETURN
      Ierror = 5
      IF ( Ldwork<2*(Nlat+1) ) RETURN
      Ierror = 0
      iw1 = 3*Nlat*imid + 1
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      CALL VES1(Nlat,Nlon,imid,Wvhses,Wvhses(lzimn+1),idz,Work,Work(iw1)&
              & ,Dwork)
      CALL HRFFTI(Nlon,Wvhses(2*lzimn+1))
      END SUBROUTINE VHSESI


      SUBROUTINE VES1(Nlat,Nlon,Imid,Vb,Wb,Idz,Vin,Wzvin,Dwork)
      IMPLICIT NONE
      INTEGER i , i3 , Idz , Imid , m , mmax , mn , mp1 , Nlat , Nlon , &
            & np1
      REAL Vb , Vin , Wb , Wzvin
      DIMENSION Vb(Imid,*) , Wb(Imid,*) , Vin(Imid,Nlat,3) , Wzvin(*)
      DOUBLE PRECISION Dwork(*)
      mmax = MIN0(Nlat,(Nlon+1)/2)
      CALL VBINIT(Nlat,Nlon,Wzvin,Dwork)
      DO mp1 = 1 , mmax
         m = mp1 - 1
         CALL VBIN(0,Nlat,Nlon,m,Vin,i3,Wzvin)
         DO np1 = mp1 , Nlat
            mn = m*(Nlat-1) - (m*(m-1))/2 + np1
            DO i = 1 , Imid
               Vb(i,mn) = Vin(i,np1,i3)
            ENDDO
         ENDDO
      ENDDO
      CALL WBINIT(Nlat,Nlon,Wzvin,Dwork)
      DO mp1 = 1 , mmax
         m = mp1 - 1
         CALL WBIN(0,Nlat,Nlon,m,Vin,i3,Wzvin)
         DO np1 = mp1 , Nlat
            mn = m*(Nlat-1) - (m*(m-1))/2 + np1
            DO i = 1 , Imid
               Wb(i,mn) = Vin(i,np1,i3)
            ENDDO
         ENDDO
      ENDDO
      END SUBROUTINE VES1

      
      SUBROUTINE DVHSES(Nlat,Nlon,Ityp,Nt,V,W,Idvw,Jdvw,Br,Bi,Cr,Ci,Mdab,&
                     & Ndab,Wvhses,Lvhses,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Br , Ci , Cr , V , W , Work , Wvhses
      INTEGER idv , Idvw , idz , Ierror , imid , ist , Ityp , iw1 ,     &
            & iw2 , iw3 , iw4 , Jdvw , jw1 , jw2 , lnl , Lvhses ,       &
            & Lwork , lzimn , Mdab , mmax
      INTEGER Ndab , Nlat , Nlon , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Work(1) , Wvhses(1)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      IF ( Ityp<0 .OR. Ityp>8 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Ityp<=2 .AND. Idvw<Nlat) .OR. (Ityp>2 .AND. Idvw<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdab<mmax ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
      IF ( Lvhses<lzimn+lzimn+Nlon+15 ) RETURN
      Ierror = 10
      idv = Nlat
      IF ( Ityp>2 ) idv = imid
      lnl = Nt*idv*Nlon
      IF ( Lwork<lnl+lnl+idv*Nlon ) RETURN
      Ierror = 0
      ist = 0
      IF ( Ityp<=2 ) ist = imid
      iw1 = ist + 1
      iw2 = lnl + 1
      iw3 = iw2 + ist
      iw4 = iw2 + lnl
      jw1 = lzimn + 1
      jw2 = jw1 + lzimn
      CALL DVHSES1(Nlat,Nlon,Ityp,Nt,imid,Idvw,Jdvw,V,W,Mdab,Ndab,Br,Bi, &
                & Cr,Ci,idv,Work,Work(iw1),Work(iw2),Work(iw3),Work(iw4)&
                & ,idz,Wvhses,Wvhses(jw1),Wvhses(jw2))
      END SUBROUTINE DVHSES

      
      SUBROUTINE DVHSES1(Nlat,Nlon,Ityp,Nt,Imid,Idvw,Jdvw,V,W,Mdab,Ndab, &
                      & Br,Bi,Cr,Ci,Idv,Ve,Vo,We,Wo,Work,Idz,Vb,Wb,     &
                      & Wrfft)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Br , Ci , Cr , V , Vb , Ve , Vo , W , Wb , We , Wo ,    &
         & Work , Wrfft
      INTEGER i , Idv , Idvw , Idz , Imid , imm1 , Ityp , itypp , j ,   &
            & Jdvw , k , m , mb , Mdab , mlat , mlon , mmax , mn , mp1 ,&
            & mp2
      INTEGER Ndab , ndo1 , ndo2 , Nlat , Nlon , nlp1 , np1 , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Ve(Idv,Nlon,1) , Vo(Idv,Nlon,1) , We(Idv,Nlon,1) ,      &
              & Wo(Idv,Nlon,1) , Work(1) , Wrfft(1) , Vb(Imid,1) ,      &
              & Wb(Imid,1)
      nlp1 = Nlat + 1
      mlat = MOD(Nlat,2)
      mlon = MOD(Nlon,2)
      mmax = MIN0(Nlat,(Nlon+1)/2)
      imm1 = Imid
      IF ( mlat/=0 ) imm1 = Imid - 1
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Idv
               Ve(i,j,k) = 0.
               We(i,j,k) = 0.
            ENDDO
         ENDDO
      ENDDO
      ndo1 = Nlat
      ndo2 = Nlat
      IF ( mlat/=0 ) ndo1 = Nlat - 1
      IF ( mlat==0 ) ndo2 = Nlat - 1
      itypp = Ityp + 1
      IF ( itypp==2 ) THEN
!
!     case ityp=1   no symmetries,  cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==3 ) THEN
!
!     case ityp=2   no symmetries,  br and bi are equal to zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==4 ) THEN
!
!     case ityp=3   v even,  w odd
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==5 ) THEN
!
!     case ityp=4   v even,  w odd, and both cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==6 ) THEN
!
!     case ityp=5   v even,  w odd,     br and bi equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==7 ) THEN
!
!     case ityp=6   v odd  ,  w even
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==8 ) THEN
!
!     case ityp=7   v odd, w even   cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==9 ) THEN
!
!     case ityp=8   v odd,  w even   br and bi equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSE
!
!     case ityp=0   no symmetries
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
               mb = m*(Nlat-1) - (m*(m-1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ENDIF
      DO k = 1 , Nt
         CALL DHRFFTB(Idv,Nlon,Ve(1,1,k),Idv,Wrfft,Work)
         CALL DHRFFTB(Idv,Nlon,We(1,1,k),Idv,Wrfft,Work)
      ENDDO
      IF ( Ityp>2 ) THEN
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , imm1
                  V(i,j,k) = .5*Ve(i,j,k)
                  W(i,j,k) = .5*We(i,j,k)
               ENDDO
            ENDDO
         ENDDO
      ELSE
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , imm1
                  V(i,j,k) = .5*(Ve(i,j,k)+Vo(i,j,k))
                  W(i,j,k) = .5*(We(i,j,k)+Wo(i,j,k))
                  V(nlp1-i,j,k) = .5*(Ve(i,j,k)-Vo(i,j,k))
                  W(nlp1-i,j,k) = .5*(We(i,j,k)-Wo(i,j,k))
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      IF ( mlat==0 ) RETURN
      DO k = 1 , Nt
         DO j = 1 , Nlon
            V(Imid,j,k) = .5*Ve(Imid,j,k)
            W(Imid,j,k) = .5*We(Imid,j,k)
         ENDDO
      ENDDO
      END SUBROUTINE DVHSES1

      
      SUBROUTINE DVHSESI(Nlat,Nlon,Wvhses,Lvhses,Work,Lwork,Dwork,Ldwork,&
                      & Ierror)
      IMPLICIT NONE
      INTEGER idz , Ierror , imid , iw1 , labc , Ldwork , Lvhses ,      &
            & Lwork , lzimn , mmax , Nlat , Nlon
      DOUBLE PRECISION Work , Wvhses
      DIMENSION Wvhses(Lvhses) , Work(Lwork)
      DOUBLE PRECISION Dwork(Ldwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      mmax = MIN0(Nlat,(Nlon+1)/2)
      imid = (Nlat+1)/2
      lzimn = (imid*mmax*(Nlat+Nlat-mmax+1))/2
      IF ( Lvhses<lzimn+lzimn+Nlon+15 ) RETURN
      Ierror = 4
      labc = 3*(MAX0(mmax-2,0)*(Nlat+Nlat-mmax-1))/2
      IF ( Lwork<5*Nlat*imid+labc ) RETURN
      Ierror = 5
      IF ( Ldwork<2*(Nlat+1) ) RETURN
      Ierror = 0
      iw1 = 3*Nlat*imid + 1
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      CALL DVES1(Nlat,Nlon,imid,Wvhses,Wvhses(lzimn+1),idz,Work,Work(iw1)&
              & ,Dwork)
      CALL DHRFFTI(Nlon,Wvhses(2*lzimn+1))
      END SUBROUTINE DVHSESI


      SUBROUTINE DVES1(Nlat,Nlon,Imid,Vb,Wb,Idz,Vin,Wzvin,Dwork)
      IMPLICIT NONE
      INTEGER i , i3 , Idz , Imid , m , mmax , mn , mp1 , Nlat , Nlon , &
            & np1
      DOUBLE PRECISION Vb , Vin , Wb , Wzvin
      DIMENSION Vb(Imid,*) , Wb(Imid,*) , Vin(Imid,Nlat,3) , Wzvin(*)
      DOUBLE PRECISION Dwork(*)
      mmax = MIN0(Nlat,(Nlon+1)/2)
      CALL DVBINIT(Nlat,Nlon,Wzvin,Dwork)
      DO mp1 = 1 , mmax
         m = mp1 - 1
         CALL DVBIN(0,Nlat,Nlon,m,Vin,i3,Wzvin)
         DO np1 = mp1 , Nlat
            mn = m*(Nlat-1) - (m*(m-1))/2 + np1
            DO i = 1 , Imid
               Vb(i,mn) = Vin(i,np1,i3)
            ENDDO
         ENDDO
      ENDDO
      CALL DWBINIT(Nlat,Nlon,Wzvin,Dwork)
      DO mp1 = 1 , mmax
         m = mp1 - 1
         CALL DWBIN(0,Nlat,Nlon,m,Vin,i3,Wzvin)
         DO np1 = mp1 , Nlat
            mn = m*(Nlat-1) - (m*(m-1))/2 + np1
            DO i = 1 , Imid
               Wb(i,mn) = Vin(i,np1,i3)
            ENDDO
         ENDDO
      ENDDO
      END SUBROUTINE DVES1
