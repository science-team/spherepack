!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
! ... file vhsgs.f
!
!     this file contains code and documentation for subroutines
!     vhsgs and vhsgsi
!
! ... files which must be loaded with vhsgs.f
!
!     sphcom.f, hrfft.f, gaqd.f
!
!     subroutine vhsgs(nlat,nlon,ityp,nt,v,w,idvw,jdvw,br,bi,cr,ci,
!    +                 mdab,ndab,wvhsgs,lvhsgs,work,lwork,ierror)
!
!
!     subroutine vhsgs performs the vector spherical harmonic synthesis
!     of the arrays br, bi, cr, and ci and stores the result in the
!     arrays v and w.  the synthesis is performed on an equally spaced
!     longitude grid and a gaussian colatitude grid (measured from
!     the north pole). v(i,j) and w(i,j) are the colatitudinal and
!     east longitudinal components respectively, located at the i(th)
!     colatitude gaussian point (see nlat below) and longitude
!     phi(j) = (j-1)*2*pi/nlon.  the spectral respresentation of (v,w)
!     is given below at output parameters v,w.
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are computed
!            in radians in theta(1) <...< theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid point
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!     ityp   = 0  no symmetries exist about the equator. the synthesis
!                 is performed on the entire sphere.  i.e. on the
!                 arrays v(i,j),w(i,j) for i=1,...,nlat and
!                 j=1,...,nlon.
!
!            = 1  no symmetries exist about the equator. the synthesis
!                 is performed on the entire sphere.  i.e. on the
!                 arrays v(i,j),w(i,j) for i=1,...,nlat and
!                 j=1,...,nlon. the curl of (v,w) is zero. that is,
!                 (d/dtheta (sin(theta) w) - dv/dphi)/sin(theta) = 0.
!                 the coefficients cr and ci are zero.
!
!            = 2  no symmetries exist about the equator. the synthesis
!                 is performed on the entire sphere.  i.e. on the
!                 arrays v(i,j),w(i,j) for i=1,...,nlat and
!                 j=1,...,nlon. the divergence of (v,w) is zero. i.e.,
!                 (d/dtheta (sin(theta) v) + dw/dphi)/sin(theta) = 0.
!                 the coefficients br and bi are zero.
!
!            = 3  v is symmetric and w is antisymmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 4  v is symmetric and w is antisymmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the curl of (v,w) is zero. that is,
!                 (d/dtheta (sin(theta) w) - dv/dphi)/sin(theta) = 0.
!                 the coefficients cr and ci are zero.
!
!            = 5  v is symmetric and w is antisymmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the divergence of (v,w) is zero. i.e.,
!                 (d/dtheta (sin(theta) v) + dw/dphi)/sin(theta) = 0.
!                 the coefficients br and bi are zero.
!
!            = 6  v is antisymmetric and w is symmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 7  v is antisymmetric and w is symmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the curl of (v,w) is zero. that is,
!                 (d/dtheta (sin(theta) w) - dv/dphi)/sin(theta) = 0.
!                 the coefficients cr and ci are zero.
!
!            = 8  v is antisymmetric and w is symmetric about the
!                 equator. the synthesis is performed on the northern
!                 hemisphere only.  i.e., if nlat is odd the synthesis
!                 is performed on the arrays v(i,j),w(i,j) for
!                 i=1,...,(nlat+1)/2 and j=1,...,nlon. if nlat is
!                 even the synthesis is performed on the the arrays
!                 v(i,j),w(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!                 the divergence of (v,w) is zero. i.e.,
!                 (d/dtheta (sin(theta) v) + dw/dphi)/sin(theta) = 0.
!                 the coefficients br and bi are zero.
!
!
!     nt     the number of syntheses.  in the program that calls vhsgs,
!            the arrays v,w,br,bi,cr, and ci can be three dimensional
!            in which case multiple syntheses will be performed.
!            the third index is the synthesis index which assumes the
!            values k=1,...,nt.  for a single synthesis set nt=1. the
!            discription of the remaining parameters is simplified
!            by assuming that nt=1 or that all the arrays are two
!            dimensional.
!
!     idvw   the first dimension of the arrays v,w as it appears in
!            the program that calls vhags. if ityp .le. 2 then idvw
!            must be at least nlat.  if ityp .gt. 2 and nlat is
!            even then idvw must be at least nlat/2. if ityp .gt. 2
!            and nlat is odd then idvw must be at least (nlat+1)/2.
!
!     jdvw   the second dimension of the arrays v,w as it appears in
!            the program that calls vhsgs. jdvw must be at least nlon.
!
!     br,bi  two or three dimensional arrays (see input parameter nt)
!     cr,ci  that contain the vector spherical harmonic coefficients
!            in the spectral representation of v(i,j) and w(i,j) given
!            below at the discription of output parameters v and w.
!
!     mdab   the first dimension of the arrays br,bi,cr, and ci as it
!            appears in the program that calls vhsgs. mdab must be at
!            least min0(nlat,nlon/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!     ndab   the second dimension of the arrays br,bi,cr, and ci as it
!            appears in the program that calls vhsgs. ndab must be at
!            least nlat.
!
!     wvhsgs an array which must be initialized by subroutine vhsgsi.
!            once initialized, wvhsgs can be used repeatedly by vhsgs
!            as long as nlon and nlat remain unchanged.  wvhsgs must
!            not be altered between calls of vhsgs.
!
!     lvhsgs the dimension of the array wvhsgs as it appears in the
!            program that calls vhsgs. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lvhsgs must be at least
!
!                 l1*l2*(nlat+nlat-l1+1)+nlon+15+2*nlat
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls vhsgs. define
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            if ityp .le. 2 then lwork must be at least
!
!                       (2*nt+1)*nlat*nlon
!
!            if ityp .gt. 2 then lwork must be at least
!
!                        (2*nt+1)*l2*nlon
!
!     **************************************************************
!
!     output parameters
!
!     v,w    two or three dimensional arrays (see input parameter nt)
!            in which the synthesis is stored. v is the colatitudinal
!            component and w is the east longitudinal component.
!            v(i,j),w(i,j) contain the components at the guassian colatitude
!            point theta(i) and longitude phi(j) = (j-1)*2*pi/nlon.
!            the index ranges are defined above at the input parameter
!            ityp. v and w are computed from the formulas given below.
!
!
!     define
!
!     1.  theta is colatitude and phi is east longitude
!
!     2.  the normalized associated legendre funnctions
!
!         pbar(m,n,theta) = sqrt((2*n+1)*factorial(n-m)
!                        /(2*factorial(n+m)))*sin(theta)**m/(2**n*
!                        factorial(n)) times the (n+m)th derivative
!                        of (x**2-1)**n with respect to x=cos(theta)
!
!     3.  vbar(m,n,theta) = the derivative of pbar(m,n,theta) with
!                           respect to theta divided by the square
!                           root of n(n+1).
!
!         vbar(m,n,theta) is more easily computed in the form
!
!         vbar(m,n,theta) = (sqrt((n+m)*(n-m+1))*pbar(m-1,n,theta)
!         -sqrt((n-m)*(n+m+1))*pbar(m+1,n,theta))/(2*sqrt(n*(n+1)))
!
!     4.  wbar(m,n,theta) = m/(sin(theta))*pbar(m,n,theta) divided
!                           by the square root of n(n+1).
!
!         wbar(m,n,theta) is more easily computed in the form
!
!         wbar(m,n,theta) = sqrt((2n+1)/(2n-1))*(sqrt((n+m)*(n+m-1))
!         *pbar(m-1,n-1,theta)+sqrt((n-m)*(n-m-1))*pbar(m+1,n-1,theta))
!         /(2*sqrt(n*(n+1)))
!
!
!    the colatitudnal dependence of the normalized surface vector
!                spherical harmonics are defined by
!
!     5.    bbar(m,n,theta) = (vbar(m,n,theta),i*wbar(m,n,theta))
!
!     6.    cbar(m,n,theta) = (i*wbar(m,n,theta),-vbar(m,n,theta))
!
!
!    the coordinate to index mappings
!
!     7.   theta(i) = i(th) gaussian grid point and phi(j) = (j-1)*2*pi/nlon
!
!
!     the maximum (plus one) longitudinal wave number
!
!     8.     mmax = min0(nlat,nlon/2) if nlon is even or
!            mmax = min0(nlat,(nlon+1)/2) if nlon is odd.
!
!    if we further define the output vector as
!
!     9.    h(i,j) = (v(i,j),w(i,j))
!
!    and the complex coefficients
!
!     10.   b(m,n) = cmplx(br(m+1,n+1),bi(m+1,n+1))
!
!     11.   c(m,n) = cmplx(cr(m+1,n+1),ci(m+1,n+1))
!
!
!    then for i=1,...,nlat and  j=1,...,nlon
!
!        the expansion for real h(i,j) takes the form
!
!     h(i,j) = the sum from n=1 to n=nlat-1 of the real part of
!
!         .5*(b(0,n)*bbar(0,n,theta(i))+c(0,n)*cbar(0,n,theta(i)))
!
!     plus the sum from m=1 to m=mmax-1 of the sum from n=m to
!     n=nlat-1 of the real part of
!
!              b(m,n)*bbar(m,n,theta(i))*exp(i*m*phi(j))
!             +c(m,n)*cbar(m,n,theta(i))*exp(i*m*phi(j))
!
!   *************************************************************
!
!   in terms of real variables this expansion takes the form
!
!             for i=1,...,nlat and  j=1,...,nlon
!
!     v(i,j) = the sum from n=1 to n=nlat-1 of
!
!               .5*br(1,n+1)*vbar(0,n,theta(i))
!
!     plus the sum from m=1 to m=mmax-1 of the sum from n=m to
!     n=nlat-1 of the real part of
!
!       (br(m+1,n+1)*vbar(m,n,theta(i))-ci(m+1,n+1)*wbar(m,n,theta(i)))
!                                          *cos(m*phi(j))
!      -(bi(m+1,n+1)*vbar(m,n,theta(i))+cr(m+1,n+1)*wbar(m,n,theta(i)))
!                                          *sin(m*phi(j))
!
!    and for i=1,...,nlat and  j=1,...,nlon
!
!     w(i,j) = the sum from n=1 to n=nlat-1 of
!
!              -.5*cr(1,n+1)*vbar(0,n,theta(i))
!
!     plus the sum from m=1 to m=mmax-1 of the sum from n=m to
!     n=nlat-1 of the real part of
!
!      -(cr(m+1,n+1)*vbar(m,n,theta(i))+bi(m+1,n+1)*wbar(m,n,theta(i)))
!                                          *cos(m*phi(j))
!      +(ci(m+1,n+1)*vbar(m,n,theta(i))-br(m+1,n+1)*wbar(m,n,theta(i)))
!                                          *sin(m*phi(j))
!
!
!      br(m+1,nlat),bi(m+1,nlat),cr(m+1,nlat), and ci(m+1,nlat) are
!      assumed zero for m even.
!
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of ityp
!            = 4  error in the specification of nt
!            = 5  error in the specification of idvw
!            = 6  error in the specification of jdvw
!            = 7  error in the specification of mdab
!            = 8  error in the specification of ndab
!            = 9  error in the specification of lvhsgs
!            = 10 error in the specification of lwork
!
!
!     subroutine vhsgsi(nlat,nlon,wvhsgs,lvhsgs,dwork,ldwork,ierror)
!
!     subroutine vhsgsi initializes the array wvhsgs which can then be
!     used repeatedly by subroutine vhsgs until nlat or nlon is changed.
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are computed
!            in radians in theta(1) <...< theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid point
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!     lvhsgs the dimension of the array wvhsgs as it appears in the
!            program that calls vhsgs. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lvhsgs must be at least
!
!                 l1*l2*(nlat+nlat-l1+1)+nlon+15+2*nlat
!
!     dwork a double precision work array that does not need to be saved
!
!     ldwork the dimension of the array dwork as it appears in the
!            program that calls vhsgsi. ldwork must be at least
!
!                 (3*nlat*(nlat+3)+2)/2
 
!
!     **************************************************************
!
!     output parameters
!
!     wvhsgs an array which is initialized for use by subroutine vhsgs.
!            once initialized, wvhsgs can be used repeatedly by vhsgs
!            as long as nlat and nlon remain unchanged.  wvhsgs must not
!            be altered between calls of vhsgs.
!
!
!     ierror = 0  no errors
!            = 1  error in the specification of nlat
!            = 2  error in the specification of nlon
!            = 3  error in the specification of lvhsgs
!            = 4  error in the specification of lwork
!
      SUBROUTINE VHSGS(Nlat,Nlon,Ityp,Nt,V,W,Idvw,Jdvw,Br,Bi,Cr,Ci,Mdab,&
                     & Ndab,Wvhsgs,Lvhsgs,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL Bi , Br , Ci , Cr , V , W , Work , Wvhsgs
      INTEGER idv , Idvw , idz , Ierror , imid , ist , Ityp , iw1 ,     &
            & iw2 , iw3 , iw4 , Jdvw , jw1 , jw2 , jw3 , lmn , lnl ,    &
            & Lvhsgs , Lwork , lzimn
      INTEGER Mdab , mmax , Ndab , Nlat , Nlon , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Work(1) , Wvhsgs(1)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      IF ( Ityp<0 .OR. Ityp>8 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Ityp<=2 .AND. Idvw<Nlat) .OR. (Ityp>2 .AND. Idvw<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdab<mmax ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
      IF ( Lvhsgs<lzimn+lzimn+Nlon+15 ) RETURN
      Ierror = 10
      idv = Nlat
      IF ( Ityp>2 ) idv = imid
      lnl = Nt*idv*Nlon
      IF ( Lwork<lnl+lnl+idv*Nlon ) RETURN
      Ierror = 0
      ist = 0
      IF ( Ityp<=2 ) ist = imid
!
!     set wvhsgs pointers
!
      lmn = Nlat*(Nlat+1)/2
      jw1 = 1
      jw2 = jw1 + imid*lmn
      jw3 = jw2 + imid*lmn
!
!     set work pointers
!
      iw1 = ist + 1
      iw2 = lnl + 1
      iw3 = iw2 + ist
      iw4 = iw2 + lnl
 
      CALL VHSGS1(Nlat,Nlon,Ityp,Nt,imid,Idvw,Jdvw,V,W,Mdab,Ndab,Br,Bi, &
                & Cr,Ci,idv,Work,Work(iw1),Work(iw2),Work(iw3),Work(iw4)&
                & ,idz,Wvhsgs(jw1),Wvhsgs(jw2),Wvhsgs(jw3))
    END SUBROUTINE VHSGS

      
      SUBROUTINE VHSGS1(Nlat,Nlon,Ityp,Nt,Imid,Idvw,Jdvw,V,W,Mdab,Ndab, &
                      & Br,Bi,Cr,Ci,Idv,Ve,Vo,We,Wo,Work,Idz,Vb,Wb,     &
                      & Wrfft)
      IMPLICIT NONE
      REAL Bi , Br , Ci , Cr , V , Vb , Ve , Vo , W , Wb , We , Wo ,    &
         & Work , Wrfft
      INTEGER i , Idv , Idvw , Idz , Imid , imm1 , Ityp , itypp , j ,   &
            & Jdvw , k , m , mb , Mdab , mlat , mlon , mmax , mn , mp1 ,&
            & mp2
      INTEGER Ndab , ndo1 , ndo2 , Nlat , Nlon , nlp1 , np1 , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Ve(Idv,Nlon,1) , Vo(Idv,Nlon,1) , We(Idv,Nlon,1) ,      &
              & Wo(Idv,Nlon,1) , Work(1) , Wrfft(1) , Vb(Imid,1) ,      &
              & Wb(Imid,1)
      nlp1 = Nlat + 1
      mlat = MOD(Nlat,2)
      mlon = MOD(Nlon,2)
      mmax = MIN0(Nlat,(Nlon+1)/2)
      imm1 = Imid
      IF ( mlat/=0 ) imm1 = Imid - 1
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Idv
               Ve(i,j,k) = 0.
               We(i,j,k) = 0.
            ENDDO
         ENDDO
      ENDDO
      ndo1 = Nlat
      ndo2 = Nlat
      IF ( mlat/=0 ) ndo1 = Nlat - 1
      IF ( mlat==0 ) ndo2 = Nlat - 1
      itypp = Ityp + 1
      IF ( itypp==2 ) THEN
!
!     case ityp=1   no symmetries,  cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==3 ) THEN
!
!     case ityp=2   no symmetries,  br and bi are equal to zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==4 ) THEN
!
!     case ityp=3   v even,  w odd
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==5 ) THEN
!
!     case ityp=4   v even,  w odd, and both cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==6 ) THEN
!
!     case ityp=5   v even,  w odd,     br and bi equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==7 ) THEN
!
!     case ityp=6   v odd  ,  w even
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==8 ) THEN
!
!     case ityp=7   v odd, w even   cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==9 ) THEN
!
!     case ityp=8   v odd,  w even   br and bi equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSE
!
!     case ityp=0   no symmetries
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ENDIF
      DO k = 1 , Nt
         CALL HRFFTB(Idv,Nlon,Ve(1,1,k),Idv,Wrfft,Work)
         CALL HRFFTB(Idv,Nlon,We(1,1,k),Idv,Wrfft,Work)
      ENDDO
      IF ( Ityp>2 ) THEN
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , imm1
                  V(i,j,k) = .5*Ve(i,j,k)
                  W(i,j,k) = .5*We(i,j,k)
               ENDDO
            ENDDO
         ENDDO
      ELSE
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , imm1
                  V(i,j,k) = .5*(Ve(i,j,k)+Vo(i,j,k))
                  W(i,j,k) = .5*(We(i,j,k)+Wo(i,j,k))
                  V(nlp1-i,j,k) = .5*(Ve(i,j,k)-Vo(i,j,k))
                  W(nlp1-i,j,k) = .5*(We(i,j,k)-Wo(i,j,k))
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      IF ( mlat==0 ) RETURN
      DO k = 1 , Nt
         DO j = 1 , Nlon
            V(Imid,j,k) = .5*Ve(Imid,j,k)
            W(Imid,j,k) = .5*We(Imid,j,k)
         ENDDO
      ENDDO
      END SUBROUTINE VHSGS1


      SUBROUTINE VHSGSI(Nlat,Nlon,Wvhsgs,Lvhsgs,Dwork,Ldwork,Ierror)
      IMPLICIT NONE
      INTEGER Ierror , imid , iw1 , iw2 , iw3 , iw4 , jw1 , jw2 , jw3 , &
            & Ldwork , lmn , Lvhsgs , Nlat , Nlon
      REAL Wvhsgs
!
!     subroutine vhsfsi computes the gaussian points theta, gauss
!     weights wts, and the components vb and wb of the vector
!     harmonics. all quantities are computed internally in double
!     precision but returned in single precision and are therfore
!     accurate to single precision.
!
!     set imid = (nlat+1)/2 and lmn=(nlat*(nlat+1))/2 then
!     wvhsgs must have 2*(imid*lmn+nlat)+nlon+15 locations
!
!     double precision array dwork must have
!       3*nlat*(nlat+1)+5*nlat+1 = nlat*(3*nlat+8)+1
!     locations which is determined by the size of dthet,
!     dwts, dwork, and dpbar in vhsgs1
!
      DIMENSION Wvhsgs(*)
      DOUBLE PRECISION Dwork(*)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      imid = (Nlat+1)/2
      lmn = (Nlat*(Nlat+1))/2
      IF ( Lvhsgs<2*(imid*lmn)+Nlon+15 ) RETURN
      Ierror = 4
      IF ( Ldwork<(Nlat*3*(Nlat+3)+2)/2 ) RETURN
      Ierror = 0
!
!     set saved work space pointers
!
      jw1 = 1
      jw2 = jw1 + imid*lmn
      jw3 = jw2 + imid*lmn
!
!     set unsaved work space pointers
!
      iw1 = 1
      iw2 = iw1 + Nlat
      iw3 = iw2 + Nlat
      iw4 = iw3 + 3*imid*Nlat
!     iw2 = iw1+nlat+nlat
!     iw3 = iw2+nlat+nlat
!     iw4 = iw3+6*imid*nlat
      CALL VHGSI1(Nlat,imid,Wvhsgs(jw1),Wvhsgs(jw2),Dwork(iw1),         &
                & Dwork(iw2),Dwork(iw3),Dwork(iw4))
      CALL HRFFTI(Nlon,Wvhsgs(jw3))
      END SUBROUTINE VHSGSI


      SUBROUTINE VHGSI1(Nlat,Imid,Vb,Wb,Dthet,Dwts,Dpbar,Work)
      IMPLICIT NONE
      INTEGER i , id , ierror , Imid , INDX , ix , iy , lwk , m , mn ,  &
            & n , Nlat , nm , np , nz
      REAL Vb , Wb
      DIMENSION Vb(Imid,*) , Wb(Imid,*)
      DOUBLE PRECISION abel , bbel , cbel , ssqr2 , dcf
      DOUBLE PRECISION Dthet(*) , Dwts(*) , Dpbar(Imid,Nlat,3) , Work(*)
!
!     compute gauss points and weights
!     use dpbar (length 3*nnlat*(nnlat+1)) as work space for gaqd
!
      lwk = Nlat*(Nlat+2)
      CALL GAQD(Nlat,Dthet,Dwts,Dpbar,lwk,ierror)
!
!     compute associated legendre functions
!
!     compute m=n=0 legendre polynomials for all theta(i)
!
      ssqr2 = 1./DSQRT(2.D0)
      DO i = 1 , Imid
         Dpbar(i,1,1) = ssqr2
         Vb(i,1) = 0.
         Wb(i,1) = 0.
      ENDDO
!
!     main loop for remaining vb, and wb
!
      DO n = 1 , Nlat - 1
         nm = MOD(n-2,3) + 1
         nz = MOD(n-1,3) + 1
         np = MOD(n,3) + 1
!
!     compute dpbar for m=0
!
         CALL DNLFK(0,n,Work)
         mn = INDX(0,n,Nlat)
         DO i = 1 , Imid
            CALL DNLFT(0,n,Dthet(i),Work,Dpbar(i,1,np))
!      pbar(i,mn) = dpbar(i,1,np)
         ENDDO
!
!     compute dpbar for m=1
!
         CALL DNLFK(1,n,Work)
         mn = INDX(1,n,Nlat)
         DO i = 1 , Imid
            CALL DNLFT(1,n,Dthet(i),Work,Dpbar(i,2,np))
!      pbar(i,mn) = dpbar(i,2,np)
         ENDDO
!
!     compute and store dpbar for m=2,n
!
         IF ( n>=2 ) THEN
            DO m = 2 , n
               abel = DSQRT(DBLE(FLOAT((2*n+1)*(m+n-2)*(m+n-3)))        &
                    & /DBLE(FLOAT((2*n-3)*(m+n-1)*(m+n))))
               bbel = DSQRT(DBLE(FLOAT((2*n+1)*(n-m-1)*(n-m)))          &
                    & /DBLE(FLOAT((2*n-3)*(m+n-1)*(m+n))))
               cbel = DSQRT(DBLE(FLOAT((n-m+1)*(n-m+2)))                &
                    & /DBLE(FLOAT((m+n-1)*(m+n))))
               id = INDX(m,n,Nlat)
               IF ( m>=n-1 ) THEN
                  DO i = 1 , Imid
                     Dpbar(i,m+1,np) = abel*Dpbar(i,m-1,nm)             &
                                     & - cbel*Dpbar(i,m-1,np)
!      pbar(i,id) = dpbar(i,m+1,np)
                  ENDDO
               ELSE
                  DO i = 1 , Imid
                     Dpbar(i,m+1,np) = abel*Dpbar(i,m-1,nm)             &
                                     & + bbel*Dpbar(i,m+1,nm)           &
                                     & - cbel*Dpbar(i,m-1,np)
!      pbar(i,id) = dpbar(i,m+1,np)
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
!
!     compute the derivative of the functions
!
         ix = INDX(0,n,Nlat)
         iy = INDX(n,n,Nlat)
         DO i = 1 , Imid
            Vb(i,ix) = -Dpbar(i,2,np)
            Vb(i,iy) = Dpbar(i,n,np)/DSQRT(DBLE(FLOAT(2*(n+1))))
         ENDDO
!
         IF ( n/=1 ) THEN
            dcf = DSQRT(DBLE(FLOAT(4*n*(n+1))))
            DO m = 1 , n - 1
               ix = INDX(m,n,Nlat)
               abel = DSQRT(DBLE(FLOAT((n+m)*(n-m+1))))/dcf
               bbel = DSQRT(DBLE(FLOAT((n-m)*(n+m+1))))/dcf
               DO i = 1 , Imid
                  Vb(i,ix) = abel*Dpbar(i,m,np) - bbel*Dpbar(i,m+2,np)
               ENDDO
            ENDDO
         ENDIF
!
!     compute the vector harmonic w(theta) = m*pbar/cos(theta)
!
!     set wb=0 for m=0
!
         ix = INDX(0,n,Nlat)
         DO i = 1 , Imid
            Wb(i,ix) = 0.D0
         ENDDO
!
!     compute wb for m=1,n
!
         dcf = DSQRT(DBLE(FLOAT(n+n+1))/DBLE(FLOAT(4*n*(n+1)*(n+n-1))))
         DO m = 1 , n
            ix = INDX(m,n,Nlat)
            abel = dcf*DSQRT(DBLE(FLOAT((n+m)*(n+m-1))))
            bbel = dcf*DSQRT(DBLE(FLOAT((n-m)*(n-m-1))))
            IF ( m>=n-1 ) THEN
               DO i = 1 , Imid
                  Wb(i,ix) = abel*Dpbar(i,m,nz)
               ENDDO
            ELSE
               DO i = 1 , Imid
                  Wb(i,ix) = abel*Dpbar(i,m,nz) + bbel*Dpbar(i,m+2,nz)
               ENDDO
            ENDIF
         ENDDO
      ENDDO
      END SUBROUTINE VHGSI1


      SUBROUTINE DVHSGS(Nlat,Nlon,Ityp,Nt,V,W,Idvw,Jdvw,Br,Bi,Cr,Ci,Mdab,&
                     & Ndab,Wvhsgs,Lvhsgs,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Br , Ci , Cr , V , W , Work , Wvhsgs
      INTEGER idv , Idvw , idz , Ierror , imid , ist , Ityp , iw1 ,     &
            & iw2 , iw3 , iw4 , Jdvw , jw1 , jw2 , jw3 , lmn , lnl ,    &
            & Lvhsgs , Lwork , lzimn
      INTEGER Mdab , mmax , Ndab , Nlat , Nlon , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Work(1) , Wvhsgs(1)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      IF ( Ityp<0 .OR. Ityp>8 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Ityp<=2 .AND. Idvw<Nlat) .OR. (Ityp>2 .AND. Idvw<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdab<mmax ) RETURN
      Ierror = 8
      IF ( Ndab<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
      IF ( Lvhsgs<lzimn+lzimn+Nlon+15 ) RETURN
      Ierror = 10
      idv = Nlat
      IF ( Ityp>2 ) idv = imid
      lnl = Nt*idv*Nlon
      IF ( Lwork<lnl+lnl+idv*Nlon ) RETURN
      Ierror = 0
      ist = 0
      IF ( Ityp<=2 ) ist = imid
!
!     set wvhsgs pointers
!
      lmn = Nlat*(Nlat+1)/2
      jw1 = 1
      jw2 = jw1 + imid*lmn
      jw3 = jw2 + imid*lmn
!
!     set work pointers
!
      iw1 = ist + 1
      iw2 = lnl + 1
      iw3 = iw2 + ist
      iw4 = iw2 + lnl
 
      CALL DVHSGS1(Nlat,Nlon,Ityp,Nt,imid,Idvw,Jdvw,V,W,Mdab,Ndab,Br,Bi, &
                & Cr,Ci,idv,Work,Work(iw1),Work(iw2),Work(iw3),Work(iw4)&
                & ,idz,Wvhsgs(jw1),Wvhsgs(jw2),Wvhsgs(jw3))
    END SUBROUTINE DVHSGS

      
      SUBROUTINE DVHSGS1(Nlat,Nlon,Ityp,Nt,Imid,Idvw,Jdvw,V,W,Mdab,Ndab, &
                      & Br,Bi,Cr,Ci,Idv,Ve,Vo,We,Wo,Work,Idz,Vb,Wb,     &
                      & Wrfft)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Br , Ci , Cr , V , Vb , Ve , Vo , W , Wb , We , Wo ,    &
         & Work , Wrfft
      INTEGER i , Idv , Idvw , Idz , Imid , imm1 , Ityp , itypp , j ,   &
            & Jdvw , k , m , mb , Mdab , mlat , mlon , mmax , mn , mp1 ,&
            & mp2
      INTEGER Ndab , ndo1 , ndo2 , Nlat , Nlon , nlp1 , np1 , Nt
      DIMENSION V(Idvw,Jdvw,1) , W(Idvw,Jdvw,1) , Br(Mdab,Ndab,1) ,     &
              & Bi(Mdab,Ndab,1) , Cr(Mdab,Ndab,1) , Ci(Mdab,Ndab,1) ,   &
              & Ve(Idv,Nlon,1) , Vo(Idv,Nlon,1) , We(Idv,Nlon,1) ,      &
              & Wo(Idv,Nlon,1) , Work(1) , Wrfft(1) , Vb(Imid,1) ,      &
              & Wb(Imid,1)
      nlp1 = Nlat + 1
      mlat = MOD(Nlat,2)
      mlon = MOD(Nlon,2)
      mmax = MIN0(Nlat,(Nlon+1)/2)
      imm1 = Imid
      IF ( mlat/=0 ) imm1 = Imid - 1
      DO k = 1 , Nt
         DO j = 1 , Nlon
            DO i = 1 , Idv
               Ve(i,j,k) = 0.
               We(i,j,k) = 0.
            ENDDO
         ENDDO
      ENDDO
      ndo1 = Nlat
      ndo2 = Nlat
      IF ( mlat/=0 ) ndo1 = Nlat - 1
      IF ( mlat==0 ) ndo2 = Nlat - 1
      itypp = Ityp + 1
      IF ( itypp==2 ) THEN
!
!     case ityp=1   no symmetries,  cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==3 ) THEN
!
!     case ityp=2   no symmetries,  br and bi are equal to zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==4 ) THEN
!
!     case ityp=3   v even,  w odd
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==5 ) THEN
!
!     case ityp=4   v even,  w odd, and both cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==6 ) THEN
!
!     case ityp=5   v even,  w odd,     br and bi equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==7 ) THEN
!
!     case ityp=6   v odd  ,  w even
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==8 ) THEN
!
!     case ityp=7   v odd, w even   cr and ci equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSEIF ( itypp==9 ) THEN
!
!     case ityp=8   v odd,  w even   br and bi equal zero
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ELSE
!
!     case ityp=0   no symmetries
!
!     case m = 0
!
         DO k = 1 , Nt
            DO np1 = 2 , ndo2 , 2
               DO i = 1 , Imid
                  Ve(i,1,k) = Ve(i,1,k) + Br(1,np1,k)*Vb(i,np1)
                  We(i,1,k) = We(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
         DO k = 1 , Nt
            DO np1 = 3 , ndo1 , 2
               DO i = 1 , imm1
                  Vo(i,1,k) = Vo(i,1,k) + Br(1,np1,k)*Vb(i,np1)
                  Wo(i,1,k) = Wo(i,1,k) - Cr(1,np1,k)*Vb(i,np1)
               ENDDO
            ENDDO
         ENDDO
!
!     case m = 1 through nlat-1
!
         IF ( mmax>=2 ) THEN
            DO mp1 = 2 , mmax
               m = mp1 - 1
!     mb = m*(nlat-1)-(m*(m-1))/2
               mb = m*Nlat - (m*(m+1))/2
               mp2 = mp1 + 1
               IF ( mp1<=ndo1 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp1 , ndo1 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & - Ci(mp1,np1,k)*Wb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Cr(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Bi(mp1,np1,k)*Wb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & + Br(mp1,np1,k)*Wb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
               IF ( mp2<=ndo2 ) THEN
                  DO k = 1 , Nt
                     DO np1 = mp2 , ndo2 , 2
                        mn = mb + np1
                        DO i = 1 , imm1
                           Ve(i,2*mp1-2,k) = Ve(i,2*mp1-2,k)            &
                            & + Br(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-2,k) = Vo(i,2*mp1-2,k)            &
                            & - Ci(mp1,np1,k)*Wb(i,mn)
                           Ve(i,2*mp1-1,k) = Ve(i,2*mp1-1,k)            &
                            & + Bi(mp1,np1,k)*Vb(i,mn)
                           Vo(i,2*mp1-1,k) = Vo(i,2*mp1-1,k)            &
                            & + Cr(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-2,k) = We(i,2*mp1-2,k)            &
                            & - Cr(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-2,k) = Wo(i,2*mp1-2,k)            &
                            & - Bi(mp1,np1,k)*Wb(i,mn)
                           We(i,2*mp1-1,k) = We(i,2*mp1-1,k)            &
                            & - Ci(mp1,np1,k)*Vb(i,mn)
                           Wo(i,2*mp1-1,k) = Wo(i,2*mp1-1,k)            &
                            & + Br(mp1,np1,k)*Wb(i,mn)
                        ENDDO
                        IF ( mlat/=0 ) THEN
                           Ve(Imid,2*mp1-2,k) = Ve(Imid,2*mp1-2,k)      &
                            & + Br(mp1,np1,k)*Vb(Imid,mn)
                           Ve(Imid,2*mp1-1,k) = Ve(Imid,2*mp1-1,k)      &
                            & + Bi(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-2,k) = We(Imid,2*mp1-2,k)      &
                            & - Cr(mp1,np1,k)*Vb(Imid,mn)
                           We(Imid,2*mp1-1,k) = We(Imid,2*mp1-1,k)      &
                            & - Ci(mp1,np1,k)*Vb(Imid,mn)
                        ENDIF
                     ENDDO
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
      ENDIF
      DO k = 1 , Nt
         CALL HRFFTB(Idv,Nlon,Ve(1,1,k),Idv,Wrfft,Work)
         CALL HRFFTB(Idv,Nlon,We(1,1,k),Idv,Wrfft,Work)
      ENDDO
      IF ( Ityp>2 ) THEN
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , imm1
                  V(i,j,k) = .5*Ve(i,j,k)
                  W(i,j,k) = .5*We(i,j,k)
               ENDDO
            ENDDO
         ENDDO
      ELSE
         DO k = 1 , Nt
            DO j = 1 , Nlon
               DO i = 1 , imm1
                  V(i,j,k) = .5*(Ve(i,j,k)+Vo(i,j,k))
                  W(i,j,k) = .5*(We(i,j,k)+Wo(i,j,k))
                  V(nlp1-i,j,k) = .5*(Ve(i,j,k)-Vo(i,j,k))
                  W(nlp1-i,j,k) = .5*(We(i,j,k)-Wo(i,j,k))
               ENDDO
            ENDDO
         ENDDO
      ENDIF
      IF ( mlat==0 ) RETURN
      DO k = 1 , Nt
         DO j = 1 , Nlon
            V(Imid,j,k) = .5*Ve(Imid,j,k)
            W(Imid,j,k) = .5*We(Imid,j,k)
         ENDDO
      ENDDO
      END SUBROUTINE DVHSGS1


      SUBROUTINE DVHSGSI(Nlat,Nlon,Wvhsgs,Lvhsgs,Dwork,Ldwork,Ierror)
      IMPLICIT NONE
      INTEGER Ierror , imid , iw1 , iw2 , iw3 , iw4 , jw1 , jw2 , jw3 , &
            & Ldwork , lmn , Lvhsgs , Nlat , Nlon
      DOUBLE PRECISION Wvhsgs
!
!     subroutine vhsfsi computes the gaussian points theta, gauss
!     weights wts, and the components vb and wb of the vector
!     harmonics. all quantities are computed internally in double
!     precision but returned in single precision and are therfore
!     accurate to single precision.
!
!     set imid = (nlat+1)/2 and lmn=(nlat*(nlat+1))/2 then
!     wvhsgs must have 2*(imid*lmn+nlat)+nlon+15 locations
!
!     double precision array dwork must have
!       3*nlat*(nlat+1)+5*nlat+1 = nlat*(3*nlat+8)+1
!     locations which is determined by the size of dthet,
!     dwts, dwork, and dpbar in vhsgs1
!
      DIMENSION Wvhsgs(*)
      DOUBLE PRECISION Dwork(*)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      imid = (Nlat+1)/2
      lmn = (Nlat*(Nlat+1))/2
      IF ( Lvhsgs<2*(imid*lmn)+Nlon+15 ) RETURN
      Ierror = 4
      IF ( Ldwork<(Nlat*3*(Nlat+3)+2)/2 ) RETURN
      Ierror = 0
!
!     set saved work space pointers
!
      jw1 = 1
      jw2 = jw1 + imid*lmn
      jw3 = jw2 + imid*lmn
!
!     set unsaved work space pointers
!
      iw1 = 1
      iw2 = iw1 + Nlat
      iw3 = iw2 + Nlat
      iw4 = iw3 + 3*imid*Nlat
!     iw2 = iw1+nlat+nlat
!     iw3 = iw2+nlat+nlat
!     iw4 = iw3+6*imid*nlat
      CALL DVHGSI1(Nlat,imid,Wvhsgs(jw1),Wvhsgs(jw2),Dwork(iw1),         &
                & Dwork(iw2),Dwork(iw3),Dwork(iw4))
      CALL DHRFFTI(Nlon,Wvhsgs(jw3))
      END SUBROUTINE DVHSGSI


      SUBROUTINE DVHGSI1(Nlat,Imid,Vb,Wb,Dthet,Dwts,Dpbar,Work)
      IMPLICIT NONE
      INTEGER i , id , ierror , Imid , INDX , ix , iy , lwk , m , mn ,  &
            & n , Nlat , nm , np , nz
      DOUBLE PRECISION Vb , Wb
      DIMENSION Vb(Imid,*) , Wb(Imid,*)
      DOUBLE PRECISION abel , bbel , cbel , ssqr2 , dcf
      DOUBLE PRECISION Dthet(*) , Dwts(*) , Dpbar(Imid,Nlat,3) , Work(*)
!
!     compute gauss points and weights
!     use dpbar (length 3*nnlat*(nnlat+1)) as work space for gaqd
!
      lwk = Nlat*(Nlat+2)
      CALL GAQD(Nlat,Dthet,Dwts,Dpbar,lwk,ierror)
!
!     compute associated legendre functions
!
!     compute m=n=0 legendre polynomials for all theta(i)
!
      ssqr2 = 1./DSQRT(2.D0)
      DO i = 1 , Imid
         Dpbar(i,1,1) = ssqr2
         Vb(i,1) = 0.
         Wb(i,1) = 0.
      ENDDO
!
!     main loop for remaining vb, and wb
!
      DO n = 1 , Nlat - 1
         nm = MOD(n-2,3) + 1
         nz = MOD(n-1,3) + 1
         np = MOD(n,3) + 1
!
!     compute dpbar for m=0
!
         CALL DNLFK(0,n,Work)
         mn = INDX(0,n,Nlat)
         DO i = 1 , Imid
            CALL DNLFT(0,n,Dthet(i),Work,Dpbar(i,1,np))
!      pbar(i,mn) = dpbar(i,1,np)
         ENDDO
!
!     compute dpbar for m=1
!
         CALL DNLFK(1,n,Work)
         mn = INDX(1,n,Nlat)
         DO i = 1 , Imid
            CALL DNLFT(1,n,Dthet(i),Work,Dpbar(i,2,np))
!      pbar(i,mn) = dpbar(i,2,np)
         ENDDO
!
!     compute and store dpbar for m=2,n
!
         IF ( n>=2 ) THEN
            DO m = 2 , n
               abel = DSQRT(DBLE(FLOAT((2*n+1)*(m+n-2)*(m+n-3)))        &
                    & /DBLE(FLOAT((2*n-3)*(m+n-1)*(m+n))))
               bbel = DSQRT(DBLE(FLOAT((2*n+1)*(n-m-1)*(n-m)))          &
                    & /DBLE(FLOAT((2*n-3)*(m+n-1)*(m+n))))
               cbel = DSQRT(DBLE(FLOAT((n-m+1)*(n-m+2)))                &
                    & /DBLE(FLOAT((m+n-1)*(m+n))))
               id = INDX(m,n,Nlat)
               IF ( m>=n-1 ) THEN
                  DO i = 1 , Imid
                     Dpbar(i,m+1,np) = abel*Dpbar(i,m-1,nm)             &
                                     & - cbel*Dpbar(i,m-1,np)
!      pbar(i,id) = dpbar(i,m+1,np)
                  ENDDO
               ELSE
                  DO i = 1 , Imid
                     Dpbar(i,m+1,np) = abel*Dpbar(i,m-1,nm)             &
                                     & + bbel*Dpbar(i,m+1,nm)           &
                                     & - cbel*Dpbar(i,m-1,np)
!      pbar(i,id) = dpbar(i,m+1,np)
                  ENDDO
               ENDIF
            ENDDO
         ENDIF
!
!     compute the derivative of the functions
!
         ix = INDX(0,n,Nlat)
         iy = INDX(n,n,Nlat)
         DO i = 1 , Imid
            Vb(i,ix) = -Dpbar(i,2,np)
            Vb(i,iy) = Dpbar(i,n,np)/DSQRT(DBLE(FLOAT(2*(n+1))))
         ENDDO
!
         IF ( n/=1 ) THEN
            dcf = DSQRT(DBLE(FLOAT(4*n*(n+1))))
            DO m = 1 , n - 1
               ix = INDX(m,n,Nlat)
               abel = DSQRT(DBLE(FLOAT((n+m)*(n-m+1))))/dcf
               bbel = DSQRT(DBLE(FLOAT((n-m)*(n+m+1))))/dcf
               DO i = 1 , Imid
                  Vb(i,ix) = abel*Dpbar(i,m,np) - bbel*Dpbar(i,m+2,np)
               ENDDO
            ENDDO
         ENDIF
!
!     compute the vector harmonic w(theta) = m*pbar/cos(theta)
!
!     set wb=0 for m=0
!
         ix = INDX(0,n,Nlat)
         DO i = 1 , Imid
            Wb(i,ix) = 0.D0
         ENDDO
!
!     compute wb for m=1,n
!
         dcf = DSQRT(DBLE(FLOAT(n+n+1))/DBLE(FLOAT(4*n*(n+1)*(n+n-1))))
         DO m = 1 , n
            ix = INDX(m,n,Nlat)
            abel = dcf*DSQRT(DBLE(FLOAT((n+m)*(n+m-1))))
            bbel = dcf*DSQRT(DBLE(FLOAT((n-m)*(n-m-1))))
            IF ( m>=n-1 ) THEN
               DO i = 1 , Imid
                  Wb(i,ix) = abel*Dpbar(i,m,nz)
               ENDDO
            ELSE
               DO i = 1 , Imid
                  Wb(i,ix) = abel*Dpbar(i,m,nz) + bbel*Dpbar(i,m+2,nz)
               ENDDO
            ENDIF
         ENDDO
      ENDDO
      END SUBROUTINE DVHGSI1
