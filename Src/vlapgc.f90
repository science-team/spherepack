!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
! ... file vlapgc.f
!
!     this file includes documentation and code for
!     subroutine vlapgc          i
!
! ... files which must be loaded with vlapgc.f
!
!     sphcom.f, hrfft.f, vhagc.f, vhsgc.f, gaqd.f
!
!
!     subroutine vlapgc(nlat,nlon,ityp,nt,vlap,wlap,idvw,jdvw,br,bi,cr,ci,
!    +mdbc,ndbc,wvhsgc,lvhsgc,work,lwork,ierror)
!
!     given the vector spherical harmonic coefficients (br,bi,cr,ci)
!     precomputed by subroutine vhagc for a vector field (v,w), subroutine
!     vlapgc computes the vector laplacian of the vector field (v,w)
!     in (vlap,wlap) (see the definition of the vector laplacian at
!     the output parameter description of vlap,wlap below).  w and wlap
!     are east longitudinal components of the vectors.  v and vlap are
!     colatitudinal components of the vectors.  the laplacian components
!     in (vlap,wlap) have the same symmetry or lack of symmetry about the
!     equator as (v,w).  the input parameters ityp,nt,mdbc,nbdc must have
!     the same values used by vhagc to compute br,bi,cr, and ci for (v,w).
!     vlap(i,j) and wlap(i,j) are given on the sphere at the gaussian
!     colatitude theta(i) (see nlat as input parameter) and east longitude
!     lambda(j) = (j-1)*2*pi/nlon for i=1,...,nlat and j=1,...,nlon.
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are computed
!            in radians in theta(1) <...< theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid point
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct longitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!     ityp   this parameter should have the same value input to subroutine
!            vhagc to compute the coefficients br,bi,cr, and ci for the
!            vector field (v,w).  ityp is set as follows:
!
!            = 0  no symmetries exist in (v,w) about the equator. (vlap,wlap)
!                 is computed and stored on the entire sphere in the arrays
!                 vlap(i,j) and wlap(i,j) for i=1,...,nlat and j=1,...,nlon.
!
!
!            = 1  no symmetries exist in (v,w) about the equator. (vlap,wlap)
!                 is computed and stored on the entire sphere in the arrays
!                 vlap(i,j) and wlap(i,j) for i=1,...,nlat and j=1,...,nlon.
!                 the vorticity of (v,w) is zero so the coefficients cr and
!                 ci are zero and are not used.  the vorticity of (vlap,wlap)
!                 is also zero.
!
!
!            = 2  no symmetries exist in (v,w) about the equator. (vlap,wlap)
!                 is computed and stored on the entire sphere in the arrays
!                 vlap(i,j) and wlap(i,j) for i=1,...,nlat and j=1,...,nlon.
!                 the divergence of (v,w) is zero so the coefficients br and
!                 bi are zero and are not used.  the divergence of (vlap,wlap)
!                 is also zero.
!
!            = 3  w is antisymmetric and v is symmetric about the equator.
!                 consequently wlap is antisymmetric and vlap is symmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 4  w is antisymmetric and v is symmetric about the equator.
!                 consequently wlap is antisymmetric and vlap is symmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.  the
!                 vorticity of (v,w) is zero so the coefficients cr,ci are
!                 zero and are not used. the vorticity of (vlap,wlap) is
!                 also zero.
!
!            = 5  w is antisymmetric and v is symmetric about the equator.
!                 consequently wlap is antisymmetric and vlap is symmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.  the
!                 divergence of (v,w) is zero so the coefficients br,bi
!                 are zero and are not used. the divergence of (vlap,wlap)
!                 is also zero.
!
!
!            = 6  w is symmetric and v is antisymmetric about the equator.
!                 consequently wlap is symmetric and vlap is antisymmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 7  w is symmetric and v is antisymmetric about the equator.
!                 consequently wlap is symmetric and vlap is antisymmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.  the
!                 vorticity of (v,w) is zero so the coefficients cr,ci are
!                 zero and are not used. the vorticity of (vlap,wlap) is
!                 also zero.
!
!            = 8  w is symmetric and v is antisymmetric about the equator.
!                 consequently wlap is symmetric and vlap is antisymmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.  the
!                 divergence of (v,w) is zero so the coefficients br,bi
!                 are zero and are not used. the divergence of (vlap,wlap)
!                 is also zero.
!
!
!     nt     nt is the number of vector fields (v,w).  some computational
!            efficiency is obtained for multiple fields.  in the program
!            that calls vlapgc, the arrays vlap,wlap,br,bi,cr and ci
!            can be three dimensional corresponding to an indexed multiple
!            vector field.  in this case multiple vector synthesis will
!            be performed to compute the vector laplacian for each field.
!            the third index is the synthesis index which assumes the values
!            k=1,...,nt.  for a single synthesis set nt=1.  the description
!            of the remaining parameters is simplified by assuming that nt=1
!            or that all arrays are two dimensional.
!
!   idvw     the first dimension of the arrays vlap and wlap as it appears
!            in the program that calls vlapgc.  if ityp=0,1, or 2  then idvw
!            must be at least nlat.  if ityp > 2 and nlat is even then idvw
!            must be at least nlat/2. if ityp > 2 and nlat is odd then idvw
!            must be at least (nlat+1)/2.
!
!   jdvw     the second dimension of the arrays vlap and wlap as it appears
!            in the program that calls vlapgc. jdvw must be at least nlon.
!
!
!   br,bi    two or three dimensional arrays (see input parameter nt)
!   cr,ci    that contain vector spherical harmonic coefficients
!            of the vector field (v,w) as computed by subroutine vhagc.
!            br,bi,cr and ci must be computed by vhagc prior to calling
!            vlapgc.  if ityp=1,4, or 7 then cr,ci are not used and can
!            be dummy arguments.  if ityp=2,5, or 8 then br,bi are not
!            used and can be dummy arguments.
!
!    mdbc    the first dimension of the arrays br,bi,cr and ci as it
!            appears in the program that calls vlapgc.  mdbc must be
!            at least min0(nlat,nlon/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!    ndbc    the second dimension of the arrays br,bi,cr and ci as it
!            appears in the program that calls vlapgc. ndbc must be at
!            least nlat.
!
!    wvhsgc  an array which must be initialized by subroutine vhsgci.
!            once initialized, wvhsgc
!            can be used repeatedly by vlapgc as long as nlat and nlon
!            remain unchanged.  wvhsgc must not be altered between calls
!            of vlapgc.
!
!    lvhsgc  the dimension of the array wvhsgc as it appears in the
!            program that calls vhagc. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lvhsgc must be at least
!
!               4*nlat*l2+3*max0(l1-2,0)*(2*nlat-l1-1)+nlon+15
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls vlapgc. define
!
!               l2 = nlat/2                    if nlat is even or
!               l2 = (nlat+1)/2                if nlat is odd
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            if ityp .le. 2 then
!
!               nlat*(2*nt*nlon+max0(6*l2,nlon)) + nlat*(4*nt*l1+1)
!
!            or if ityp .gt. 2 let
!
!               l2*(2*nt*nlon+max0(6*nlat,nlon)) + nlat*(4*nt*l1+1)
!
!            will suffice as a minimum length for lwork
!            (see ierror=10 below)
!            (see ierror=10 below)
!
!     **************************************************************
!
!     output parameters
!
!
!    vlap,   two or three dimensional arrays (see input parameter nt) that
!    wlap    contain the vector laplacian of the field (v,w).  wlap(i,j) is
!            the east longitude component and vlap(i,j) is the colatitudinal
!            component of the vector laplacian.  the definition of the
!            vector laplacian follows:
!
!            let cost and sint be the cosine and sine at colatitude theta.
!            let d( )/dlambda  and d( )/dtheta be the first order partial
!            derivatives in longitude and colatitude.  let del2 be the scalar
!            laplacian operator
!
!                 del2(s) = [d(sint*d(s)/dtheta)/dtheta +
!                             2            2
!                            d (s)/dlambda /sint]/sint
!
!            then the vector laplacian opeator
!
!                 dvel2(v,w) = (vlap,wlap)
!
!            is defined by
!
!                 vlap = del2(v) - (2*cost*dw/dlambda + v)/sint**2
!
!                 wlap = del2(w) + (2*cost*dv/dlambda - w)/sint**2
!
!  ierror    a parameter which flags errors in input parameters as follows:
!
!            = 0  no errors detected
!
!            = 1  error in the specification of nlat
!
!            = 2  error in the specification of nlon
!
!            = 3  error in the specification of ityp
!
!            = 4  error in the specification of nt
!
!            = 5  error in the specification of idvw
!
!            = 6  error in the specification of jdvw
!
!            = 7  error in the specification of mdbc
!
!            = 8  error in the specification of ndbc
!
!            = 9  error in the specification of lvhsgc
!
!            = 10 error in the specification of lwork
!
!
! **********************************************************************
!
!     end of documentation for vlapgc
!
! **********************************************************************
!
      SUBROUTINE VLAPGC(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Br,Bi,Cr, &
                      & Ci,Mdbc,Ndbc,Wvhsgc,Lvhsgc,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL Bi , Br , Ci , Cr , Vlap , Wlap , Work , Wvhsgc
      INTEGER ibi , ibr , ici , icr , Idvw , idz , Ierror , ifn , imid ,&
            & Ityp , iwk , Jdvw , l1 , l2 , liwk , Lvhsgc , lwkmin ,    &
            & lwmin , Lwork , lzimn
      INTEGER Mdbc , mmax , mn , Ndbc , Nlat , Nlon , Nt
      DIMENSION Vlap(Idvw,Jdvw,Nt) , Wlap(Idvw,Jdvw,Nt)
      DIMENSION Br(Mdbc,Ndbc,Nt) , Bi(Mdbc,Ndbc,Nt)
      DIMENSION Cr(Mdbc,Ndbc,Nt) , Ci(Mdbc,Ndbc,Nt)
      DIMENSION Wvhsgc(Lvhsgc) , Work(Lwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      IF ( Ityp<0 .OR. Ityp>8 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Ityp<=2 .AND. Idvw<Nlat) .OR. (Ityp>2 .AND. Idvw<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdbc<mmax ) RETURN
      Ierror = 8
      IF ( Ndbc<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
!     lsavmin = lzimn+lzimn+nlon+15
!     if(lsave .lt. lsavmin) return
 
      l1 = MIN0(Nlat,(Nlon+1)/2)
      l2 = (Nlat+1)/2
      lwmin = 4*Nlat*l2 + 3*MAX0(l1-2,0)*(2*Nlat-l1-1) + Nlon + 15
      IF ( Lvhsgc<lwmin ) RETURN
 
!
!     verify unsaved work space length
!
      mn = mmax*Nlat*Nt
      IF ( Ityp<3 ) THEN
!       no symmetry
         IF ( Ityp==0 ) THEN
!       br,bi,cr,ci nonzero
            lwkmin = Nlat*(2*Nt*Nlon+MAX0(6*imid,Nlon)+1) + 4*mn
         ELSE
!       br,bi or cr,ci zero
            lwkmin = Nlat*(2*Nt*Nlon+MAX0(6*imid,Nlon)+1) + 2*mn
         ENDIF
!     symmetry about equator
      ELSEIF ( Ityp==3 .OR. Ityp==6 ) THEN
!       br,bi,cr,ci nonzero
         lwkmin = imid*(2*Nt*Nlon+MAX0(6*Nlat,Nlon)) + 4*mn + Nlat
      ELSE
!       br,bi or cr,ci zero
         lwkmin = imid*(2*Nt*Nlon+MAX0(6*Nlat,Nlon)) + 2*mn + Nlat
      ENDIF
      IF ( Lwork<lwkmin ) RETURN
      Ierror = 0
!
!     set work space pointers for vector laplacian coefficients
!
      IF ( Ityp==0 .OR. Ityp==3 .OR. Ityp==6 ) THEN
         ibr = 1
         ibi = ibr + mn
         icr = ibi + mn
         ici = icr + mn
      ELSEIF ( Ityp==1 .OR. Ityp==4 .OR. Ityp==7 ) THEN
         ibr = 1
         ibi = ibr + mn
         icr = ibi + mn
         ici = icr
      ELSE
         ibr = 1
         ibi = 1
         icr = ibi + mn
         ici = icr + mn
      ENDIF
      ifn = ici + mn
      iwk = ifn + Nlat
      liwk = Lwork - 4*mn - Nlat
      CALL VLAPGC1(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Work(ibr),     &
                 & Work(ibi),Work(icr),Work(ici),mmax,Work(ifn),Mdbc,   &
                 & Ndbc,Br,Bi,Cr,Ci,Wvhsgc,Lvhsgc,Work(iwk),liwk,Ierror)
    END SUBROUTINE VLAPGC
    
      
    SUBROUTINE VLAPGC1(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Brlap,   &
                       & Bilap,Crlap,Cilap,Mmax,Fnn,Mdb,Ndb,Br,Bi,Cr,Ci,&
                       & Wsave,Lwsav,Wk,Lwk,Ierror)
      IMPLICIT NONE
      REAL Bi , Bilap , Br , Brlap , Ci , Cilap , Cr , Crlap , fn ,     &
         & Fnn , Vlap , Wk , Wlap , Wsave
      INTEGER Idvw , Ierror , Ityp , Jdvw , k , Lwk , Lwsav , m , Mdb , &
            & Mmax , n , Ndb , Nlat , Nlon , Nt
      DIMENSION Vlap(Idvw,Jdvw,Nt) , Wlap(Idvw,Jdvw,Nt)
      DIMENSION Fnn(Nlat) , Brlap(Mmax,Nlat,Nt) , Bilap(Mmax,Nlat,Nt)
      DIMENSION Crlap(Mmax,Nlat,Nt) , Cilap(Mmax,Nlat,Nt)
      DIMENSION Br(Mdb,Ndb,Nt) , Bi(Mdb,Ndb,Nt)
      DIMENSION Cr(Mdb,Ndb,Nt) , Ci(Mdb,Ndb,Nt)
      DIMENSION Wsave(Lwsav) , Wk(Lwk)
!
!     preset coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Fnn(n) = -fn*(fn+1.)
      ENDDO
!
!     set laplacian coefficients from br,bi,cr,ci
!
      IF ( Ityp==0 .OR. Ityp==3 .OR. Ityp==6 ) THEN
!
!     all coefficients needed
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Brlap(m,n,k) = 0.0
                  Bilap(m,n,k) = 0.0
                  Crlap(m,n,k) = 0.0
                  Cilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Brlap(1,n,k) = Fnn(n)*Br(1,n,k)
               Bilap(1,n,k) = Fnn(n)*Bi(1,n,k)
               Crlap(1,n,k) = Fnn(n)*Cr(1,n,k)
               Cilap(1,n,k) = Fnn(n)*Ci(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Brlap(m,n,k) = Fnn(n)*Br(m,n,k)
                  Bilap(m,n,k) = Fnn(n)*Bi(m,n,k)
                  Crlap(m,n,k) = Fnn(n)*Cr(m,n,k)
                  Cilap(m,n,k) = Fnn(n)*Ci(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ELSEIF ( Ityp==1 .OR. Ityp==4 .OR. Ityp==7 ) THEN
!
!     vorticity is zero so cr,ci=0 not used
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Brlap(m,n,k) = 0.0
                  Bilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Brlap(1,n,k) = Fnn(n)*Br(1,n,k)
               Bilap(1,n,k) = Fnn(n)*Bi(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Brlap(m,n,k) = Fnn(n)*Br(m,n,k)
                  Bilap(m,n,k) = Fnn(n)*Bi(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ELSE
!
!     divergence is zero so br,bi=0 not used
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Crlap(m,n,k) = 0.0
                  Cilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Crlap(1,n,k) = Fnn(n)*Cr(1,n,k)
               Cilap(1,n,k) = Fnn(n)*Ci(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Crlap(m,n,k) = Fnn(n)*Cr(m,n,k)
                  Cilap(m,n,k) = Fnn(n)*Ci(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ENDIF
!
!     sythesize coefs into vector field (vlap,wlap)
!
      CALL VHSGC(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Brlap,Bilap,     &
               & Crlap,Cilap,Mmax,Nlat,Wsave,Lwsav,Wk,Lwk,Ierror)
      END SUBROUTINE VLAPGC1

      
      SUBROUTINE DVLAPGC(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Br,Bi,Cr, &
                      & Ci,Mdbc,Ndbc,Wvhsgc,Lvhsgc,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Br , Ci , Cr , Vlap , Wlap , Work , Wvhsgc
      INTEGER ibi , ibr , ici , icr , Idvw , idz , Ierror , ifn , imid ,&
            & Ityp , iwk , Jdvw , l1 , l2 , liwk , Lvhsgc , lwkmin ,    &
            & lwmin , Lwork , lzimn
      INTEGER Mdbc , mmax , mn , Ndbc , Nlat , Nlon , Nt
      DIMENSION Vlap(Idvw,Jdvw,Nt) , Wlap(Idvw,Jdvw,Nt)
      DIMENSION Br(Mdbc,Ndbc,Nt) , Bi(Mdbc,Ndbc,Nt)
      DIMENSION Cr(Mdbc,Ndbc,Nt) , Ci(Mdbc,Ndbc,Nt)
      DIMENSION Wvhsgc(Lvhsgc) , Work(Lwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      IF ( Ityp<0 .OR. Ityp>8 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Ityp<=2 .AND. Idvw<Nlat) .OR. (Ityp>2 .AND. Idvw<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdbc<mmax ) RETURN
      Ierror = 8
      IF ( Ndbc<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
!     lsavmin = lzimn+lzimn+nlon+15
!     if(lsave .lt. lsavmin) return
 
      l1 = MIN0(Nlat,(Nlon+1)/2)
      l2 = (Nlat+1)/2
      lwmin = 4*Nlat*l2 + 3*MAX0(l1-2,0)*(2*Nlat-l1-1) + Nlon + 15
      IF ( Lvhsgc<lwmin ) RETURN
 
!
!     verify unsaved work space length
!
      mn = mmax*Nlat*Nt
      IF ( Ityp<3 ) THEN
!       no symmetry
         IF ( Ityp==0 ) THEN
!       br,bi,cr,ci nonzero
            lwkmin = Nlat*(2*Nt*Nlon+MAX0(6*imid,Nlon)+1) + 4*mn
         ELSE
!       br,bi or cr,ci zero
            lwkmin = Nlat*(2*Nt*Nlon+MAX0(6*imid,Nlon)+1) + 2*mn
         ENDIF
!     symmetry about equator
      ELSEIF ( Ityp==3 .OR. Ityp==6 ) THEN
!       br,bi,cr,ci nonzero
         lwkmin = imid*(2*Nt*Nlon+MAX0(6*Nlat,Nlon)) + 4*mn + Nlat
      ELSE
!       br,bi or cr,ci zero
         lwkmin = imid*(2*Nt*Nlon+MAX0(6*Nlat,Nlon)) + 2*mn + Nlat
      ENDIF
      IF ( Lwork<lwkmin ) RETURN
      Ierror = 0
!
!     set work space pointers for vector laplacian coefficients
!
      IF ( Ityp==0 .OR. Ityp==3 .OR. Ityp==6 ) THEN
         ibr = 1
         ibi = ibr + mn
         icr = ibi + mn
         ici = icr + mn
      ELSEIF ( Ityp==1 .OR. Ityp==4 .OR. Ityp==7 ) THEN
         ibr = 1
         ibi = ibr + mn
         icr = ibi + mn
         ici = icr
      ELSE
         ibr = 1
         ibi = 1
         icr = ibi + mn
         ici = icr + mn
      ENDIF
      ifn = ici + mn
      iwk = ifn + Nlat
      liwk = Lwork - 4*mn - Nlat
      CALL DVLAPGC1(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Work(ibr),     &
                 & Work(ibi),Work(icr),Work(ici),mmax,Work(ifn),Mdbc,   &
                 & Ndbc,Br,Bi,Cr,Ci,Wvhsgc,Lvhsgc,Work(iwk),liwk,Ierror)
    END SUBROUTINE DVLAPGC
    
      
    SUBROUTINE DVLAPGC1(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Brlap,   &
                       & Bilap,Crlap,Cilap,Mmax,Fnn,Mdb,Ndb,Br,Bi,Cr,Ci,&
                       & Wsave,Lwsav,Wk,Lwk,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Bilap , Br , Brlap , Ci , Cilap , Cr , Crlap , fn ,     &
         & Fnn , Vlap , Wk , Wlap , Wsave
      INTEGER Idvw , Ierror , Ityp , Jdvw , k , Lwk , Lwsav , m , Mdb , &
            & Mmax , n , Ndb , Nlat , Nlon , Nt
      DIMENSION Vlap(Idvw,Jdvw,Nt) , Wlap(Idvw,Jdvw,Nt)
      DIMENSION Fnn(Nlat) , Brlap(Mmax,Nlat,Nt) , Bilap(Mmax,Nlat,Nt)
      DIMENSION Crlap(Mmax,Nlat,Nt) , Cilap(Mmax,Nlat,Nt)
      DIMENSION Br(Mdb,Ndb,Nt) , Bi(Mdb,Ndb,Nt)
      DIMENSION Cr(Mdb,Ndb,Nt) , Ci(Mdb,Ndb,Nt)
      DIMENSION Wsave(Lwsav) , Wk(Lwk)
!
!     preset coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Fnn(n) = -fn*(fn+1.)
      ENDDO
!
!     set laplacian coefficients from br,bi,cr,ci
!
      IF ( Ityp==0 .OR. Ityp==3 .OR. Ityp==6 ) THEN
!
!     all coefficients needed
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Brlap(m,n,k) = 0.0
                  Bilap(m,n,k) = 0.0
                  Crlap(m,n,k) = 0.0
                  Cilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Brlap(1,n,k) = Fnn(n)*Br(1,n,k)
               Bilap(1,n,k) = Fnn(n)*Bi(1,n,k)
               Crlap(1,n,k) = Fnn(n)*Cr(1,n,k)
               Cilap(1,n,k) = Fnn(n)*Ci(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Brlap(m,n,k) = Fnn(n)*Br(m,n,k)
                  Bilap(m,n,k) = Fnn(n)*Bi(m,n,k)
                  Crlap(m,n,k) = Fnn(n)*Cr(m,n,k)
                  Cilap(m,n,k) = Fnn(n)*Ci(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ELSEIF ( Ityp==1 .OR. Ityp==4 .OR. Ityp==7 ) THEN
!
!     vorticity is zero so cr,ci=0 not used
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Brlap(m,n,k) = 0.0
                  Bilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Brlap(1,n,k) = Fnn(n)*Br(1,n,k)
               Bilap(1,n,k) = Fnn(n)*Bi(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Brlap(m,n,k) = Fnn(n)*Br(m,n,k)
                  Bilap(m,n,k) = Fnn(n)*Bi(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ELSE
!
!     divergence is zero so br,bi=0 not used
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Crlap(m,n,k) = 0.0
                  Cilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Crlap(1,n,k) = Fnn(n)*Cr(1,n,k)
               Cilap(1,n,k) = Fnn(n)*Ci(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Crlap(m,n,k) = Fnn(n)*Cr(m,n,k)
                  Cilap(m,n,k) = Fnn(n)*Ci(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ENDIF
!
!     sythesize coefs into vector field (vlap,wlap)
!
      CALL DVHSGC(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Brlap,Bilap,     &
               & Crlap,Cilap,Mmax,Nlat,Wsave,Lwsav,Wk,Lwk,Ierror)
      END SUBROUTINE DVLAPGC1
