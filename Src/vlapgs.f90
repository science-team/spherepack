!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
! ... file vlapgs.f
!
!     this file includes documentation and code for
!     subroutine vlapgs          i
!
! ... files which must be loaded with vlapgs.f
!
!     sphcom.f, hrfft.f, vhags.f, vhsgs.f, gaqd.f
!
!
!
!     subroutine vlapgs(nlat,nlon,ityp,nt,vlap,wlap,idvw,jdvw,br,bi,cr,ci,
!    +mdbc,ndbc,wvhsgs,lvhsgs,work,lwork,ierror)
!
!     given the vector spherical harmonic coefficients (br,bi,cr,ci)
!     precomputed by subroutine vhags for a vector field (v,w), subroutine
!     vlapgs computes the vector laplacian of the vector field (v,w)
!     in (vlap,wlap) (see the definition of the vector laplacian at
!     the output parameter description of vlap,wlap below).  w and wlap
!     are east longitudinal components of the vectors.  v and vlap are
!     colatitudinal components of the vectors.  the laplacian components
!     in (vlap,wlap) have the same symmetry or lack of symmetry about the
!     equator as (v,w).  the input parameters ityp,nt,mdbc,nbdc must have
!     the same values used by vhags to compute br,bi,cr, and ci for (v,w).
!     vlap(i,j) and wlap(i,j) are given on the sphere at the gaussian
!     colatitude theta(i) (see nlat as input parameter) and east longitude
!     lambda(j) = (j-1)*2*pi/nlon for i=1,...,nlat and j=1,...,nlon.
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are computed
!            in radians in theta(1) <...< theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid point
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct longitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than zero. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!     ityp   this parameter should have the same value input to subroutine
!            vhags to compute the coefficients br,bi,cr, and ci for the
!            vector field (v,w).  ityp is set as follows:
!
!            = 0  no symmetries exist in (v,w) about the equator. (vlap,wlap)
!                 is computed and stored on the entire sphere in the arrays
!                 vlap(i,j) and wlap(i,j) for i=1,...,nlat and j=1,...,nlon.
!
!
!            = 1  no symmetries exist in (v,w) about the equator. (vlap,wlap)
!                 is computed and stored on the entire sphere in the arrays
!                 vlap(i,j) and wlap(i,j) for i=1,...,nlat and j=1,...,nlon.
!                 the vorticity of (v,w) is zero so the coefficients cr and
!                 ci are zero and are not used.  the vorticity of (vlap,wlap)
!                 is also zero.
!
!
!            = 2  no symmetries exist in (v,w) about the equator. (vlap,wlap)
!                 is computed and stored on the entire sphere in the arrays
!                 vlap(i,j) and wlap(i,j) for i=1,...,nlat and j=1,...,nlon.
!                 the divergence of (v,w) is zero so the coefficients br and
!                 bi are zero and are not used.  the divergence of (vlap,wlap)
!                 is also zero.
!
!            = 3  w is antisymmetric and v is symmetric about the equator.
!                 consequently wlap is antisymmetric and vlap is symmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 4  w is antisymmetric and v is symmetric about the equator.
!                 consequently wlap is antisymmetric and vlap is symmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.  the
!                 vorticity of (v,w) is zero so the coefficients cr,ci are
!                 zero and are not used. the vorticity of (vlap,wlap) is
!                 also zero.
!
!            = 5  w is antisymmetric and v is symmetric about the equator.
!                 consequently wlap is antisymmetric and vlap is symmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.  the
!                 divergence of (v,w) is zero so the coefficients br,bi
!                 are zero and are not used. the divergence of (vlap,wlap)
!                 is also zero.
!
!
!            = 6  w is symmetric and v is antisymmetric about the equator.
!                 consequently wlap is symmetric and vlap is antisymmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!            = 7  w is symmetric and v is antisymmetric about the equator.
!                 consequently wlap is symmetric and vlap is antisymmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.  the
!                 vorticity of (v,w) is zero so the coefficients cr,ci are
!                 zero and are not used. the vorticity of (vlap,wlap) is
!                 also zero.
!
!            = 8  w is symmetric and v is antisymmetric about the equator.
!                 consequently wlap is symmetric and vlap is antisymmetric.
!                 (vlap,wlap) is computed and stored on the northern
!                 hemisphere only.  if nlat is odd, storage is in the arrays
!                 vlap(i,j),wlap(i,j) for i=1,...,(nlat+1)/2 and j=1,...,nlon.
!                 if nlat is even, storage is in the arrays vlap(i,j),
!                 wlap(i,j) for i=1,...,nlat/2 and j=1,...,nlon.  the
!                 divergence of (v,w) is zero so the coefficients br,bi
!                 are zero and are not used. the divergence of (vlap,wlap)
!                 is also zero.
!
!
!     nt     nt is the number of vector fields (v,w).  some computational
!            efficiency is obtained for multiple fields.  in the program
!            that calls vlapgs, the arrays vlap,wlap,br,bi,cr and ci
!            can be three dimensional corresponding to an indexed multiple
!            vector field.  in this case multiple vector synthesis will
!            be performed to compute the vector laplacian for each field.
!            the third index is the synthesis index which assumes the values
!            k=1,...,nt.  for a single synthesis set nt=1.  the description
!            of the remaining parameters is simplified by assuming that nt=1
!            or that all arrays are two dimensional.
!
!   idvw     the first dimension of the arrays vlap and wlap as it appears
!            in the program that calls vlapgs.  if ityp=0,1, or 2  then idvw
!            must be at least nlat.  if ityp > 2 and nlat is even then idvw
!            must be at least nlat/2. if ityp > 2 and nlat is odd then idvw
!            must be at least (nlat+1)/2.
!
!   jdvw     the second dimension of the arrays vlap and wlap as it appears
!            in the program that calls vlapgs. jdvw must be at least nlon.
!
!
!   br,bi    two or three dimensional arrays (see input parameter nt)
!   cr,ci    that contain vector spherical harmonic coefficients
!            of the vector field (v,w) as computed by subroutine vhags.
!            br,bi,cr and ci must be computed by vhags prior to calling
!            vlapgs.  if ityp=1,4, or 7 then cr,ci are not used and can
!            be dummy arguments.  if ityp=2,5, or 8 then br,bi are not
!            used and can be dummy arguments.
!
!    mdbc    the first dimension of the arrays br,bi,cr and ci as it
!            appears in the program that calls vlapgs.  mdbc must be
!            at least min0(nlat,nlon/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!    ndbc    the second dimension of the arrays br,bi,cr and ci as it
!            appears in the program that calls vlapgs. ndbc must be at
!            least nlat.
!
!    wvhsgs  an array which must be initialized by subroutine vlapgsi
!            (or equivalently by vhsgsi).  once initialized, wvhsgs
!            can be used repeatedly by vlapgs as long as nlat and nlon
!            remain unchanged.  wvhsgs must not be altered between calls
!            of vlapgs.
!
!     lvhsgs the dimension of the array wvhsgs as it appears in the
!            program that calls vlapgs.  define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lvhsgs must be at least
!
!                 l1*l2*(nlat+nlat-l1+1)+nlon+15+2*nlat
!
!
!     work   a work array that does not have to be saved.
!
!     lwork  the dimension of the array work as it appears in the
!            program that calls vlapgs. define
!
!               l2 = nlat/2                    if nlat is even or
!               l2 = (nlat+1)/2                if nlat is odd
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            if ityp .le. 2 then
!
!               (2*nt+1)*nlat*nlon + nlat*(4*nt*l1+1)
!
!            or if ityp .gt. 2 then
!
!               (2*nt+1)*l2*nlon + nlat*(4*nt*l1+1)
!
!            will suffice as a length for lwork.
!
!     **************************************************************
!
!     output parameters
!
!
!    vlap,   two or three dimensional arrays (see input parameter nt) that
!    wlap    contain the vector laplacian of the field (v,w).  wlap(i,j) is
!            the east longitude component and vlap(i,j) is the colatitudinal
!            component of the vector laplacian.  the definition of the
!            vector laplacian follows:
!
!            let cost and sint be the cosine and sine at colatitude theta.
!            let d( )/dlambda  and d( )/dtheta be the first order partial
!            derivatives in longitude and colatitude.  let del2 be the scalar
!            laplacian operator
!
!                 del2(s) = [d(sint*d(s)/dtheta)/dtheta +
!                             2            2
!                            d (s)/dlambda /sint]/sint
!
!            then the vector laplacian opeator
!
!                 dvel2(v,w) = (vlap,wlap)
!
!            is defined by
!
!                 vlap = del2(v) - (2*cost*dw/dlambda + v)/sint**2
!
!                 wlap = del2(w) + (2*cost*dv/dlambda - w)/sint**2
!
!  ierror    a parameter which flags errors in input parameters as follows:
!
!            = 0  no errors detected
!
!            = 1  error in the specification of nlat
!
!            = 2  error in the specification of nlon
!
!            = 3  error in the specification of ityp
!
!            = 4  error in the specification of nt
!
!            = 5  error in the specification of idvw
!
!            = 6  error in the specification of jdvw
!
!            = 7  error in the specification of mdbc
!
!            = 8  error in the specification of ndbc
!
!            = 9  error in the specification of lvhsgs
!
!            = 10 error in the specification of lwork
!
!
! **********************************************************************
!
!     end of documentation for vlapgs
!
! **********************************************************************
!
      SUBROUTINE VLAPGS(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Br,Bi,Cr, &
                      & Ci,Mdbc,Ndbc,Wvhsgs,Lvhsgs,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL Bi , Br , Ci , Cr , Vlap , Wlap , Work , Wvhsgs
      INTEGER ibi , ibr , ici , icr , Idvw , idz , Ierror , ifn , imid ,&
            & Ityp , iwk , Jdvw , l1 , l2 , liwk , lsavmin , Lvhsgs ,   &
            & lwkmin , Lwork , lzimn
      INTEGER Mdbc , mmax , mn , Ndbc , Nlat , Nlon , Nt
      DIMENSION Vlap(Idvw,Jdvw,Nt) , Wlap(Idvw,Jdvw,Nt)
      DIMENSION Br(Mdbc,Ndbc,Nt) , Bi(Mdbc,Ndbc,Nt)
      DIMENSION Cr(Mdbc,Ndbc,Nt) , Ci(Mdbc,Ndbc,Nt)
      DIMENSION Wvhsgs(Lvhsgs) , Work(Lwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      IF ( Ityp<0 .OR. Ityp>8 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Ityp<=2 .AND. Idvw<Nlat) .OR. (Ityp>2 .AND. Idvw<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdbc<mmax ) RETURN
      Ierror = 8
      IF ( Ndbc<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
      lsavmin = lzimn + lzimn + Nlon + 15
      IF ( Lvhsgs<lsavmin ) RETURN
!
!     verify unsaved work space length
!
      mn = mmax*Nlat*Nt
      l2 = (Nlat+1)/2
      l1 = mmax
      IF ( Ityp<=2 ) THEN
         lwkmin = (2*Nt+1)*Nlat*Nlon + Nlat*(4*Nt*l1+1)
      ELSE
         lwkmin = (2*Nt+1)*l2*Nlon + Nlat*(4*Nt*l1+1)
      ENDIF
      IF ( Lwork<lwkmin ) RETURN
      Ierror = 0
!
!     set work space pointers for vector laplacian coefficients
!
      IF ( Ityp==0 .OR. Ityp==3 .OR. Ityp==6 ) THEN
         ibr = 1
         ibi = ibr + mn
         icr = ibi + mn
         ici = icr + mn
      ELSEIF ( Ityp==1 .OR. Ityp==4 .OR. Ityp==7 ) THEN
         ibr = 1
         ibi = ibr + mn
         icr = ibi + mn
         ici = icr
      ELSE
         ibr = 1
         ibi = 1
         icr = ibi + mn
         ici = icr + mn
      ENDIF
      ifn = ici + mn
      iwk = ifn + Nlat
      liwk = Lwork - 4*mn - Nlat
      CALL VLAPGS1(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Work(ibr),     &
                 & Work(ibi),Work(icr),Work(ici),mmax,Work(ifn),Mdbc,   &
                 & Ndbc,Br,Bi,Cr,Ci,Wvhsgs,Lvhsgs,Work(iwk),liwk,Ierror)
      END SUBROUTINE VLAPGS

      
      SUBROUTINE VLAPGS1(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Brlap,   &
                       & Bilap,Crlap,Cilap,Mmax,Fnn,Mdb,Ndb,Br,Bi,Cr,Ci,&
                       & Wsave,Lsave,Wk,Lwk,Ierror)
      IMPLICIT NONE
      REAL Bi , Bilap , Br , Brlap , Ci , Cilap , Cr , Crlap , fn ,     &
         & Fnn , Vlap , Wk , Wlap , Wsave
      INTEGER Idvw , Ierror , Ityp , Jdvw , k , Lsave , Lwk , m , Mdb , &
            & Mmax , n , Ndb , Nlat , Nlon , Nt
      DIMENSION Vlap(Idvw,Jdvw,Nt) , Wlap(Idvw,Jdvw,Nt)
      DIMENSION Fnn(Nlat) , Brlap(Mmax,Nlat,Nt) , Bilap(Mmax,Nlat,Nt)
      DIMENSION Crlap(Mmax,Nlat,Nt) , Cilap(Mmax,Nlat,Nt)
      DIMENSION Br(Mdb,Ndb,Nt) , Bi(Mdb,Ndb,Nt)
      DIMENSION Cr(Mdb,Ndb,Nt) , Ci(Mdb,Ndb,Nt)
      DIMENSION Wsave(Lsave) , Wk(Lwk)
!
!     preset coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Fnn(n) = -fn*(fn+1.)
      ENDDO
!
!     set laplacian coefficients from br,bi,cr,ci
!
      IF ( Ityp==0 .OR. Ityp==3 .OR. Ityp==6 ) THEN
!
!     all coefficients needed
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Brlap(m,n,k) = 0.0
                  Bilap(m,n,k) = 0.0
                  Crlap(m,n,k) = 0.0
                  Cilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Brlap(1,n,k) = Fnn(n)*Br(1,n,k)
               Bilap(1,n,k) = Fnn(n)*Bi(1,n,k)
               Crlap(1,n,k) = Fnn(n)*Cr(1,n,k)
               Cilap(1,n,k) = Fnn(n)*Ci(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Brlap(m,n,k) = Fnn(n)*Br(m,n,k)
                  Bilap(m,n,k) = Fnn(n)*Bi(m,n,k)
                  Crlap(m,n,k) = Fnn(n)*Cr(m,n,k)
                  Cilap(m,n,k) = Fnn(n)*Ci(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ELSEIF ( Ityp==1 .OR. Ityp==4 .OR. Ityp==7 ) THEN
!
!     vorticity is zero so cr,ci=0 not used
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Brlap(m,n,k) = 0.0
                  Bilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Brlap(1,n,k) = Fnn(n)*Br(1,n,k)
               Bilap(1,n,k) = Fnn(n)*Bi(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Brlap(m,n,k) = Fnn(n)*Br(m,n,k)
                  Bilap(m,n,k) = Fnn(n)*Bi(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ELSE
!
!     divergence is zero so br,bi=0 not used
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Crlap(m,n,k) = 0.0
                  Cilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Crlap(1,n,k) = Fnn(n)*Cr(1,n,k)
               Cilap(1,n,k) = Fnn(n)*Ci(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Crlap(m,n,k) = Fnn(n)*Cr(m,n,k)
                  Cilap(m,n,k) = Fnn(n)*Ci(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ENDIF
!
!     sythesize coefs into vector field (vlap,wlap)
!
      CALL VHSGS(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Brlap,Bilap,     &
               & Crlap,Cilap,Mmax,Nlat,Wsave,Lsave,Wk,Lwk,Ierror)
      END SUBROUTINE VLAPGS1


      SUBROUTINE DVLAPGS(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Br,Bi,Cr, &
                      & Ci,Mdbc,Ndbc,Wvhsgs,Lvhsgs,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Br , Ci , Cr , Vlap , Wlap , Work , Wvhsgs
      INTEGER ibi , ibr , ici , icr , Idvw , idz , Ierror , ifn , imid ,&
            & Ityp , iwk , Jdvw , l1 , l2 , liwk , lsavmin , Lvhsgs ,   &
            & lwkmin , Lwork , lzimn
      INTEGER Mdbc , mmax , mn , Ndbc , Nlat , Nlon , Nt
      DIMENSION Vlap(Idvw,Jdvw,Nt) , Wlap(Idvw,Jdvw,Nt)
      DIMENSION Br(Mdbc,Ndbc,Nt) , Bi(Mdbc,Ndbc,Nt)
      DIMENSION Cr(Mdbc,Ndbc,Nt) , Ci(Mdbc,Ndbc,Nt)
      DIMENSION Wvhsgs(Lvhsgs) , Work(Lwork)
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<1 ) RETURN
      Ierror = 3
      IF ( Ityp<0 .OR. Ityp>8 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Ityp<=2 .AND. Idvw<Nlat) .OR. (Ityp>2 .AND. Idvw<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jdvw<Nlon ) RETURN
      Ierror = 7
      mmax = MIN0(Nlat,(Nlon+1)/2)
      IF ( Mdbc<mmax ) RETURN
      Ierror = 8
      IF ( Ndbc<Nlat ) RETURN
      Ierror = 9
      idz = (mmax*(Nlat+Nlat-mmax+1))/2
      lzimn = idz*imid
      lsavmin = lzimn + lzimn + Nlon + 15
      IF ( Lvhsgs<lsavmin ) RETURN
!
!     verify unsaved work space length
!
      mn = mmax*Nlat*Nt
      l2 = (Nlat+1)/2
      l1 = mmax
      IF ( Ityp<=2 ) THEN
         lwkmin = (2*Nt+1)*Nlat*Nlon + Nlat*(4*Nt*l1+1)
      ELSE
         lwkmin = (2*Nt+1)*l2*Nlon + Nlat*(4*Nt*l1+1)
      ENDIF
      IF ( Lwork<lwkmin ) RETURN
      Ierror = 0
!
!     set work space pointers for vector laplacian coefficients
!
      IF ( Ityp==0 .OR. Ityp==3 .OR. Ityp==6 ) THEN
         ibr = 1
         ibi = ibr + mn
         icr = ibi + mn
         ici = icr + mn
      ELSEIF ( Ityp==1 .OR. Ityp==4 .OR. Ityp==7 ) THEN
         ibr = 1
         ibi = ibr + mn
         icr = ibi + mn
         ici = icr
      ELSE
         ibr = 1
         ibi = 1
         icr = ibi + mn
         ici = icr + mn
      ENDIF
      ifn = ici + mn
      iwk = ifn + Nlat
      liwk = Lwork - 4*mn - Nlat
      CALL DVLAPGS1(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Work(ibr),     &
                 & Work(ibi),Work(icr),Work(ici),mmax,Work(ifn),Mdbc,   &
                 & Ndbc,Br,Bi,Cr,Ci,Wvhsgs,Lvhsgs,Work(iwk),liwk,Ierror)
      END SUBROUTINE DVLAPGS

      
      SUBROUTINE DVLAPGS1(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Brlap,   &
                       & Bilap,Crlap,Cilap,Mmax,Fnn,Mdb,Ndb,Br,Bi,Cr,Ci,&
                       & Wsave,Lsave,Wk,Lwk,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION Bi , Bilap , Br , Brlap , Ci , Cilap , Cr , Crlap , fn ,     &
         & Fnn , Vlap , Wk , Wlap , Wsave
      INTEGER Idvw , Ierror , Ityp , Jdvw , k , Lsave , Lwk , m , Mdb , &
            & Mmax , n , Ndb , Nlat , Nlon , Nt
      DIMENSION Vlap(Idvw,Jdvw,Nt) , Wlap(Idvw,Jdvw,Nt)
      DIMENSION Fnn(Nlat) , Brlap(Mmax,Nlat,Nt) , Bilap(Mmax,Nlat,Nt)
      DIMENSION Crlap(Mmax,Nlat,Nt) , Cilap(Mmax,Nlat,Nt)
      DIMENSION Br(Mdb,Ndb,Nt) , Bi(Mdb,Ndb,Nt)
      DIMENSION Cr(Mdb,Ndb,Nt) , Ci(Mdb,Ndb,Nt)
      DIMENSION Wsave(Lsave) , Wk(Lwk)
!
!     preset coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Fnn(n) = -fn*(fn+1.)
      ENDDO
!
!     set laplacian coefficients from br,bi,cr,ci
!
      IF ( Ityp==0 .OR. Ityp==3 .OR. Ityp==6 ) THEN
!
!     all coefficients needed
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Brlap(m,n,k) = 0.0
                  Bilap(m,n,k) = 0.0
                  Crlap(m,n,k) = 0.0
                  Cilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Brlap(1,n,k) = Fnn(n)*Br(1,n,k)
               Bilap(1,n,k) = Fnn(n)*Bi(1,n,k)
               Crlap(1,n,k) = Fnn(n)*Cr(1,n,k)
               Cilap(1,n,k) = Fnn(n)*Ci(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Brlap(m,n,k) = Fnn(n)*Br(m,n,k)
                  Bilap(m,n,k) = Fnn(n)*Bi(m,n,k)
                  Crlap(m,n,k) = Fnn(n)*Cr(m,n,k)
                  Cilap(m,n,k) = Fnn(n)*Ci(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ELSEIF ( Ityp==1 .OR. Ityp==4 .OR. Ityp==7 ) THEN
!
!     vorticity is zero so cr,ci=0 not used
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Brlap(m,n,k) = 0.0
                  Bilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Brlap(1,n,k) = Fnn(n)*Br(1,n,k)
               Bilap(1,n,k) = Fnn(n)*Bi(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Brlap(m,n,k) = Fnn(n)*Br(m,n,k)
                  Bilap(m,n,k) = Fnn(n)*Bi(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ELSE
!
!     divergence is zero so br,bi=0 not used
!
         DO k = 1 , Nt
            DO n = 1 , Nlat
               DO m = 1 , Mmax
                  Crlap(m,n,k) = 0.0
                  Cilap(m,n,k) = 0.0
               ENDDO
            ENDDO
            DO n = 2 , Nlat
               Crlap(1,n,k) = Fnn(n)*Cr(1,n,k)
               Cilap(1,n,k) = Fnn(n)*Ci(1,n,k)
            ENDDO
            DO m = 2 , Mmax
               DO n = m , Nlat
                  Crlap(m,n,k) = Fnn(n)*Cr(m,n,k)
                  Cilap(m,n,k) = Fnn(n)*Ci(m,n,k)
               ENDDO
            ENDDO
         ENDDO
      ENDIF
!
!     sythesize coefs into vector field (vlap,wlap)
!
      CALL DVHSGS(Nlat,Nlon,Ityp,Nt,Vlap,Wlap,Idvw,Jdvw,Brlap,Bilap,     &
               & Crlap,Cilap,Mmax,Nlat,Wsave,Lsave,Wk,Lwk,Ierror)
      END SUBROUTINE DVLAPGS1
