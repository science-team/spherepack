!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
!
! ... file vrtec.f
!
!     this file includes documentation and code for
!     subroutine divec          i
!
! ... files which must be loaded with vrtec.f
!
!     sphcom.f, hrfft.f, vhaec.f,shsec.f
!
!     subroutine vrtec(nlat,nlon,isym,nt,vt,ivrt,jvrt,cr,ci,mdc,ndc,
!    +                 wshsec,lshsec,work,lwork,ierror)
!
!     given the vector spherical harmonic coefficients cr and ci, precomputed
!     by subroutine vhaec for a vector field (v,w), subroutine vrtec
!     computes the vorticity of the vector field in the scalar array
!     vt.  vt(i,j) is the vorticity at the colatitude
!
!            theta(i) = (i-1)*pi/(nlat-1)
!
!     and longitude
!
!            lambda(j) = (j-1)*2*pi/nlon
!
!     on the sphere.  i.e.,
!
!            vt(i,j) =  [-dv/dlambda + d(sint*w)/dtheta]/sint
!
!     where sint = sin(theta(i)).  w is the east longitudinal and v
!     is the colatitudinal component of the vector field from which
!     cr,ci were precomputed.  required associated legendre polynomials
!     are recomputed rather than stored as they are in subroutine vrtes.
!
!
!     input parameters
!
!     nlat   the number of colatitudes on the full sphere including the
!            poles. for example, nlat = 37 for a five degree grid.
!            nlat determines the grid increment in colatitude as
!            pi/(nlat-1).  if nlat is odd the equator is located at
!            grid point i=(nlat+1)/2. if nlat is even the equator is
!            located half way between points i=nlat/2 and i=nlat/2+1.
!            nlat must be at least 3. note: on the half sphere, the
!            number of grid points in the colatitudinal direction is
!            nlat/2 if nlat is even or (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than 3. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!
!     isym   a parameter which determines whether the vorticity is
!            computed on the full or half sphere as follows:
!
!      = 0
!            the symmetries/antsymmetries described in isym=1,2 below
!            do not exist in (v,w) about the equator.  in this case the
!            vorticity is neither symmetric nor antisymmetric about
!            the equator.  the vorticity is computed on the entire
!            sphere.  i.e., in the array vt(i,j) for i=1,...,nlat and
!            j=1,...,nlon.
!
!      = 1
!            w is antisymmetric and v is symmetric about the equator.
!            in this case the vorticity is symmetyric about the
!            equator and is computed for the northern hemisphere
!            only.  i.e., if nlat is odd the vorticity is computed
!            in the array vt(i,j) for i=1,...,(nlat+1)/2 and for
!            j=1,...,nlon.  if nlat is even the vorticity is computed
!            in the array vt(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!      = 2
!            w is symmetric and v is antisymmetric about the equator
!            in this case the vorticity is antisymmetric about the
!            equator and is computed for the northern hemisphere
!            only.  i.e., if nlat is odd the vorticity is computed
!            in the array vt(i,j) for i=1,...,(nlat+1)/2 and for
!            j=1,...,nlon.  if nlat is even the vorticity is computed
!            in the array vt(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!
!      nt    nt is the number of scalar and vector fields.  some
!            computational efficiency is obtained for multiple fields.
!            in the program that calls vrtec, the arrays cr,ci, and vort
!            can be three dimensional corresponding to an indexed multiple
!            vector field.  in this case multiple scalar synthesis will
!            be performed to compute the vorticity for each field.  the
!            third index is the synthesis index which assumes the values
!            k=1,...,nt.  for a single synthesis set nt = 1.  the
!            description of the remaining parameters is simplified by
!            assuming that nt=1 or that all the arrays are two dimensional.
!
!     ivrt   the first dimension of the array vt as it appears in
!            the program that calls vrtec. if isym = 0 then ivrt
!            must be at least nlat.  if isym = 1 or 2 and nlat is
!            even then ivrt must be at least nlat/2. if isym = 1 or 2
!            and nlat is odd then ivrt must be at least (nlat+1)/2.
!
!     jvrt   the second dimension of the array vt as it appears in
!            the program that calls vrtec. jvrt must be at least nlon.
!
!    cr,ci   two or three dimensional arrays (see input parameter nt)
!            that contain vector spherical harmonic coefficients
!            of the vector field (v,w) as computed by subroutine vhaec.
!     ***    cr and ci must be computed by vhaec prior to calling
!            vrtec.
!
!      mdc   the first dimension of the arrays cr and ci as it
!            appears in the program that calls vrtec. mdc must be at
!            least min0(nlat,nlon/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!      ndc   the second dimension of the arrays cr and ci as it
!            appears in the program that calls vrtec. ndc must be at
!            least nlat.
!
!   wshsec   an array which must be initialized by subroutine shseci.
!            once initialized,
!            wshsec can be used repeatedly by vrtec as long as nlon
!            and nlat remain unchanged.  wshsec must not be altered
!            between calls of vrtec
!
!   lshsec   the dimension of the array wshsec as it appears in the
!            program that calls vrtec. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshsec must be at least
!
!            2*nlat*l2+3*((l1-2)*(nlat+nlat-l1-1))/2+nlon+15
!
!     work   a work array that does not have to be saved.
!
!    lwork   the dimension of the array work as it appears in the
!            program that calls vrtec. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd.
!
!            if isym is zero then lwork must be at least
!
!               nlat*(nt*nlon+max0(3*l2,nlon)+2*nt*l1+1)
!
!            if isym is not zero then lwork must be at least
!
!               l2*(nt*nlon+max0(3*nlat,nlon)) + nlat*(2*nt*l1+1)
!
!
!
!     **************************************************************
!
!     output parameters
!
!
!     vt     a two or three dimensional array (see input parameter nt)
!            that contains the vorticity of the vector field (v,w)
!            whose coefficients cr,ci where computed by subroutine vhaec.
!            vt(i,j) is the vorticity at the colatitude point theta(i) =
!            (i-1)*pi/(nlat-1) and longitude point lambda(j) =
!            (j-1)*2*pi/nlon. the index ranges are defined above at the
!            input parameter isym.
!
!
!   ierror   an error parameter which indicates fatal errors with input
!            parameters when returned positive.
!          = 0  no errors
!          = 1  error in the specification of nlat
!          = 2  error in the specification of nlon
!          = 3  error in the specification of isym
!          = 4  error in the specification of nt
!          = 5  error in the specification of ivrt
!          = 6  error in the specification of jvrt
!          = 7  error in the specification of mdc
!          = 8  error in the specification of ndc
!          = 9  error in the specification of lshsec
!          = 10 error in the specification of lwork
! **********************************************************************
!
!
      SUBROUTINE VRTEC(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc,  &
                     & Wshsec,Lshsec,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL Ci , Cr , Vort , Work , Wshsec
      INTEGER ia , ib , Ierror , imid , is , Isym , Ivrt , iwk , Jvrt , &
            & l1 , l2 , labc , ls , Lshsec , lwk , lwkmin , Lwork ,     &
            & lzz1 , mab , Mdc
      INTEGER mmax , mn , Ndc , Nlat , nln , Nlon , Nt
 
      DIMENSION Vort(Ivrt,Jvrt,Nt) , Cr(Mdc,Ndc,Nt) , Ci(Mdc,Ndc,Nt)
      DIMENSION Wshsec(Lshsec) , Work(Lwork)
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Isym==0 .AND. Ivrt<Nlat) .OR. (Isym>0 .AND. Ivrt<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jvrt<Nlon ) RETURN
      Ierror = 7
      IF ( Mdc<MIN0(Nlat,(Nlon+1)/2) ) RETURN
      mmax = MIN0(Nlat,(Nlon+2)/2)
      Ierror = 8
      IF ( Ndc<Nlat ) RETURN
      Ierror = 9
!
!     verify saved work space (same as shec)
!
      lzz1 = 2*Nlat*imid
      labc = 3*(MAX0(mmax-2,0)*(Nlat+Nlat-mmax-1))/2
      IF ( Lshsec<lzz1+labc+Nlon+15 ) RETURN
      Ierror = 10
!
!     verify unsaved work space (add to what shec requires)
!
      ls = Nlat
      IF ( Isym>0 ) ls = imid
      nln = Nt*ls*Nlon
!
!     set first dimension for a,b (as requried by shsec)
!
      mab = MIN0(Nlat,Nlon/2+1)
      mn = mab*Nlat*Nt
!     if(lwork.lt.nln+max0(ls*nlon,3*nlat*imid)+2*mn+nlat) return
      l1 = MIN0(Nlat,(Nlon+2)/2)
      l2 = (Nlat+1)/2
      IF ( Isym==0 ) THEN
         lwkmin = Nlat*(Nt*Nlon+MAX0(3*l2,Nlon)+2*Nt*l1+1)
      ELSE
         lwkmin = l2*(Nt*Nlon+MAX0(3*Nlat,Nlon)) + Nlat*(2*Nt*l1+1)
      ENDIF
      IF ( Lwork<lwkmin ) RETURN
      Ierror = 0
!
!     set work space pointers
!
      ia = 1
      ib = ia + mn
      is = ib + mn
      iwk = is + Nlat
      lwk = Lwork - 2*mn - Nlat
      CALL VRTEC1(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc,       &
                & Work(ia),Work(ib),mab,Work(is),Wshsec,Lshsec,Work(iwk)&
                & ,lwk,Ierror)
      END SUBROUTINE VRTEC

      
      SUBROUTINE VRTEC1(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc, &
                      & A,B,Mab,Sqnn,Wshsec,Lshsec,Wk,Lwk,Ierror)
      IMPLICIT NONE
      REAL A , B , Ci , Cr , fn , Sqnn , Vort , Wk , Wshsec
      INTEGER Ierror , Isym , Ivrt , Jvrt , k , Lshsec , Lwk , m , Mab ,&
            & Mdc , mmax , n , Ndc , Nlat , Nlon , Nt
      DIMENSION Vort(Ivrt,Jvrt,Nt) , Cr(Mdc,Ndc,Nt) , Ci(Mdc,Ndc,Nt)
      DIMENSION A(Mab,Nlat,Nt) , B(Mab,Nlat,Nt) , Sqnn(Nlat)
      DIMENSION Wshsec(Lshsec) , Wk(Lwk)
!
!     set coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Sqnn(n) = SQRT(fn*(fn+1.))
      ENDDO
!
!     compute vorticity scalar coefficients for each vector field
!
      DO k = 1 , Nt
         DO n = 1 , Nlat
            DO m = 1 , Mab
               A(m,n,k) = 0.0
               B(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            A(1,n,k) = Sqnn(n)*Cr(1,n,k)
            B(1,n,k) = Sqnn(n)*Ci(1,n,k)
         ENDDO
!
!     compute m>0 coefficients, use mmax from vector coef range
!
         mmax = MIN0(Nlat,(Nlon+1)/2)
         DO m = 2 , mmax
            DO n = m , Nlat
               A(m,n,k) = Sqnn(n)*Cr(m,n,k)
               B(m,n,k) = Sqnn(n)*Ci(m,n,k)
            ENDDO
         ENDDO
      ENDDO
!
!     synthesize a,b into vort
!
      CALL SHSEC(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,A,B,Mab,Nlat,Wshsec,  &
               & Lshsec,Wk,Lwk,Ierror)
      END SUBROUTINE VRTEC1


      SUBROUTINE DVRTEC(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc,  &
                     & Wshsec,Lshsec,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION Ci , Cr , Vort , Work , Wshsec
      INTEGER ia , ib , Ierror , imid , is , Isym , Ivrt , iwk , Jvrt , &
            & l1 , l2 , labc , ls , Lshsec , lwk , lwkmin , Lwork ,     &
            & lzz1 , mab , Mdc
      INTEGER mmax , mn , Ndc , Nlat , nln , Nlon , Nt
 
      DIMENSION Vort(Ivrt,Jvrt,Nt) , Cr(Mdc,Ndc,Nt) , Ci(Mdc,Ndc,Nt)
      DIMENSION Wshsec(Lshsec) , Work(Lwork)
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Isym==0 .AND. Ivrt<Nlat) .OR. (Isym>0 .AND. Ivrt<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jvrt<Nlon ) RETURN
      Ierror = 7
      IF ( Mdc<MIN0(Nlat,(Nlon+1)/2) ) RETURN
      mmax = MIN0(Nlat,(Nlon+2)/2)
      Ierror = 8
      IF ( Ndc<Nlat ) RETURN
      Ierror = 9
!
!     verify saved work space (same as shec)
!
      lzz1 = 2*Nlat*imid
      labc = 3*(MAX0(mmax-2,0)*(Nlat+Nlat-mmax-1))/2
      IF ( Lshsec<lzz1+labc+Nlon+15 ) RETURN
      Ierror = 10
!
!     verify unsaved work space (add to what shec requires)
!
      ls = Nlat
      IF ( Isym>0 ) ls = imid
      nln = Nt*ls*Nlon
!
!     set first dimension for a,b (as requried by shsec)
!
      mab = MIN0(Nlat,Nlon/2+1)
      mn = mab*Nlat*Nt
!     if(lwork.lt.nln+max0(ls*nlon,3*nlat*imid)+2*mn+nlat) return
      l1 = MIN0(Nlat,(Nlon+2)/2)
      l2 = (Nlat+1)/2
      IF ( Isym==0 ) THEN
         lwkmin = Nlat*(Nt*Nlon+MAX0(3*l2,Nlon)+2*Nt*l1+1)
      ELSE
         lwkmin = l2*(Nt*Nlon+MAX0(3*Nlat,Nlon)) + Nlat*(2*Nt*l1+1)
      ENDIF
      IF ( Lwork<lwkmin ) RETURN
      Ierror = 0
!
!     set work space pointers
!
      ia = 1
      ib = ia + mn
      is = ib + mn
      iwk = is + Nlat
      lwk = Lwork - 2*mn - Nlat
      CALL DVRTEC1(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc,       &
                & Work(ia),Work(ib),mab,Work(is),Wshsec,Lshsec,Work(iwk)&
                & ,lwk,Ierror)
      END SUBROUTINE DVRTEC

      
      SUBROUTINE DVRTEC1(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc, &
                      & A,B,Mab,Sqnn,Wshsec,Lshsec,Wk,Lwk,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION A , B , Ci , Cr , fn , Sqnn , Vort , Wk , Wshsec
      INTEGER Ierror , Isym , Ivrt , Jvrt , k , Lshsec , Lwk , m , Mab ,&
            & Mdc , mmax , n , Ndc , Nlat , Nlon , Nt
      DIMENSION Vort(Ivrt,Jvrt,Nt) , Cr(Mdc,Ndc,Nt) , Ci(Mdc,Ndc,Nt)
      DIMENSION A(Mab,Nlat,Nt) , B(Mab,Nlat,Nt) , Sqnn(Nlat)
      DIMENSION Wshsec(Lshsec) , Wk(Lwk)
!
!     set coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Sqnn(n) = SQRT(fn*(fn+1.))
      ENDDO
!
!     compute vorticity scalar coefficients for each vector field
!
      DO k = 1 , Nt
         DO n = 1 , Nlat
            DO m = 1 , Mab
               A(m,n,k) = 0.0
               B(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            A(1,n,k) = Sqnn(n)*Cr(1,n,k)
            B(1,n,k) = Sqnn(n)*Ci(1,n,k)
         ENDDO
!
!     compute m>0 coefficients, use mmax from vector coef range
!
         mmax = MIN0(Nlat,(Nlon+1)/2)
         DO m = 2 , mmax
            DO n = m , Nlat
               A(m,n,k) = Sqnn(n)*Cr(m,n,k)
               B(m,n,k) = Sqnn(n)*Ci(m,n,k)
            ENDDO
         ENDDO
      ENDDO
!
!     synthesize a,b into vort
!
      CALL DSHSEC(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,A,B,Mab,Nlat,Wshsec,  &
               & Lshsec,Wk,Lwk,Ierror)
      END SUBROUTINE DVRTEC1
