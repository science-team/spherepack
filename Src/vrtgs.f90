!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!  .                                                             .
!  .                  copyright (c) 1998 by UCAR                 .
!  .                                                             .
!  .       University Corporation for Atmospheric Research       .
!  .                                                             .
!  .                      all rights reserved                    .
!  .                                                             .
!  .                                                             .
!  .                         SPHEREPACK3.0                       .
!  .                                                             .
!  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
!
!
!
! ... file vrtgs.f
!
!     this file includes documentation and code for
!     subroutine divgs          i
!
! ... files which must be loaded with vrtgs.f
!
!     sphcom.f, hrfft.f, vhgsc.f, shsgs.f, gaqd.f
!
!     subroutine vrtgs(nlat,nlon,isym,nt,vort,ivrt,jvrt,cr,ci,mdc,ndc,
!    +                 wshsgs,lshsgs,work,lwork,ierror)
!
!     given the vector spherical harmonic coefficients cr and ci, precomputed
!     by subroutine vhags for a vector field (v,w), subroutine vrtgs
!     computes the vorticity of the vector field in the scalar array
!     vort.  vort(i,j) is the vorticity at the gaussian colatitude
!     theta(i) (see nlat as input parameter) and longitude
!     lambda(j) = (j-1)*2*pi/nlon on the sphere.  i.e.,
!
!            vort(i,j) =  [-dv/dlambda + d(sint*w)/dtheta]/sint
!
!     where sint = sin(theta(i)).  w is the east longitudinal and v
!     is the colatitudinal component of the vector field from which
!     cr,ci were precomputed.  required associated legendre polynomials
!     are stored rather than recomputed as they are in subroutine vrtgc.
!
!
!     input parameters
!
!     nlat   the number of points in the gaussian colatitude grid on the
!            full sphere. these lie in the interval (0,pi) and are computed
!            in radians in theta(1) <...< theta(nlat) by subroutine gaqd.
!            if nlat is odd the equator will be included as the grid point
!            theta((nlat+1)/2).  if nlat is even the equator will be
!            excluded as a grid point and will lie half way between
!            theta(nlat/2) and theta(nlat/2+1). nlat must be at least 3.
!            note: on the half sphere, the number of grid points in the
!            colatitudinal direction is nlat/2 if nlat is even or
!            (nlat+1)/2 if nlat is odd.
!
!     nlon   the number of distinct londitude points.  nlon determines
!            the grid increment in longitude as 2*pi/nlon. for example
!            nlon = 72 for a five degree grid. nlon must be greater
!            than 3. the axisymmetric case corresponds to nlon=1.
!            the efficiency of the computation is improved when nlon
!            is a product of small prime numbers.
!
!
!     isym   a parameter which determines whether the vorticity is
!            computed on the full or half sphere as follows:
!
!      = 0
!            the symmetries/antsymmetries described in isym=1,2 below
!            do not exist in (v,w) about the equator.  in this case the
!            vorticity is neither symmetric nor antisymmetric about
!            the equator.  the vorticity is computed on the entire
!            sphere.  i.e., in the array vort(i,j) for i=1,...,nlat and
!            j=1,...,nlon.
!
!      = 1
!            w is antisymmetric and v is symmetric about the equator.
!            in this case the vorticity is symmetyric about the
!            equator and is computed for the northern hemisphere
!            only.  i.e., if nlat is odd the vorticity is computed
!            in the array vort(i,j) for i=1,...,(nlat+1)/2 and for
!            j=1,...,nlon.  if nlat is even the vorticity is computed
!            in the array vort(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!      = 2
!            w is symmetric and v is antisymmetric about the equator
!            in this case the vorticity is antisymmetric about the
!            equator and is computed for the northern hemisphere
!            only.  i.e., if nlat is odd the vorticity is computed
!            in the array vort(i,j) for i=1,...,(nlat+1)/2 and for
!            j=1,...,nlon.  if nlat is even the vorticity is computed
!            in the array vort(i,j) for i=1,...,nlat/2 and j=1,...,nlon.
!
!
!      nt    nt is the number of scalar and vector fields.  some
!            computational efficiency is obtained for multiple fields.
!            in the program that calls vrtgs, the arrays cr,ci, and vort
!            can be three dimensional corresponding to an indexed multiple
!            vector field.  in this case multiple scalar synthesis will
!            be performed to compute the vorticity for each field.  the
!            third index is the synthesis index which assumes the values
!            k=1,...,nt.  for a single synthesis set nt = 1.  the
!            description of the remaining parameters is simplified by
!            assuming that nt=1 or that all the arrays are two dimensional.
!
!     ivrt   the first dimension of the array vort as it appears in
!            the program that calls vrtgs. if isym = 0 then ivrt
!            must be at least nlat.  if isym = 1 or 2 and nlat is
!            even then ivrt must be at least nlat/2. if isym = 1 or 2
!            and nlat is odd then ivrt must be at least (nlat+1)/2.
!
!     jvrt   the second dimension of the array vort as it appears in
!            the program that calls vrtgs. jvrt must be at least nlon.
!
!    cr,ci   two or three dimensional arrays (see input parameter nt)
!            that contain vector spherical harmonic coefficients
!            of the vector field (v,w) as computed by subroutine vhags.
!     ***    cr and ci must be computed by vhags prior to calling
!            vrtgs.
!
!      mdc   the first dimension of the arrays cr and ci as it
!            appears in the program that calls vrtgs. mdc must be at
!            least min0(nlat,nlon/2) if nlon is even or at least
!            min0(nlat,(nlon+1)/2) if nlon is odd.
!
!      ndc   the second dimension of the arrays cr and ci as it
!            appears in the program that calls vrtgs. ndc must be at
!            least nlat.
!
!   wshsgs   an array which must be initialized by subroutine shsgsi.
!            once initialized,
!            wshsgs can be used repeatedly by vrtgs as long as nlon
!            and nlat remain unchanged.  wshsgs must not be altered
!            between calls of vrtgs
!
!   lshsgs   the dimension of the array wshsgs   as it appears in the
!            program that calls vrtgs. define
!
!               l1 = min0(nlat,(nlon+2)/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd
!
!            then lshsgs must be at least
!
!            nlat*(3*(l1+l2)-2)+(l1-1)*(l2*(2*nlat-l1)-3*l1)/2+nlon+15
!
!     work   a work array that does not have to be saved.
!
!    lwork   the dimension of the array work as it appears in the
!            program that calls vrtgs. define
!
!               l1 = min0(nlat,nlon/2) if nlon is even or
!               l1 = min0(nlat,(nlon+1)/2) if nlon is odd
!
!            and
!
!               l2 = nlat/2        if nlat is even or
!               l2 = (nlat+1)/2    if nlat is odd.
!
!            if isym = 0 then lwork must be at least
!
!               nlat*((nt+1)*nlon+2*nt*l1+1)
!
!            if isym > 0 then lwork must be at least
!
!               (nt+1)*l2*nlon+nlat*(2*nt*l1+1)
!
!
!     **************************************************************
!
!     output parameters
!
!
!     vort   a two or three dimensional array (see input parameter nt)
!            that contains the vorticity of the vector field (v,w)
!            whose coefficients cr,ci where computed by subroutine vhags.
!            vort(i,j) is the vorticity at the gaussian colatitude point
!            theta(i) and longitude point lambda(j) = (j-1)*2*pi/nlon.
!            the index ranges are defined above at the input parameter
!            isym.
!
!
!   ierror   an error parameter which indicates fatal errors with input
!            parameters when returned positive.
!          = 0  no errors
!          = 1  error in the specification of nlat
!          = 2  error in the specification of nlon
!          = 3  error in the specification of isym
!          = 4  error in the specification of nt
!          = 5  error in the specification of ivrt
!          = 6  error in the specification of jvrt
!          = 7  error in the specification of mdc
!          = 8  error in the specification of ndc
!          = 9  error in the specification of lshsgs
!          = 10 error in the specification of lwork
! **********************************************************************
!
!
      SUBROUTINE VRTGS(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc,  &
                     & Wshsgs,Lshsgs,Work,Lwork,Ierror)
      IMPLICIT NONE
      REAL Ci , Cr , Vort , Work , Wshsgs
      INTEGER ia , ib , Ierror , imid , is , Isym , Ivrt , iwk , Jvrt , &
            & l1 , l2 , lp , lpimn , ls , Lshsgs , lwk , Lwork , mab ,  &
            & Mdc , mmax
      INTEGER mn , Ndc , Nlat , nln , Nlon , Nt
 
      DIMENSION Vort(Ivrt,Jvrt,Nt) , Cr(Mdc,Ndc,Nt) , Ci(Mdc,Ndc,Nt)
      DIMENSION Wshsgs(Lshsgs) , Work(Lwork)
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Isym==0 .AND. Ivrt<Nlat) .OR. (Isym>0 .AND. Ivrt<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jvrt<Nlon ) RETURN
      Ierror = 7
      IF ( Mdc<MIN0(Nlat,(Nlon+1)/2) ) RETURN
      mmax = MIN0(Nlat,(Nlon+2)/2)
      Ierror = 8
      IF ( Ndc<Nlat ) RETURN
      Ierror = 9
      imid = (Nlat+1)/2
      lpimn = (imid*mmax*(Nlat+Nlat-mmax+1))/2
      l2 = (Nlat+MOD(Nlat,2))/2
      l1 = MIN0((Nlon+2)/2,Nlat)
      lp = Nlat*(3*(l1+l2)-2) + (l1-1)*(l2*(2*Nlat-l1)-3*l1)/2 + Nlon + &
         & 15
      IF ( Lshsgs<lp ) RETURN
      Ierror = 10
!
!     verify unsaved work space (add to what shses requires, file f3)
!
!
!     set first dimension for a,b (as requried by shses)
!
      mab = MIN0(Nlat,Nlon/2+1)
      mn = mab*Nlat*Nt
      ls = Nlat
      IF ( Isym>0 ) ls = imid
      nln = Nt*ls*Nlon
      IF ( Lwork<nln+ls*Nlon+2*mn+Nlat ) RETURN
      Ierror = 0
!
!     set work space pointers
!
      ia = 1
      ib = ia + mn
      is = ib + mn
      iwk = is + Nlat
      lwk = Lwork - 2*mn - Nlat
      CALL VRTGS1(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc,       &
                & Work(ia),Work(ib),mab,Work(is),Wshsgs,Lshsgs,Work(iwk)&
                & ,lwk,Ierror)
      END SUBROUTINE VRTGS

      
      SUBROUTINE VRTGS1(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc, &
                      & A,B,Mab,Sqnn,Wsav,Lwsav,Wk,Lwk,Ierror)
      IMPLICIT NONE
      REAL A , B , Ci , Cr , fn , Sqnn , Vort , Wk , Wsav
      INTEGER Ierror , Isym , Ivrt , Jvrt , k , Lwk , Lwsav , m , Mab , &
            & Mdc , mmax , n , Ndc , Nlat , Nlon , Nt
      DIMENSION Vort(Ivrt,Jvrt,Nt) , Cr(Mdc,Ndc,Nt) , Ci(Mdc,Ndc,Nt)
      DIMENSION A(Mab,Nlat,Nt) , B(Mab,Nlat,Nt) , Sqnn(Nlat)
      DIMENSION Wsav(Lwsav) , Wk(Lwk)
!
!     set coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Sqnn(n) = SQRT(fn*(fn+1.))
      ENDDO
!
!     compute divergence scalar coefficients for each vector field
!
      DO k = 1 , Nt
         DO n = 1 , Nlat
            DO m = 1 , Mab
               A(m,n,k) = 0.0
               B(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            A(1,n,k) = Sqnn(n)*Cr(1,n,k)
            B(1,n,k) = Sqnn(n)*Ci(1,n,k)
         ENDDO
!
!     compute m>0 coefficients
!
         mmax = MIN0(Nlat,(Nlon+1)/2)
         DO m = 2 , mmax
            DO n = m , Nlat
               A(m,n,k) = Sqnn(n)*Cr(m,n,k)
               B(m,n,k) = Sqnn(n)*Ci(m,n,k)
            ENDDO
         ENDDO
      ENDDO
!
!     synthesize a,b into vort
!
      CALL SHSGS(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,A,B,Mab,Nlat,Wsav,    &
               & Lwsav,Wk,Lwk,Ierror)
      END SUBROUTINE VRTGS1

      
      SUBROUTINE DVRTGS(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc,  &
                     & Wshsgs,Lshsgs,Work,Lwork,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION Ci , Cr , Vort , Work , Wshsgs
      INTEGER ia , ib , Ierror , imid , is , Isym , Ivrt , iwk , Jvrt , &
            & l1 , l2 , lp , lpimn , ls , Lshsgs , lwk , Lwork , mab ,  &
            & Mdc , mmax
      INTEGER mn , Ndc , Nlat , nln , Nlon , Nt
 
      DIMENSION Vort(Ivrt,Jvrt,Nt) , Cr(Mdc,Ndc,Nt) , Ci(Mdc,Ndc,Nt)
      DIMENSION Wshsgs(Lshsgs) , Work(Lwork)
!
!     check input parameters
!
      Ierror = 1
      IF ( Nlat<3 ) RETURN
      Ierror = 2
      IF ( Nlon<4 ) RETURN
      Ierror = 3
      IF ( Isym<0 .OR. Isym>2 ) RETURN
      Ierror = 4
      IF ( Nt<0 ) RETURN
      Ierror = 5
      imid = (Nlat+1)/2
      IF ( (Isym==0 .AND. Ivrt<Nlat) .OR. (Isym>0 .AND. Ivrt<imid) )    &
         & RETURN
      Ierror = 6
      IF ( Jvrt<Nlon ) RETURN
      Ierror = 7
      IF ( Mdc<MIN0(Nlat,(Nlon+1)/2) ) RETURN
      mmax = MIN0(Nlat,(Nlon+2)/2)
      Ierror = 8
      IF ( Ndc<Nlat ) RETURN
      Ierror = 9
      imid = (Nlat+1)/2
      lpimn = (imid*mmax*(Nlat+Nlat-mmax+1))/2
      l2 = (Nlat+MOD(Nlat,2))/2
      l1 = MIN0((Nlon+2)/2,Nlat)
      lp = Nlat*(3*(l1+l2)-2) + (l1-1)*(l2*(2*Nlat-l1)-3*l1)/2 + Nlon + &
         & 15
      IF ( Lshsgs<lp ) RETURN
      Ierror = 10
!
!     verify unsaved work space (add to what shses requires, file f3)
!
!
!     set first dimension for a,b (as requried by shses)
!
      mab = MIN0(Nlat,Nlon/2+1)
      mn = mab*Nlat*Nt
      ls = Nlat
      IF ( Isym>0 ) ls = imid
      nln = Nt*ls*Nlon
      IF ( Lwork<nln+ls*Nlon+2*mn+Nlat ) RETURN
      Ierror = 0
!
!     set work space pointers
!
      ia = 1
      ib = ia + mn
      is = ib + mn
      iwk = is + Nlat
      lwk = Lwork - 2*mn - Nlat
      CALL DVRTGS1(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc,       &
                & Work(ia),Work(ib),mab,Work(is),Wshsgs,Lshsgs,Work(iwk)&
                & ,lwk,Ierror)
      END SUBROUTINE DVRTGS

      
      SUBROUTINE DVRTGS1(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,Cr,Ci,Mdc,Ndc, &
                      & A,B,Mab,Sqnn,Wsav,Lwsav,Wk,Lwk,Ierror)
      IMPLICIT NONE
      DOUBLE PRECISION A , B , Ci , Cr , fn , Sqnn , Vort , Wk , Wsav
      INTEGER Ierror , Isym , Ivrt , Jvrt , k , Lwk , Lwsav , m , Mab , &
            & Mdc , mmax , n , Ndc , Nlat , Nlon , Nt
      DIMENSION Vort(Ivrt,Jvrt,Nt) , Cr(Mdc,Ndc,Nt) , Ci(Mdc,Ndc,Nt)
      DIMENSION A(Mab,Nlat,Nt) , B(Mab,Nlat,Nt) , Sqnn(Nlat)
      DIMENSION Wsav(Lwsav) , Wk(Lwk)
!
!     set coefficient multiplyers
!
      DO n = 2 , Nlat
         fn = FLOAT(n-1)
         Sqnn(n) = SQRT(fn*(fn+1.))
      ENDDO
!
!     compute divergence scalar coefficients for each vector field
!
      DO k = 1 , Nt
         DO n = 1 , Nlat
            DO m = 1 , Mab
               A(m,n,k) = 0.0
               B(m,n,k) = 0.0
            ENDDO
         ENDDO
!
!     compute m=0 coefficients
!
         DO n = 2 , Nlat
            A(1,n,k) = Sqnn(n)*Cr(1,n,k)
            B(1,n,k) = Sqnn(n)*Ci(1,n,k)
         ENDDO
!
!     compute m>0 coefficients
!
         mmax = MIN0(Nlat,(Nlon+1)/2)
         DO m = 2 , mmax
            DO n = m , Nlat
               A(m,n,k) = Sqnn(n)*Cr(m,n,k)
               B(m,n,k) = Sqnn(n)*Ci(m,n,k)
            ENDDO
         ENDDO
      ENDDO
!
!     synthesize a,b into vort
!
      CALL DSHSGS(Nlat,Nlon,Isym,Nt,Vort,Ivrt,Jvrt,A,B,Mab,Nlat,Wsav,    &
               & Lwsav,Wk,Lwk,Ierror)
      END SUBROUTINE DVRTGS1
